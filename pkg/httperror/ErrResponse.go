package httperror

import (
	"net/http"

	"github.com/go-chi/render"
)

// ErrResponse represents an error
type ErrResponse struct {
	Err            error  `json:"-"` // low-level runtime error
	HTTPStatusCode int    `json:"-"` // http response status code
	ErrorText      string `json:"error,omitempty"`
}

// Render the ErrResponse with right http status
func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

// ErrResponseNotAcceptable is error with StatusNotAcceptable
func ErrResponseNotAcceptable(err error) *ErrResponse {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusNotAcceptable,
		ErrorText:      err.Error(),
	}
}

// ErrResponseBadRequest is error with StatusBadRequest
func ErrResponseBadRequest(err error) *ErrResponse {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusBadRequest,
		ErrorText:      err.Error(),
	}
}

// ErrResponseInternalServerError is error with StatusInternalServerError
func ErrResponseInternalServerError(err error) *ErrResponse {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusInternalServerError,
		ErrorText:      err.Error(),
	}
}

// ErrResponseForbidden is error with StatusForbidden
func ErrResponseForbidden(err error) *ErrResponse {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusForbidden,
		ErrorText:      err.Error(),
	}
}

// ErrResponseNotFound is error with StatusNotFound
func ErrResponseNotFound(err error) *ErrResponse {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusNotFound,
		ErrorText:      err.Error(),
	}
}
