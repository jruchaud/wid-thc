package project

import (
	"net/http"
	tag "wid/pkg/tag"
	"wid/pkg/team"
	"wid/pkg/user"

	"github.com/google/uuid"
)

// IDs list of uuid
type IDs []uuid.UUID

// ToArray returns the ids to string type
func (u IDs) ToArray() []string {
	var arr []string
	for _, id := range u {
		arr = append(arr, id.String())
	}
	return arr
}

// Bind the IDs
func (u *IDs) Bind(r *http.Request) error {
	return nil
}

// Project is a project in which a user is timing a task
type Project struct {
	ID            uuid.UUID `json:"id"`
	Color         string    `json:"color"`
	Name          string    `json:"name"`
	AllTagsUsed   tag.Tags  `json:"tags,omitempty"`
	DefaultTags   tag.Tags  `json:"default,omitempty"`
	Permission    string    `json:"permission,omitempty"`
	RepoID        uuid.UUID `json:"repoId,omitempty"`
	RepoCategory  string    `json:"repoCategory,omitempty"`
	RepoProjectID string    `json:"repoProjectId,omitempty"`
}

// Render the project
func (u Project) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the Project
func (u *Project) Bind(r *http.Request) error {
	return nil
}

// Projects is list of Project
type Projects []Project

// Render the Projects
func (u Projects) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedProjects is paginated list of Project
type PaginatedProjects struct {
	Projects Projects `json:"items"`
	Count    int      `json:"count"`
}

// Render the ResponseProjects
func (tags PaginatedProjects) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// ProjectTaskRemame is use to global renaming of title or tags
type ProjectTaskRemame struct {
	Title     string   `json:"title"`
	Tags      []string `json:"tags"`
	ByTitle   string   `json:"byTitle"`
	ByTags    []string `json:"byTags"`
	IsDefault bool     `json:"isDefault"`
}

// Bind the ProjectTaskRemame
func (u *ProjectTaskRemame) Bind(r *http.Request) error {
	return nil
}

// TaskToMove a task to move
type TaskToMove struct {
	Title       string    `json:"title"`
	Tags        tag.Tags  `json:"tags"`
	ToProjectID uuid.UUID `json:"toProjectId"`
	OnlyForMe   bool      `json:"onlyForMe"`
}

// Bind the TaskToMove
func (u *TaskToMove) Bind(r *http.Request) error {
	return nil
}

// Collaborators for the projects
type Collaborators struct {
	Users user.TinyUsers `json:"users"`
	Teams team.Teams     `json:"teams"`
}

// Render the Collaborators
func (collab Collaborators) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
