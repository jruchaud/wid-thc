package project

import (
	"net/http"
	"wid/pkg/httperror"
	"wid/pkg/permission"
	tag "wid/pkg/tag"
	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// SaveProject saves the project
func SaveProject(w http.ResponseWriter, r *http.Request) {
	data := &Project{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	newProject := false
	if data.ID == (uuid.UUID{}) {
		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		data.ID = id
		newProject = true
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsert(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if newProject {
		err = permission.UpInsertMaintainer(r.Context(), userID, data.ID)
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
	}

	render.Render(w, r, data)
}

// RemoveProject remove a project
func RemoveProject(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteProject(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetProject returns the project information
func GetProject(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	project, err := Select(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, project)
}

// GetProjects returns projects for the connected user
func GetProjects(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	projects, err := SelectAll(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, projects)
}

// SearchByName returns the all projects begin by name
func SearchByName(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	name := r.URL.Query().Get("q")

	projects, err := Search(r.Context(), userID, name)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, projects)
}

// AddTag in a project
func AddTag(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	data := &tag.Configuration{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	ts := tag.NewTags(data.Tag)
	err = UpdateTags(r.Context(), userID, projectID, ts)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if data.IsDefault {
		err = AddDefaultTag(r.Context(), userID, projectID, ts)
	} else {
		err = RemoveDefaultTag(r.Context(), userID, projectID, ts)
	}
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetTags returns all tags for a project
func GetTags(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	ps, err := SelectTags(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, ps)
}

// GetDefaultTags returns all default tags for a project
func GetDefaultTags(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	ps, err := SelectAllDefaultTags(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, ps)
}

// GetUsers returns all users for a projects
func GetUsers(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	users, err := SelectUsers(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, users)
}

// GetTeams returns all teams for a projects
func GetTeams(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	teams, err := SelectTeams(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, teams)
}

// RemoveTag remove a tag in the project
func RemoveTag(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	value := r.URL.Query().Get("tag")

	err = DeleteTag(r.Context(), userID, projectID, tag.Tag(value))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RenameTag rename a tag by an other in the project
func RenameTag(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	value := r.URL.Query().Get("tag")
	other := r.URL.Query().Get("by")

	err = UpdateTag(r.Context(), userID, projectID, tag.Tag(value), tag.Tag(other))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RenameTasks rename the tasks name by an other in the project
func RenameTasks(w http.ResponseWriter, r *http.Request) {
	data := &ProjectTaskRemame{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpdateTasks(r.Context(), userID, projectID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = UpdateTags(r.Context(), userID, projectID, tag.NewStringTags(data.ByTags...))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Move the tasks
func MoveTasks(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	data := &TaskToMove{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = UpdateProjectForTask(r.Context(), userID, projectID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetCollaborators get all collaborators for all projects
func GetCollaborators(w http.ResponseWriter, r *http.Request) {
	data := &IDs{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	users, err := SelectAllUsersCollaborators(r.Context(), userID, reportID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	teams, err := SelectAllTeamsCollaborators(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, Collaborators{
		Users: users,
		Teams: teams,
	})
}
