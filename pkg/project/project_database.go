package project

import (
	"context"
	"errors"
	"fmt"
	"wid/pkg/permission"
	"wid/pkg/selector"
	tag "wid/pkg/tag"
	"wid/pkg/team"
	"wid/pkg/user"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// UpInsert a project
func UpInsert(
	context context.Context,
	userID uuid.UUID,
	project *Project) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		INSERT INTO project(id, name, color) 
		VALUES ($1, $2, $3)
		ON CONFLICT (id) DO UPDATE SET name=excluded.name, color=excluded.color
		WHERE %s
	`, "$4", "$1")

	_, err := db.Exec(context, sql,
		project.ID, project.Name, project.Color, userID)

	return err
}

// DeleteProject deletes a project
func DeleteProject(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		SELECT 1 
		FROM project
		WHERE id=$1 AND  %s
	`, "$2", "$1")

	var checkMaintainer int
	row := db.QueryRow(context, sql, projectID, userID)
	err := row.Scan(&checkMaintainer)
	if err != nil {
		return err
	}
	if checkMaintainer == 0 {
		return errors.New("Invalid permission")
	}

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql = "DELETE FROM team_permission WHERE projectId=$1"
	_, err = tx.Exec(context, sql, projectID)
	if err != nil {
		return err
	}

	sql = "UPDATE task SET project=NULL WHERE project=$1"
	_, err = tx.Exec(context, sql, projectID)
	if err != nil {
		return err
	}

	sql = "DELETE FROM user_permission WHERE projectId=$1"
	_, err = tx.Exec(context, sql, projectID)
	if err != nil {
		return err
	}

	sql = `
		DELETE FROM report_project WHERE projectId=$1 
	`
	_, err = tx.Exec(context, sql, projectID)
	if err != nil {
		return err
	}

	sql = "DELETE FROM project WHERE id=$1"
	_, err = tx.Exec(context, sql, projectID)

	err = tx.Commit(context)
	return err
}

// SQLGetPermission is use to have the correct permission between maintainer/reporter in team/user.
var SQLGetPermission = `
	COALESCE(
		((
			SELECT up.permission
			FROM user_permission up
			WHERE up.projectID = p.id AND up.userId = $1 AND up.permission = 'maintainer'
			LIMIT 1
		)
		UNION
		(
			SELECT tp.permission
			FROM team_permission tp JOIN team_member tm ON tm.teamId = tp.teamId
			WHERE tp.projectID = p.id AND tm.userId = $1 AND tp.permission = 'maintainer'
			LIMIT 1
		)),
		((
			SELECT up.permission
			FROM user_permission up
			WHERE up.projectID = p.id AND up.userId = $1 AND up.permission = 'reporter'
			LIMIT 1
		)
		UNION
		(
			SELECT tp.permission
			FROM team_permission tp JOIN team_member tm ON tm.teamId = tp.teamId
			WHERE tp.projectID = p.id AND tm.userId = $1 AND tp.permission = 'reporter'
			LIMIT 1
		))
	)
`

// Select project
func Select(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (Project, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		SELECT 
			p.id, p.name, p.color,
			p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, ''),
			%s as permission
		FROM project p
		WHERE p.id = $2 
		AND %s
	`, SQLGetPermission, permission.VerifyPermission("$1", "$2", permission.PermissionReporter))

	row := db.QueryRow(context, sql, userID, projectID)

	var p Project
	err := row.Scan(&p.ID, &p.Name, &p.Color,
		&p.RepoID, &p.RepoCategory, &p.RepoProjectID,
		&p.Permission)

	return p, err
}

// SelectAll projects
func SelectAll(
	context context.Context,
	userID uuid.UUID) (PaginatedProjects, error) {

	sql := fmt.Sprintf(`
		SELECT 
			p.id, p.name, p.color,
			p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, ''),
			%s as permission
		FROM project p
		WHERE %s
		ORDER BY p.name
	`, SQLGetPermission, permission.VerifyPermission("$1", "p.id", permission.PermissionReporter))

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "color", "permission"},
		},
		sql,
		userID,
	)

	var res PaginatedProjects
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var p Project

		err := rows.Scan(&p.ID, &p.Name, &p.Color,
			&p.RepoID, &p.RepoCategory, &p.RepoProjectID,
			&p.Permission, &res.Count)
		if err != nil {
			return res, err
		}

		res.Projects = append(res.Projects, p)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// Search a project by name
func Search(
	context context.Context,
	userID uuid.UUID,
	projectName string) (Projects, error) {

	sql := fmt.Sprintf(`
		SELECT 
			p.id, p.name, p.color,
			p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, ''),
			%s as permission
		FROM project p
		WHERE %s 
			AND p.name ilike $2 
		ORDER BY name
		LIMIT 10
	`, SQLGetPermission, permission.VerifyPermission("$1", "p.id", permission.PermissionReporter))

	db := wcontext.GetDatabase(context)

	rows, err := db.Query(context, sql, userID, "%"+projectName+"%")

	var res []Project
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var p Project

		err := rows.Scan(&p.ID, &p.Name, &p.Color,
			&p.RepoID, &p.RepoCategory, &p.RepoProjectID,
			&p.Permission)
		if err != nil {
			return res, err
		}

		res = append(res, p)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// UpdateTags add tag in project
func UpdateTags(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	tags tag.Tags) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		UPDATE project 
		SET allTagsUsed = array_distinct(allTagsUsed || $1)
		WHERE id = $2 AND %s
	`, "$3", "$2")

	_, err := db.Exec(context, sql, tags.ToArray(), projectID, userID)

	return err
}

// AddDefaultTag add default tag in project
func AddDefaultTag(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	tags tag.Tags) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET defaultTags = array_distinct(defaultTags || $1)
		WHERE id = $2 AND %s
	`, "$3", "$2")

	_, err := db.Exec(context, sql, tags.ToArray(), projectID, userID)

	return err
}

// RemoveDefaultTag remove default tag in project
func RemoveDefaultTag(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	tags tag.Tags) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET defaultTags = array_remove(defaultTags, $1)
		WHERE id = $2 AND %s
	`, "$3", "$2")

	_, err := db.Exec(context, sql, tags.ToArray()[0], projectID, userID)

	return err
}

// SelectAllDefaultTags get all default tags
func SelectAllDefaultTags(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (tag.Tags, error) {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		SELECT defaultTags FROM project WHERE id = $2 AND %s
	`, "$1", "$2")

	rows := db.QueryRow(context, sql, userID, projectID)

	var tags []string
	var res tag.Tags

	err := rows.Scan(&tags)
	if err != nil {
		return res, err
	}

	res = tag.NewStringTags(tags...)
	return res, nil
}

// SelectTags get all tags and default
func SelectTags(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (tag.PaginatedConfigurations, error) {

	sql := permission.SQLRequestReporter(`
		WITH tags AS (
			SELECT UNNEST(alltagsused) tag, COALESCE(defaultTags, '{}') AS defaultTags 
			FROM project WHERE id = $2 AND %s
		) SELECT tag, tag = ANY (defaultTags) AS isDefault FROM tags
	`, "$1", "$2")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "tag",
			Fields:     []string{"tag", "isDefault"},
		},
		sql,
		userID, projectID,
	)

	var res tag.PaginatedConfigurations
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var t tag.Configuration

		err := rows.Scan(&t.Tag, &t.IsDefault, &res.Count)
		if err != nil {
			return res, err
		}

		res.Configurations = append(res.Configurations, t)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectUsers returns all users of project
func SelectUsers(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (user.PaginatedTinyUsers, error) {

	sql := permission.SQLRequestMaintainer(`
		SELECT u.id, u.name, u.email, p.permission
		FROM wid_user u, user_permission p
		WHERE u.id = p.userId
		AND p.projectId = $1
		AND %s
	`, "$2", "p.projectId")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "email", "permission"},
		},
		sql,
		projectID, userID,
	)

	var res user.PaginatedTinyUsers
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var u user.TinyUser
		var email string

		err := rows.Scan(&u.ID, &u.Name, &email, &u.Permission, &res.Count)
		if err != nil {
			return res, err
		}
		u.Avatar = user.GetAvatar(email)

		res.TinyUsers = append(res.TinyUsers, u)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectTeams return all teams for an user
func SelectTeams(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (team.PaginatedTeams, error) {

	sql := permission.SQLRequestMaintainer(`
		SELECT t.id, t.name, p.permission
		FROM team t, team_permission p
		WHERE t.id = p.teamId
		AND projectId = $1
		AND %s
	`, "$2", "p.projectId")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "permission"},
		},
		sql,
		projectID, userID,
	)

	var res team.PaginatedTeams
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var t team.Team

		err := rows.Scan(&t.ID, &t.Name, &t.Permission, &res.Count)
		if err != nil {
			return res, err
		}

		res.Teams = append(res.Teams, t)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// DeleteTag remove a tag
func DeleteTag(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	tag tag.Tag) error {

	db := wcontext.GetDatabase(context)

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET allTagsUsed = array_remove(allTagsUsed, $1)
		WHERE id = $2 AND %s
	`, "$3", "$2")

	_, err = tx.Exec(context, sql, tag, projectID, userID)
	if err != nil {
		return err
	}

	sql = permission.SQLRequestMaintainer(`
		UPDATE task t
		SET tags = array_remove(t.tags, $1)
		WHERE t.project = $2 AND %s
	`, "$3", "$2")

	_, err = tx.Exec(context, sql, tag, projectID, userID)
	if err != nil {
		return err
	}

	err = tx.Commit(context)
	return err
}

// UpdateTag update a tag
func UpdateTag(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	tag tag.Tag,
	by tag.Tag) error {

	db := wcontext.GetDatabase(context)

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET allTagsUsed = array_distinct(array_replace(allTagsUsed, $1, $2))
		WHERE id = $3 AND %s
	`, "$4", "$3")

	_, err = tx.Exec(context, sql, tag, by, projectID, userID)
	if err != nil {
		return err
	}

	sql = permission.SQLRequestMaintainer(`
		UPDATE task t
		SET tags = array_distinct(array_replace(t.tags, $1, $2))
		WHERE t.project = $3 AND %s
	`, "$4", "$3")

	_, err = tx.Exec(context, sql, tag, by, projectID, userID)
	if err != nil {
		return err
	}

	err = tx.Commit(context)
	return err
}

// UpdateTasks update all tasks by a new title and tags
func UpdateTasks(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	taskRename *ProjectTaskRemame) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE task t
		SET title = $3, tags = $4
		WHERE t.title = $1 AND ((t.tags @> $2 AND t.tags <@ $2) OR ((t.tags IS NULL OR t.tags = '{}') AND ($2 IS NULL OR $2 = '{}')))
		AND (($5 AND t.endDate IS NULL) OR (NOT $5 AND t.endDate IS NOT NULL))
		AND t.project=$6 AND %s
		`, "$7", "$6")

	_, err := db.Exec(
		context, sql,
		taskRename.Title, taskRename.Tags,
		taskRename.ByTitle, taskRename.ByTags,
		taskRename.IsDefault,
		projectID, userID,
	)

	return err
}

// UpdateProjectForTask update the project for all tasks
func UpdateProjectForTask(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	task *TaskToMove) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
			UPDATE task t
			SET project = $6
			WHERE ((t.owner = $1 AND $5) OR NOT $5)
			AND ((t.tags @> $2 AND t.tags <@ $2) OR ((t.tags IS NULL OR t.tags = '{}') AND ($2 IS NULL OR $2 = '{}')))
			AND t.title = $3 
			AND t.project = $4
			AND %s
	`, "$1", "t.project")

	_, err := db.Exec(context, sql, userID, task.Tags.ToArray(), task.Title, projectID, task.OnlyForMe, task.ToProjectID)

	return err
}

// SelectFromRepo get project from the project id of the repo
func SelectFromRepo(
	context context.Context,
	userID uuid.UUID,
	repoID uuid.UUID,
	repoProjectId string,
) (Project, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		SELECT 
			p.id, p.name, p.color,
			p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, ''),
			%s as permission
		FROM project p
		WHERE p.repoID = $2 AND p.repoProjectId = $3
		AND %s
	`, SQLGetPermission, permission.VerifyPermission("$1", "p.id", permission.PermissionReporter))

	row := db.QueryRow(context, sql, userID, repoID, repoProjectId)

	var p Project
	err := row.Scan(&p.ID, &p.Name, &p.Color,
		&p.RepoID, &p.RepoCategory, &p.RepoProjectID,
		&p.Permission)

	return p, err
}

// SelectAllUsersCollaborators return all uers for the projects
func SelectAllUsersCollaborators(
	context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	projectIDs *IDs,
) (user.TinyUsers, error) {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		SELECT DISTINCT u.id, u.name, u.email
		FROM wid_user u, user_permission p
		WHERE u.id = p.userId
		AND (p.projectId = ANY ($1) OR array_length($1, 1) IS NULL)
		AND %s
		UNION
		SELECT DISTINCT u.id, u.name, u.email
		FROM wid_user u, report_requested_user r
		WHERE u.id = r.userId
		AND r.reportID=$3
		UNION
		SELECT DISTINCT u.id, u.name, u.email
		FROM wid_user u, report r
		WHERE u.id = ANY (r.sharedUsers)
		AND r.id=$3

	`, "$2", "p.projectId")

	rows, err := db.Query(context, sql, projectIDs.ToArray(), userID, reportID)

	var res user.TinyUsers
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var u user.TinyUser
		var email string

		err := rows.Scan(&u.ID, &u.Name, &email)
		if err != nil {
			return res, err
		}
		u.Avatar = user.GetAvatar(email)

		res = append(res, u)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectAllTeamsCollaborators return all teams for the projects
func SelectAllTeamsCollaborators(
	context context.Context,
	userID uuid.UUID,
	projectIDs *IDs,
) (team.Teams, error) {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		SELECT DISTINCT t.id, t.name
		FROM team t, team_permission p
		WHERE t.id = p.teamId
		AND (p.projectId = ANY ($1) OR array_length($1, 1) IS NULL)
		AND %s
	`, "$2", "p.projectId")

	rows, err := db.Query(context, sql, projectIDs.ToArray(), userID)

	var res team.Teams
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var t team.Team

		err := rows.Scan(&t.ID, &t.Name)
		if err != nil {
			return res, err
		}

		res = append(res, t)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}
