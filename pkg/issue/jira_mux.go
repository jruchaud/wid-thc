package issue

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/tag"
	"wid/pkg/user"

	"github.com/andygrunwald/go-jira"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// ConvertJiraIssue convert jira issues
func ConvertJiraIssue(client jira.Client, issue jira.Issue) *Issue {
	id := issue.Key

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))
	for _, label := range issue.Fields.Labels {
		tags.Add(tag.Tag(label))
	}

	url := client.GetBaseURL()
	url.Path += "/browse/" + id

	return &Issue{
		ID:    id,
		Title: issue.Fields.Summary,
		URL:   url.String(),
		Tags:  tags,
	}
}

// SaveJiraAuth save the auth for Jira
func SaveJiraAuth(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	host, err := SelectHost(r.Context(), id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	username := r.URL.Query().Get("username")
	token := r.URL.Query().Get("token")

	tp := jira.BasicAuthTransport{
		Username: username,
		Password: token,
	}

	client, err := jira.NewClient(tp.Client(), host)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	_, response, err := client.Status.GetAllStatuses()
	if response.StatusCode == http.StatusUnauthorized {
		// FIXME We should return unauthorized here but wid-cbd use this value in a generic manner to disconnect the user from the ui
		// This needs to be refined at wid-cbd level. Meanwhile, let's use forbidden status to notify the client that the auth failed
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	SaveAuth(w, r)
}

// ProjectSearchList extends jira lib
type ProjectSearchList struct {
	Values jira.ProjectList `json:"values" structs:"values"`
}

// ProjectSearch extends jira lib
func ProjectSearch(c *jira.Client, query string) (*ProjectSearchList, *jira.Response, error) {
	apiEndpoint := fmt.Sprintf("rest/api/2/project/search?query=%s&maxResults=10", url.QueryEscape(query))
	req, err := c.NewRequest("GET", apiEndpoint, nil)
	if err != nil {
		return nil, nil, err
	}

	projectList := new(ProjectSearchList)
	resp, err := c.Do(req, projectList)
	if err != nil {
		jerr := jira.NewJiraError(resp, err)
		return nil, resp, jerr
	}

	return projectList, resp, nil
}

// DeleteWorklogRecord extends jira lib
func DeleteWorklogRecord(c *jira.Client, issueID string, worklogID string) (*jira.Response, error) {
	apiEndpoint := fmt.Sprintf("rest/api/2/issue/%s/worklog/%s", issueID, worklogID)
	req, err := c.NewRequest("DELETE", apiEndpoint, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.Do(req, nil)
	if err != nil {
		err = jira.NewJiraError(resp, err)
	}

	return resp, err
}

// WorklogRecord
type WorklogRecord struct {
	Started          string `json:"started,omitempty"`
	TimeSpentSeconds int    `json:"timeSpentSeconds,omitempty"`
	Comment          string `json:"comment,omitempty"`
}

// AddWorklogRecord extends jira lib
func AddWorklogRecord(c *jira.Client, issueID string, record *WorklogRecord) (*jira.WorklogRecord, *jira.Response, error) {
	apiEndpoint := fmt.Sprintf("rest/api/2/issue/%s/worklog", issueID)
	req, err := c.NewRequest("POST", apiEndpoint, record)
	if err != nil {
		return nil, nil, err
	}

	var w *jira.WorklogRecord
	resp, err := c.Do(req, &w)
	if err != nil {
		err = jira.NewJiraError(resp, err)
	}

	return w, resp, err
}

// GetJiraClient get the client
func GetJiraClient(w http.ResponseWriter, r *http.Request, id uuid.UUID) (*jira.Client, error) {
	var client *jira.Client

	username, token, host, err := GetToken(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return client, err
	}

	tp := jira.BasicAuthTransport{
		Username: username,
		Password: token,
	}

	client, _ = jira.NewClient(tp.Client(), host)
	return client, nil
}

// SearchJiraProjects search the projects
func SearchJiraProjects(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	q := r.URL.Query().Get("q")

	client, err := GetJiraClient(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	projects, _, err := ProjectSearch(client, q)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var repoProjects RepoProjects
	for _, project := range *&projects.Values {
		repoProjects = append(repoProjects, RepoProject{
			ID:   project.ID,
			Name: project.Name,
		})
	}

	render.Render(w, r, repoProjects)
}

// SearchJiraIssues returns the issues
func SearchJiraIssues(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetJiraClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var jql string
	if len(q) > 0 && q[0] == '#' && strings.Contains(q, "-") {
		jql = fmt.Sprintf(
			"project = %s AND id = \"%s\" order by created DESC",
			currentProject.RepoProjectID, q[1:],
		)
	} else {
		jql = fmt.Sprintf(
			"project = %s AND summary ~ \"%s\\u002a\" order by created DESC",
			currentProject.RepoProjectID, q,
		)
	}
	values, _, err := client.Issue.Search(jql, &jira.SearchOptions{MaxResults: 10})
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var all Issues
	for _, value := range values {
		issue := ConvertJiraIssue(*client, value)
		all = append(all, *issue)
	}

	render.Render(w, r, all)
}

// GetJiraIssue get an issue
func GetJiraIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetJiraClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err := client.Issue.Get(q[1:], nil)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	issue := ConvertJiraIssue(*client, *value)
	render.Render(w, r, issue)
}

// GetJiraProject returns the project
func GetJiraProject(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	client, err := GetJiraClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	project, _, err := client.Project.Get(currentProject.RepoProjectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	url := client.GetBaseURL()
	url.Path += "/browse/" + project.Key

	repoProject := RepoProject{
		ID:   project.ID,
		Name: project.Name,
		URL:  url.String(),
	}
	render.Render(w, r, repoProject)
}

// CreateJiraIssue create an issue
func CreateJiraIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	title := r.URL.Query().Get("title")

	client, err := GetJiraClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err := client.Issue.Create(&jira.Issue{
		Fields: &jira.IssueFields{
			Type: jira.IssueType{
				Name: "Task",
			},
			Project: jira.Project{
				ID: currentProject.RepoProjectID,
			},
			Summary: title,
		},
	})
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err = client.Issue.Get(value.ID, nil)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	issue := ConvertJiraIssue(*client, *value)
	render.Render(w, r, *issue)
}

// GetJiraActivities returns the activities
func GetJiraActivities(w http.ResponseWriter, r *http.Request, repoID uuid.UUID) (Activities, error) {
	var activities Activities

	userID, err := user.GetUserID(r)
	if err != nil {
		return activities, err
	}

	startDate, err := strconv.ParseInt(r.URL.Query().Get("startDate"), 10, 64)
	endDate, err := strconv.ParseInt(r.URL.Query().Get("endDate"), 10, 64)

	client, err := GetJiraClient(w, r, repoID)
	if err != nil {
		return activities, err
	}

	username, _, err := GetJiraUsername(client)
	if err != nil {
		return activities, err
	}

	values, _, err := ActivityStreams(client, username, startDate*1000, endDate*1000)
	if err != nil {
		return activities, err
	}

	// Get the current jira projects
	projects := make(map[string]*project.Project)
	for _, entry := range values.Entries {
		ref := entry.Object
		if ref.Type != ActivityObjectTypeIssue {
			ref = entry.Target
		}

		if ref.Type == ActivityObjectTypeIssue {
			key := strings.Split(ref.Title, "-")[0]
			projects[key] = nil
		}
	}

	// Search the jira projects into wid projects
	for key := range projects {
		var p project.Project

		jiraProject, _, err := client.Project.Get(key)
		if err == nil {
			p, err = project.SelectFromRepo(r.Context(), userID, repoID, jiraProject.ID)
			if err != nil {
				p = project.Project{Name: jiraProject.Name}
			}
		}

		projects[key] = &p
	}

	// Convert
	for _, entry := range values.Entries {
		activity := ConvertJiraActivity(projects, entry)
		if activity != nil {
			activities = append(activities, *activity)
		}
	}

	return activities, nil
}

func ConvertJiraActivity(projects map[string]*project.Project, entry Entry) *Activity {
	ref := entry.Object
	if ref.Type != ActivityObjectTypeIssue {
		ref = entry.Target

		if ref.Type != ActivityObjectTypeIssue {
			return nil
		}
	}

	id := ref.Title

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))
	// for _, label := range issue.Fields.Labels {
	// 	tags.Add(tag.Tag(label))
	// }

	// 2021-04-13T15:41:05.011Z
	updated, _ := time.Parse(time.RFC3339Nano, entry.Updated)

	projectKey := strings.Split(ref.Title, "-")[0]
	p := projects[projectKey]

	return &Activity{
		Category:     RepoCategoryJira,
		Type:         "ISSUE",
		Date:         updated,
		Title:        ref.Summary,
		Tags:         tags,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// SyncJiraIssue synchronise the issues
func SyncJiraIssue(
	w http.ResponseWriter, r *http.Request,
	userID uuid.UUID, data *report.ValidationSheet,
) error {

	// Delete old worklogs
	oldSync, err := report.SelectAllReportSynchronization(
		r.Context(), userID, data.Period.ReportID,
		data.Period.StartDate, data.Period.EndDate,
		RepoCategoryJira,
	)
	if err != nil {
		return err
	}

	for _, sync := range oldSync {
		client, err := GetJiraClient(w, r, sync.RepoID)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogs []jira.WorklogRecord
		err = json.Unmarshal([]byte(sync.Worklogs), &worklogs)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogsFailed []jira.WorklogRecord
		for _, worklog := range worklogs {
			_, err = DeleteWorklogRecord(client, worklog.IssueID, worklog.ID)
			if err != nil {
				log.Println(err)
				worklogsFailed = append(worklogsFailed, worklog)
			}
		}

		// Keeps only the worklogs in error
		buf, err := json.Marshal(worklogsFailed)
		if err != nil {
			log.Println(err)
			continue
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:          sync.ID,
			Worklogs:    string(buf),
			UpdatedDate: sync.UpdatedDate,
		})
		if err != nil {
			log.Println(err)
		}
	}

	// Get all data to synchronise
	tasks, err := report.SelectAllValidatedTasksToSync(
		r.Context(),
		userID,
		data.Period.ReportID,
		data.Period.StartDate,
		data.Period.EndDate,
		RepoCategoryJira,
	)
	if err != nil {
		return err
	}

	// Create worklogs
	now := time.Now()
	worklogs := make(map[uuid.UUID][]*jira.WorklogRecord)

	for _, validation := range tasks {
		for _, tag := range validation.TaskTags.ToArray() {
			if tag[0] == '#' {

				client, err := GetJiraClient(w, r, validation.RepoID)
				if err != nil {
					log.Println(err)
					continue
				}

				worklog, _, err := AddWorklogRecord(client, tag[1:], &WorklogRecord{
					Started:          data.Period.StartDate.Format("2006-01-02T15:04:05.000+0000"),
					Comment:          "From WID",
					TimeSpentSeconds: int(validation.Duration),
				})
				if err != nil {
					log.Println(err)
					continue
				}

				if worklogs[validation.RepoID] == nil {
					worklogs[validation.RepoID] = []*jira.WorklogRecord{worklog}
				} else {
					worklogs[validation.RepoID] = append(worklogs[validation.RepoID], worklog)
				}
			}
		}
	}

	// Keep the worklogs created
	for repoID, values := range worklogs {
		syncID, _ := uuid.NewUUID()

		buf, err := json.Marshal(values)
		if err != nil {
			return err
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:           syncID,
			RepoID:       repoID,
			RepoCategory: RepoCategoryJira,
			UpdatedDate:  now,
			StartDate:    data.Period.StartDate,
			EndDate:      data.Period.EndDate,
			ReportID:     data.Period.ReportID,
			Worklogs:     string(buf),
		})
		if err != nil {
			return err
		}
	}

	return err
}
