package issue

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/redmine"
	"wid/pkg/report"
	"wid/pkg/tag"
	"wid/pkg/user"

	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// SaveRedmineAuth save the auth for Redmine
func SaveRedmineAuth(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	host, err := SelectHost(r.Context(), id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	token := r.URL.Query().Get("token")

	client, err := redmine.NewClient(token, host)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	_, response, err := client.Account.Me()
	if response.StatusCode == http.StatusUnauthorized {
		// FIXME We should return unauthorized here but wid-cbd use this value in a generic manner to disconnect the user from the ui
		// This needs to be refined at wid-cbd level. Meanwhile, let's use forbidden status to notify the client that the auth failed
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	SaveAuth(w, r)
}

// GetRedmineClient get the client
func GetRedmineClient(w http.ResponseWriter, r *http.Request, id uuid.UUID) (*redmine.Client, error) {
	var client *redmine.Client

	_, token, host, err := GetToken(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return client, err
	}

	client, _ = redmine.NewClient(token, host)
	return client, nil
}

// SearchRedmineProjects search the projects
func SearchRedmineProjects(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	client, err := GetRedmineClient(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var repoProjects RepoProjects
	searchRedmineProjects(w, r, client, &repoProjects, 0)

	render.Render(w, r, repoProjects)
}

func searchRedmineProjects(w http.ResponseWriter, r *http.Request, client *redmine.Client, repoProjects *RepoProjects, offset int) {
	limit := 100

	projects, _, err := client.Projects.Get(&redmine.Option{Limit: limit, Offset: offset})
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	// It is not perfect, we do one more request to know if we are at the end, normally we have
	// to use the total_count
	if len(projects) == 0 {
		return
	}

	q := strings.ToUpper(r.URL.Query().Get("q"))

	for _, project := range projects {

		if strings.Contains(strings.ToUpper(project.Name), q) {
			*repoProjects = append(*repoProjects, RepoProject{
				ID:   strconv.Itoa(project.ID),
				Name: project.Name,
			})
		}
	}

	if len(*repoProjects) > 10 {
		return
	}

	searchRedmineProjects(w, r, client, repoProjects, offset+1*limit)
}

// SearchRedmineIssues returns the issues
func SearchRedmineIssues(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetRedmineClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	values, _, err := client.Search.Issues(currentProject.RepoProjectID, q, &redmine.Option{Limit: 10})
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var all Issues
	for _, value := range values {
		id := strconv.Itoa(value.ID)

		issue := Issue{
			ID:    id,
			Title: strings.SplitN(value.Title, ": ", 2)[1],
			URL:   value.URL,
			Tags:  tag.NewStringTags("#" + id),
		}
		all = append(all, issue)
	}

	render.Render(w, r, all)
}

// GetRedmineIssue get an issue
func GetRedmineIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")
	issueID, err := strconv.ParseInt(q[1:], 10, 64)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	client, err := GetRedmineClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err := client.Issues.Get(int(issueID))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if value == nil {
		err = errors.New("No issue")
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	id := strconv.Itoa(value.ID)
	issue := Issue{
		ID:    id,
		Title: value.Subject,
		URL:   client.Host + "/issues/" + id,
		Tags:  tag.NewStringTags("#" + id),
	}
	render.Render(w, r, issue)
}

// GetRedmineProject returns the project
func GetRedmineProject(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	client, err := GetRedmineClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	projectID, err := strconv.ParseInt(currentProject.RepoProjectID, 10, 64)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	project, _, err := client.Projects.GetByID(int(projectID))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	repoProject := RepoProject{
		ID:   currentProject.RepoProjectID,
		Name: project.Name,
		URL:  client.Host + "/projects/" + currentProject.RepoProjectID,
	}
	render.Render(w, r, repoProject)
}

// CreateRedmineIssue create an issue
func CreateRedmineIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	title := r.URL.Query().Get("title")

	client, err := GetRedmineClient(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	projectID, err := strconv.ParseInt(currentProject.RepoProjectID, 10, 64)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err := client.Issues.Create(&redmine.Issue{
		ProjectID: int(projectID),
		Subject:   title,
	})
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	id := strconv.Itoa(value.ID)
	issue := Issue{
		ID:    id,
		Title: value.Subject,
		URL:   client.Host + "/issues/" + id,
		Tags:  tag.NewStringTags("#" + id),
	}
	render.Render(w, r, issue)
}

// GetRedmineActivities returns the activities
func GetRedmineActivities(w http.ResponseWriter, r *http.Request, repoID uuid.UUID) (Activities, error) {
	var activities Activities

	userID, err := user.GetUserID(r)
	if err != nil {
		return activities, err
	}

	client, err := GetRedmineClient(w, r, repoID)
	if err != nil {
		return activities, err
	}

	endDate, err := strconv.ParseInt(r.URL.Query().Get("endDate"), 10, 64)
	dateValue := time.Unix(endDate, 0).UTC().Format("2006-01-02")

	user, _, err := client.Account.Me()
	if err != nil {
		return activities, err
	}

	key, _, err := client.Account.GetAtomKey()
	if err != nil {
		return activities, err
	}

	values, _, err := client.Issues.AtomActivity(key, dateValue, user.ID)
	if err != nil {
		return activities, err
	}

	// Get the current redmine projects
	projects := make(map[string]*project.Project)
	for _, entry := range values.Entries {
		projectName := strings.Split(entry.Title, " - ")[0]
		projects[entry.Id] = &project.Project{Name: projectName}
	}

	// Search the redmine projects into wid projects
	for key := range projects {

		redmineIssue, _, err := client.Issues.FromUrl(key)
		if err == nil {
			p, err := project.SelectFromRepo(r.Context(), userID, repoID, strconv.Itoa(redmineIssue.Project.ID))
			if err == nil {
				projects[key] = &p
			}
		}
	}

	// Convert
	for _, entry := range values.Entries {
		activity := ConvertRedmineActivity(projects, entry)
		if activity != nil {
			activities = append(activities, *activity)
		}
	}

	return activities, nil
}

func ConvertRedmineActivity(projects map[string]*project.Project, entry redmine.Entry) *Activity {
	// Redmine - Patch #32424: CommonMark Markdown Text Formatting
	// wid-test - Anomalie #11323 (Constatée): Test activité
	r := regexp.MustCompile(`([^-]+)- [^#]+#([0-9]+)[^:]*: (.+)`)
	groups := r.FindStringSubmatch(string(entry.Title))
	issueId := groups[2]
	title := groups[3]

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + issueId))

	// 2021-04-13T15:41:05.011Z
	updated, _ := time.Parse(time.RFC3339Nano, entry.Updated)

	p := projects[entry.Id]

	return &Activity{
		Category:     RepoCategoryRedmine,
		Type:         "ISSUE",
		Date:         updated,
		Title:        title,
		Tags:         tags,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// SyncRedmineIssue synchronise the issues
func SyncRedmineIssue(
	w http.ResponseWriter, r *http.Request,
	userID uuid.UUID, data *report.ValidationSheet,
) error {

	// Delete old worklogs
	oldSync, err := report.SelectAllReportSynchronization(
		r.Context(), userID, data.Period.ReportID,
		data.Period.StartDate, data.Period.EndDate,
		RepoCategoryRedmine,
	)
	if err != nil {
		return err
	}

	for _, sync := range oldSync {
		client, err := GetRedmineClient(w, r, sync.RepoID)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogs []redmine.TimeEntry
		err = json.Unmarshal([]byte(sync.Worklogs), &worklogs)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogsFailed []redmine.TimeEntry
		for _, worklog := range worklogs {
			_, err = client.TimeEntries.Delete(worklog.ID)
			if err != nil {
				log.Println(err)
				worklogsFailed = append(worklogsFailed, worklog)
			}
		}

		// Keeps only the worklogs in error
		buf, err := json.Marshal(worklogsFailed)
		if err != nil {
			log.Println(err)
			continue
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:          sync.ID,
			Worklogs:    string(buf),
			UpdatedDate: sync.UpdatedDate,
		})
		if err != nil {
			log.Println(err)
		}
	}

	// Get all data to synchronise
	tasks, err := report.SelectAllValidatedTasksToSync(
		r.Context(),
		userID,
		data.Period.ReportID,
		data.Period.StartDate,
		data.Period.EndDate,
		RepoCategoryRedmine,
	)
	if err != nil {
		return err
	}

	// Create worklogs
	now := time.Now()
	worklogs := make(map[uuid.UUID][]*redmine.TimeEntry)

	for _, validation := range tasks {
		tags := validation.TaskTags.ToArray()

		for _, tag := range tags {
			if tag[0] == '#' {

				client, err := GetRedmineClient(w, r, validation.RepoID)
				if err != nil {
					log.Println(err)
					continue
				}

				activites, _, err := client.TimeEntries.Enumerations()
				if err != nil {
					log.Println(err)
					continue
				}

				var current redmine.TimeEntryActivity
				for _, activity := range activites {
					for _, aTag := range tags {
						if strings.ToLower(aTag) == strings.ToLower(activity.Name) {
							current = activity
						}
					}
				}

				if current.ID == 0 {
					for _, activity := range activites {
						fmt.Println(current)
						if current.ID == 0 && activity.Active {
							current = activity
						}

						if activity.IsDefault {
							current = activity
							break
						}
					}
				}

				if current.ID == 0 {
					current = activites[0]
				}

				duration := validation.Duration / 60 / 60
				date := data.Period.StartDate.UTC().Add(time.Duration(-data.Period.Timezone) * time.Minute).Format("2006-01-02")

				timeEntry, _, err := client.TimeEntries.Create(&redmine.TimeEntry{
					IssueID:    tag[1:],
					Hours:      duration,
					Comments:   "From WID",
					ActivityId: strconv.Itoa(current.ID),
					SpentOn:    date,
				})
				if err != nil {
					log.Println(err)
					continue
				}

				if worklogs[validation.RepoID] == nil {
					worklogs[validation.RepoID] = []*redmine.TimeEntry{timeEntry}
				} else {
					worklogs[validation.RepoID] = append(worklogs[validation.RepoID], timeEntry)
				}
			}
		}
	}

	// Keep the worklogs created
	for repoID, values := range worklogs {
		syncID, _ := uuid.NewUUID()

		buf, err := json.Marshal(values)
		if err != nil {
			return err
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:           syncID,
			RepoID:       repoID,
			RepoCategory: RepoCategoryRedmine,
			UpdatedDate:  now,
			StartDate:    data.Period.StartDate,
			EndDate:      data.Period.EndDate,
			ReportID:     data.Period.ReportID,
			Worklogs:     string(buf),
		})
		if err != nil {
			return err
		}
	}

	return err
}
