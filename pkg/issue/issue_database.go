package issue

import (
	"context"
	"errors"
	"wid/pkg/permission"
	"wid/pkg/selector"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// UpInsertToken save the token for a repo
func UpInsertToken(
	context context.Context,
	repositoryID uuid.UUID,
	userID uuid.UUID,
	json string) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO repository_token(repositoryId, userId, token) 
		VALUES ($1, $2, $3)
		ON CONFLICT (repositoryId, userId) DO UPDATE SET token=excluded.token`

	_, err := db.Exec(context, sql, repositoryID, userID, json)

	return err
}

// UpdateRepoNotification update the notification for a repo
func UpdateRepoNotification(
	context context.Context,
	repositoryID uuid.UUID,
	userID uuid.UUID,
	notification bool) error {

	db := wcontext.GetDatabase(context)

	sql := `UPDATE repository_token SET notification = $3
			WHERE repositoryId=$1 AND userId=$2`

	_, err := db.Exec(context, sql, repositoryID, userID, notification)

	return err
}

// SelectToken get the current token for a repo
func SelectToken(
	context context.Context,
	repositoryID uuid.UUID,
	userID uuid.UUID) (string, string, error) {

	db := wcontext.GetDatabase(context)

	sql := `SELECT rt.token, r.host
		FROM repository_token rt JOIN repository r ON r.id = rt.repositoryId 
		WHERE rt.repositoryId=$1 AND rt.userId=$2`

	row := db.QueryRow(context, sql, repositoryID, userID)

	var token string
	var host string
	err := row.Scan(&token, &host)
	return token, host, err
}

// SelectAllRepos returns all repos
func SelectAllRepos(
	context context.Context,
	userID uuid.UUID) (PaginatedRepos, error) {

	sql := `SELECT r.id, r.name, r.category, r.host, rt.token IS NOT NULL as credential, COALESCE(rt.notification, false)
		FROM repository r 
		LEFT JOIN repository_token rt ON r.id = rt.repositoryId AND rt.userId = $1
		JOIN team_member t ON t.teamId = r.teamId
		WHERE t.userId = $1`

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "category", "host", "credential"},
		},
		sql,
		userID,
	)
	var res PaginatedRepos
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var r Repo

		err := rows.Scan(&r.ID, &r.Name, &r.Category, &r.Host, &r.Credential, &r.Notification, &res.Count)
		if err != nil {
			return res, err
		}

		res.Repos = append(res.Repos, r)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectRepository return the repo
func SelectRepository(
	context context.Context,
	userID uuid.UUID,
	repositoryID uuid.UUID) (Repo, error) {

	db := wcontext.GetDatabase(context)

	sql := `SELECT r.id, r.name, r.category, r.host, rt.token IS NOT NULL as credential, COALESCE(rt.notification, false)
		FROM repository r 
		LEFT JOIN repository_token rt ON r.id = rt.repositoryId AND rt.userId = $1
		JOIN team_member t ON t.teamId = r.teamId
		WHERE t.userId = $1 AND r.id = $2`

	var r Repo

	rows := db.QueryRow(context, sql, userID, repositoryID)
	err := rows.Scan(&r.ID, &r.Name, &r.Category, &r.Host, &r.Credential, &r.Notification)
	return r, err
}

// SelectAllAvailableRepos returns all available repo
func SelectAllAvailableRepos(
	context context.Context,
	userID uuid.UUID) (Repos, error) {

	db := wcontext.GetDatabase(context)

	sql := `SELECT r.id, r.name, r.category, r.host, rt.token IS NOT NULL
		FROM repository r 
		JOIN repository_token rt ON r.id = rt.repositoryId AND rt.userId = $1
		JOIN team_member t ON t.teamId = r.teamId
		WHERE t.userId = $1 AND rt.notification`

	rows, err := db.Query(context, sql, userID)

	var repos Repos
	if err != nil {
		return repos, err
	}
	defer rows.Close()

	for rows.Next() {
		var r Repo

		err := rows.Scan(&r.ID, &r.Name, &r.Category, &r.Host, &r.Credential)
		if err != nil {
			return repos, err
		}

		repos = append(repos, r)
	}

	err = rows.Err()
	if err != nil {
		return repos, err
	}

	return repos, nil
}

// DeleteToken delete the token
func DeleteToken(
	context context.Context,
	repositoryID uuid.UUID,
	userID uuid.UUID) error {
	db := wcontext.GetDatabase(context)

	sql := `DELETE FROM repository_token WHERE repositoryId=$1 AND userId=$2`

	_, err := db.Exec(context, sql, repositoryID, userID)

	return err
}

// UpInsertProject save the project repo
func UpInsertProject(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	project RepoProject) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET repoId=$1, repoCategory=$2, repoProjectId=$3
		WHERE id=$4 AND %s
	`, "$5", "$4")

	_, err := db.Exec(context, sql,
		project.Repo.ID, project.Repo.Category, project.ID,
		projectID, userID)

	return err
}

// DeleteProject delete the project repo
func DeleteProject(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		UPDATE project 
		SET repoId=NULL, repoCategory=NULL, repoProjectId=NULL
		WHERE id = $1 AND %s
	`, "$2", "$1")

	_, err := db.Exec(context, sql, projectID, userID)

	return err
}

// SelectHost returns the host of the repository
func SelectHost(context context.Context, repositoryID uuid.UUID) (string, error) {
	db := wcontext.GetDatabase(context)

	sql := `SELECT host FROM repository WHERE id=$1`
	row := db.QueryRow(context, sql, repositoryID)

	var host string
	err := row.Scan(&host)

	return host, err
}

// InsertRepository add a repository for a team
func InsertRepository(
	context context.Context,
	userID uuid.UUID,
	teamID uuid.UUID,
	repository *Repo) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO repository(id, name, category, host, teamId) 
		SELECT $1, $2, $3, $4, $5
		WHERE EXISTS (
			SELECT 1 
			FROM team_member t
			WHERE t.teamId = $5
			AND t.userId = $6
			AND t.isAdmin = true
		)`

	_, err := db.Exec(context, sql,
		repository.ID, repository.Name, repository.Category, repository.Host,
		teamID, userID)

	return err
}

// DeleteRepository remove a repository for a team
func DeleteRepository(
	context context.Context,
	userID uuid.UUID,
	repositoryID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := `SELECT 1 
			FROM team_member t JOIN repository r ON r.teamId = t.teamId
			WHERE r.id = $1 AND t.userId=$2 AND t.isAdmin = true`

	var checkAdmin int
	row := db.QueryRow(context, sql, repositoryID, userID)
	err := row.Scan(&checkAdmin)
	if err != nil {
		return err
	}
	if checkAdmin == 0 {
		return errors.New("Invalid permission")
	}

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql = permission.SQLRequestMaintainer(`
		UPDATE project p
		SET repoId=NULL, repoCategory=NULL, repoProjectId=NULL
		WHERE repoId=$1 AND %s
	`, "$2", "p.id")

	_, err = tx.Exec(context, sql, repositoryID, userID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM repository_token rt WHERE rt.repositoryId = $1`
	_, err = tx.Exec(context, sql, repositoryID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM repository WHERE id=$1 `
	_, err = tx.Exec(context, sql, repositoryID)
	if err != nil {
		return err
	}

	err = tx.Commit(context)
	return err
}

// SelectAllRepositories get all repositories for a team
func SelectAllRepositories(
	context context.Context,
	userID uuid.UUID,
	teamID uuid.UUID) (PaginatedRepos, error) {

	sql := `
			SELECT r.id, r.name, r.category, r.host
			FROM repository r
			WHERE r.teamId = $1
			AND EXISTS (
				SELECT 1 
				FROM team_member t
				WHERE t.teamId = $1
				AND t.userId = $2
			)
			ORDER BY r.name
		`
	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "category", "host"},
		},
		sql,
		teamID, userID,
	)

	var res PaginatedRepos
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var repo Repo

		err := rows.Scan(&repo.ID, &repo.Name, &repo.Category, &repo.Host, &res.Count)
		if err != nil {
			return res, err
		}

		res.Repos = append(res.Repos, repo)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}
