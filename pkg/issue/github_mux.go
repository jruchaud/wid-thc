package issue

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/tag"
	"wid/pkg/user"

	"github.com/go-chi/render"
	"github.com/google/go-github/github"
	"github.com/google/uuid"
	"golang.org/x/oauth2"
)

// ConvertGithubIssue convert github issues
func ConvertGithubIssue(issue *github.Issue) *Issue {
	id := strconv.Itoa(*issue.Number)

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))
	for _, label := range issue.Labels {
		tags.Add(tag.Tag(*label.Name))
	}

	return &Issue{
		ID:    id,
		Title: *issue.Title,
		URL:   *issue.HTMLURL,
		Tags:  tags,
	}
}

// SaveGithubAuth if token is correct
func SaveGithubAuth(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get("token")

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(r.Context(), ts)

	client := github.NewClient(tc)

	// list all repositories for the authenticated user
	_, response, err := client.Repositories.List(r.Context(), "", nil)
	if response.StatusCode == http.StatusUnauthorized {
		// FIXME We should return unauthorized here but wid-cbd use this value in a generic manner to disconnect the user from the ui
		// This needs to be refined at wid-cbd level. Meanwhile, let's use forbidden status to notify the client that the auth failed
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	SaveAuth(w, r)
}

// GetClientGithub get GetClientGithub connection
func GetClientGithub(w http.ResponseWriter, r *http.Request, id uuid.UUID) (*github.Client, error) {
	var client *github.Client

	_, token, _, err := GetToken(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return client, err
	}

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(r.Context(), ts)

	client = github.NewClient(tc)

	return client, nil
}

// GetRepoPathGithub get the repo path from the id
func GetRepoPathGithub(w http.ResponseWriter, r *http.Request, client *github.Client, id string) (string, string, error) {
	var owner, name string

	projectID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return owner, name, err
	}

	repo, _, err := client.Repositories.GetByID(r.Context(), projectID)
	if err != nil {
		return owner, name, err
	}

	splitedPath := strings.SplitN(*repo.FullName, "/", 2)
	owner = splitedPath[0]
	name = splitedPath[1]

	return owner, name, nil
}

// SearchGithubProjects search the projects
func SearchGithubProjects(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	q := r.URL.Query().Get("q")

	client, err := GetClientGithub(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var options = github.SearchOptions{
		ListOptions: github.ListOptions{
			PerPage: 10,
		},
	}

	searchResult, _, err := client.Search.Repositories(r.Context(), q, &options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var repoProjects RepoProjects
	for _, project := range searchResult.Repositories {
		repoProjects = append(repoProjects, RepoProject{
			ID:   strconv.FormatInt(*project.ID, 10),
			Name: *project.FullName,
		})
	}

	render.Render(w, r, repoProjects)
}

// SearchGithubIssues returns the issues
func SearchGithubIssues(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetClientGithub(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	owner, name, err := GetRepoPathGithub(w, r, client, currentProject.RepoProjectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var options = github.SearchOptions{
		ListOptions: github.ListOptions{
			PerPage: 10,
		},
	}

	searchResult, _, err := client.Search.Issues(r.Context(), q+"+repo:"+owner+"/"+name, &options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var all Issues
	for _, value := range searchResult.Issues {
		issue := ConvertGithubIssue(&value)
		all = append(all, *issue)
	}

	render.Render(w, r, all)
}

// GetGithubIssue get an issue
func GetGithubIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")
	id, err := strconv.ParseInt(q[1:], 10, 64)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	client, err := GetClientGithub(w, r, currentProject.RepoID)
	if err != nil {
		return
	}

	owner, name, err := GetRepoPathGithub(w, r, client, currentProject.RepoProjectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	value, _, err := client.Issues.Get(r.Context(), owner, name, int(id))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, ConvertGithubIssue(value))
}

// GetGithubProject returns the project
func GetGithubProject(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	client, err := GetClientGithub(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	projectID, err := strconv.ParseInt(currentProject.RepoProjectID, 10, 64)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	projectsService, _, err := client.Repositories.GetByID(r.Context(), projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	repoProject := RepoProject{
		ID:   strconv.FormatInt(*projectsService.ID, 10),
		Name: *projectsService.FullName,
		URL:  *projectsService.HTMLURL,
	}
	render.Render(w, r, repoProject)
}

// CreateGithubIssue create an issue
func CreateGithubIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	title := r.URL.Query().Get("title")

	client, err := GetClientGithub(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	owner, name, err := GetRepoPathGithub(w, r, client, currentProject.RepoProjectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	options := &github.IssueRequest{
		Title: &title,
	}
	issueService, _, err := client.Issues.Create(r.Context(), owner, name, options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	issue := *ConvertGithubIssue(issueService)
	render.Render(w, r, issue)
}

// GetGithubActivities returns the activities
func GetGithubActivities(w http.ResponseWriter, r *http.Request, repoID uuid.UUID) (Activities, error) {
	var activities Activities

	userID, err := user.GetUserID(r)
	if err != nil {
		return activities, err
	}

	client, err := GetClientGithub(w, r, repoID)
	if err != nil {
		return activities, err
	}

	endDate, err := strconv.ParseInt(r.URL.Query().Get("endDate"), 10, 64)
	dateValue := time.Unix(endDate, 0).UTC().Format("2006-01-02")

	username, _, _, err := GetToken(w, r, repoID)
	if err != nil {
		return activities, err
	}

	var options = github.SearchOptions{
		ListOptions: github.ListOptions{
			PerPage: 100,
		},
	}

	q := fmt.Sprintf("type:issue involves:%s updated:%s", username, dateValue)
	issues, _, err := client.Search.Issues(r.Context(), q, &options)
	if err != nil {
		return activities, err
	}

	q = fmt.Sprintf("author:%s author-date:%s", username, dateValue)
	commits, _, err := client.Search.Commits(r.Context(), q, &options)
	if err != nil {
		return activities, err
	}

	// Get the current github projects
	projects := make(map[string]*project.Project)
	for _, issue := range issues.Issues {
		key := *issue.RepositoryURL
		projects[key] = &project.Project{}
	}
	for _, commit := range commits.Commits {
		key := *commit.Repository.URL
		projects[key] = &project.Project{}
	}

	// Search the github projects into wid projects
	for key := range projects {
		var p project.Project

		req, err := client.NewRequest("GET", key, nil)
		if err == nil {
			githubProject := &github.Project{}
			_, err := client.Do(r.Context(), req, githubProject)

			if err == nil {
				repoProjectId := strconv.Itoa(int(*githubProject.ID))
				p, _ = project.SelectFromRepo(r.Context(), userID, repoID, repoProjectId)

				if p.Name == "" {
					p = project.Project{Name: *githubProject.Name}
				}
			}
		}

		if p.Name == "" {
			p = project.Project{Name: "<error>"}
		}

		projects[key] = &p
	}

	// Convert
	for _, issue := range issues.Issues {
		activity := ConvertGithubIssueActivity(projects, &issue)
		activities = append(activities, *activity)
	}
	for _, commit := range commits.Commits {
		activity := ConvertGithubCommitActivity(projects, commit)
		activities = append(activities, *activity)
	}

	return activities, nil
}

// ConvertGitlabActivity convert gitlab event
func ConvertGithubIssueActivity(projects map[string]*project.Project, issue *github.Issue) *Activity {
	id := strconv.Itoa(*issue.Number)

	projectKey := *issue.RepositoryURL
	p := projects[projectKey]

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))
	for _, label := range issue.Labels {
		tags.Add(tag.Tag(*label.Name))
	}

	return &Activity{
		Category:     RepoCategoryGithub,
		Type:         "ISSUE",
		Title:        *issue.Title,
		Tags:         tags,
		Date:         *issue.UpdatedAt,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// ConvertGithubCommitActivity convert gitlab event
func ConvertGithubCommitActivity(projects map[string]*project.Project, commit *github.CommitResult) *Activity {
	projectKey := *commit.Repository.URL
	p := projects[projectKey]

	return &Activity{
		Category:     RepoCategoryGithub,
		Type:         "COMMIT",
		Title:        *commit.Commit.Message,
		Date:         *commit.Commit.Committer.Date,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// SyncGithubIssue synchronise the issues
func SyncGithubIssue(
	w http.ResponseWriter, r *http.Request,
	userID uuid.UUID, data *report.ValidationSheet,
) error {

	// Delete old worklogs
	oldSync, err := report.SelectAllReportSynchronization(
		r.Context(), userID, data.Period.ReportID,
		data.Period.StartDate, data.Period.EndDate,
		RepoCategoryGithub,
	)
	if err != nil {
		return err
	}

	for _, sync := range oldSync {
		client, err := GetClientGithub(w, r, sync.RepoID)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogs []string
		err = json.Unmarshal([]byte(sync.Worklogs), &worklogs)
		if err != nil {
			log.Println(err, sync.Worklogs)
			continue
		}

		var worklogsFailed []string
		for _, worklog := range worklogs {

			data := make(map[string]string)
			err = json.Unmarshal([]byte(worklog), &data)
			if err != nil {
				log.Println(err)
				continue
			}

			owner := data["owner"]
			repo := data["repo"]
			commentId, _ := strconv.ParseInt(data["commentId"], 10, 0)

			_, err = client.Issues.DeleteComment(r.Context(), owner, repo, commentId)

			if err != nil {
				log.Println(err)
				worklogsFailed = append(worklogsFailed, worklog)
			}
		}

		// Keeps only the worklogs in error
		buf, err := json.Marshal(worklogsFailed)
		if err != nil {
			log.Println(err)
			continue
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:          sync.ID,
			Worklogs:    string(buf),
			UpdatedDate: sync.UpdatedDate,
		})
		if err != nil {
			log.Println(err)
		}
	}

	// Get all data to synchronise
	tasks, err := report.SelectAllValidatedTasksToSync(
		r.Context(),
		userID,
		data.Period.ReportID,
		data.Period.StartDate,
		data.Period.EndDate,
		RepoCategoryGithub,
	)
	if err != nil {
		return err
	}

	// Create worklogs
	now := time.Now()
	worklogs := make(map[uuid.UUID][]string)

	for _, validation := range tasks {
		for _, tag := range validation.TaskTags.ToArray() {
			if tag[0] == '#' {

				client, err := GetClientGithub(w, r, validation.RepoID)
				if err != nil {
					log.Println(err)
					continue
				}

				issueID, _ := strconv.Atoi(tag[1:])
				var duration time.Duration = time.Duration(validation.Duration * float64(time.Second))
				date := data.Period.StartDate.UTC().Add(time.Duration(-data.Period.Timezone) * time.Minute).Format("2006-01-02")

				owner, repo, err := GetRepoPathGithub(w, r, client, validation.RepoProjectID)
				if err != nil {
					render.Render(w, r, httperror.ErrResponseInternalServerError(err))
					log.Println(err)
					continue
				}

				body := ":hourglass: **" + duration.String() + "** at _" + date + "_"
				comment, _, err := client.Issues.CreateComment(r.Context(), owner, repo, issueID, &github.IssueComment{
					Body: &body,
				})
				if err != nil {
					log.Println(err)
					continue
				}

				worklog := `{"owner": "` + owner + `", "repo": "` + repo + `", "commentId": "` + strconv.FormatInt(*comment.ID, 10) + `"}`

				if worklogs[validation.RepoID] == nil {
					worklogs[validation.RepoID] = []string{worklog}
				} else {
					worklogs[validation.RepoID] = append(worklogs[validation.RepoID], worklog)
				}
			}
		}
	}

	// Keep the worklogs created
	for repoID, values := range worklogs {
		syncID, _ := uuid.NewUUID()

		buf, err := json.Marshal(values)
		if err != nil {
			return err
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:           syncID,
			RepoID:       repoID,
			RepoCategory: RepoCategoryGithub,
			UpdatedDate:  now,
			StartDate:    data.Period.StartDate,
			EndDate:      data.Period.EndDate,
			ReportID:     data.Period.ReportID,
			Worklogs:     string(buf),
		})
		if err != nil {
			return err
		}
	}

	return err
}
