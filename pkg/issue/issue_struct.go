package issue

import (
	"net/http"
	"time"
	"wid/pkg/tag"

	"github.com/google/uuid"
)

// RepoCategoryGitlab gitlab category
var RepoCategoryGitlab = "gitlab"

// RepoCategoryJira jira category
var RepoCategoryJira = "jira"

// RepoCategoryGithub github category
var RepoCategoryGithub = "github"

// RepoCategoryRedmine redmine category
var RepoCategoryRedmine = "redmine"

// Repo is a repo
type Repo struct {
	ID           uuid.UUID `json:"id"`
	Category     string    `json:"category"`
	Name         string    `json:"name"`
	Host         string    `json:"host"`
	Credential   bool      `json:"credential,omitempty"`
	Notification bool      `json:"notification,omitempty"`
}

// Render the Repo
func (u Repo) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the Repo
func (u *Repo) Bind(r *http.Request) error {
	return nil
}

// Repos is list of Repo
type Repos []Repo

// Render the Repos
func (u Repos) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedRepos is paginated list of Repos
type PaginatedRepos struct {
	Repos Repos `json:"items"`
	Count int   `json:"count"`
}

// Render the PaginatedRepos
func (u PaginatedRepos) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// RepoProject is a project from repo
type RepoProject struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	URL  string `json:"url,omitempty"`
	Repo *Repo  `json:"repo,omitempty"`
}

// Render the RepoProject
func (u RepoProject) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the RepoProject
func (u *RepoProject) Bind(r *http.Request) error {
	return nil
}

// RepoProjects is list of RepoProject
type RepoProjects []RepoProject

// Render the RepoProjects
func (u RepoProjects) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Issue is an issue of repo
type Issue struct {
	ID    string   `json:"id"`
	Title string   `json:"title"`
	URL   string   `json:"url"`
	Tags  tag.Tags `json:"tags"`
}

// Render the Issue
func (u Issue) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Issues is list of Issue
type Issues []Issue

// Render the Issues
func (u Issues) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Activity is an activity
type Activity struct {
	Category     string    `json:"category"`
	Type         string    `json:"type"`
	Title        string    `json:"title"`
	Date         time.Time `json:"date"`
	Tags         tag.Tags  `json:"tags"`
	ProjectID    uuid.UUID `json:"projectId,omitempty"`
	ProjectColor string    `json:"projectColor"`
	ProjectName  string    `json:"projectName"`
}

// Activities is list of Activity
type Activities []Activity

// Render the Activities
func (u Activities) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
