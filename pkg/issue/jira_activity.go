package issue

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"

	"github.com/andygrunwald/go-jira"
)

// GetActivity extends jira lib
func ActivityStreams(c *jira.Client, username string, startDate int64, endDate int64) (*Feed, *jira.Response, error) {
	apiEndpoint := fmt.Sprintf("activity?streams=user+IS+%s&streams=update-date+BETWEEN+%d+%d&issues=activity+IS+issue:close+issue:update+issue:reopen+issue:resolve+issue:transition&maxResults=1000", username, startDate, endDate)
	req, err := c.NewRequest("GET", apiEndpoint, nil)
	if err != nil {
		return nil, nil, err
	}

	result := new(Feed)
	resp, err := DoXml(c, req, result)
	if err != nil {
		jerr := jira.NewJiraError(resp, err)
		return nil, resp, jerr
	}

	return result, resp, nil
}

// GetJiraUsername get the current username
func GetJiraUsername(c *jira.Client) (string, *jira.Response, error) {
	req, err := c.NewRequest("GET", "", nil)
	if err != nil {
		return "", nil, err
	}

	resp, err := c.Do(req, nil)
	if err != nil {
		return "", nil, err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", nil, err
	}

	// https://confluence.atlassian.com/cloud/blog/2018/06/say-goodbye-to-usernames-in-atlassian-cloud
	// https://community.atlassian.com/t5/Jira-questions/How-do-I-filter-Activity-Stream-by-a-username-with-spaces-in-it/qaq-p/786659
	// https://community.atlassian.com/t5/Jira-Software-questions/How-do-I-see-what-my-username-is/qaq-p/913324
	// <meta name="ajs-remote-user" content="ug:66538b79-2f61-4a11-8411-1721aa73348a">
	r := regexp.MustCompile(`<meta name="ajs-remote-user" content="([^"]+)">`)
	username := r.FindStringSubmatch(string(data))[1]
	return string(username), resp, err
}

func DoXml(c *jira.Client, req *http.Request, v interface{}) (*jira.Response, error) {
	httpResp, err := c.Do(req, nil)
	if err != nil {
		return nil, err
	}

	if v != nil {
		// Open a NewDecoder and defer closing the reader only if there is a provided interface to decode to
		defer httpResp.Body.Close()

		err = xml.NewDecoder(httpResp.Body).Decode(v)
	}

	return httpResp, err
}

type Feed struct {
	Id      string  `xml:"id"`
	Entries []Entry `xml:"entry"`
}

type Entry struct {
	Id        string         `xml:"id"`
	Title     string         `xml:"title"`
	Published string         `xml:"published"`
	Updated   string         `xml:"updated"`
	Verb      string         `xml:"verb"`
	Category  Category       `xml:"category"`
	Object    ActivityObject `xml:"object"`
	Target    ActivityObject `xml:"target"`
}

type Category struct {
	Term string `xml:"term,attr"`
}

type ActivityObject struct {
	Id      string `xml:"id"`
	Title   string `xml:"title"`
	Summary string `xml:"summary"`
	Type    string `xml:"object-type"`
}

// ActivityObjectTypeIssue type of issue
var ActivityObjectTypeIssue = "http://streams.atlassian.com/syndication/types/issue"
