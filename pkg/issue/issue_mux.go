package issue

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// GetRepos get all repos available
func GetRepos(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	repos, err := SelectAllRepos(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, repos)
}

// SaveAuth save the auth token
func SaveAuth(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	username := r.URL.Query().Get("username")
	token := r.URL.Query().Get("token")

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsertToken(r.Context(), id, userID, "{\"username\":\""+username+"\",\"token\":\""+token+"\"}")
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SaveNotification save the notification
func SaveNotification(w http.ResponseWriter, r *http.Request) {
	data := &Repo{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpdateRepoNotification(r.Context(), data.ID, userID, data.Notification)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RemoveAuth remove the auth token
func RemoveAuth(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteToken(r.Context(), id, userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetToken get the auth token
func GetToken(w http.ResponseWriter, r *http.Request, id uuid.UUID) (string, string, string, error) {
	var username string
	var token string
	var host string

	userID, err := user.GetUserID(r)
	if err != nil {
		return username, token, host, err
	}

	auth, host, err := SelectToken(r.Context(), id, userID)
	if err != nil {
		return username, token, host, err
	}

	var values map[string]string
	err = json.NewDecoder(strings.NewReader(auth)).Decode(&values)
	if err != nil {
		return username, token, host, err
	}

	username = values["username"]
	token = values["token"]

	return username, token, host, nil
}

// SaveProject save the repo into the project
func SaveProject(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	data := &RepoProject{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsertProject(r.Context(), userID, projectID, *data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RemoveProject remove the repo into the project
func RemoveProject(w http.ResponseWriter, r *http.Request) {
	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteProject(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetCurrentProject get the current project
func GetCurrentProject(w http.ResponseWriter, r *http.Request) (*project.Project, error) {
	var currentProject project.Project

	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return &currentProject, err
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return &currentProject, err
	}

	currentProject, err = project.Select(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return &currentProject, err
	}
	if currentProject.RepoID == (uuid.UUID{}) {
		err = errors.New("No repo")
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return &currentProject, err
	}

	return &currentProject, nil
}

// GetProject get project
func GetProject(w http.ResponseWriter, r *http.Request) {
	currentProject, err := GetCurrentProject(w, r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if currentProject.RepoCategory == RepoCategoryGitlab {
		GetGitlabProject(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryJira {
		GetJiraProject(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryGithub {
		GetGithubProject(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryRedmine {
		GetRedmineProject(w, r, currentProject)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SearchIssues search issues
func SearchIssues(w http.ResponseWriter, r *http.Request) {
	currentProject, err := GetCurrentProject(w, r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if currentProject.RepoCategory == RepoCategoryGitlab {
		SearchGitlabIssues(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryJira {
		SearchJiraIssues(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryGithub {
		SearchGithubIssues(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryRedmine {
		SearchRedmineIssues(w, r, currentProject)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetIssue get issue
func GetIssue(w http.ResponseWriter, r *http.Request) {
	currentProject, err := GetCurrentProject(w, r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if currentProject.RepoCategory == RepoCategoryGitlab {
		GetGitlabIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryJira {
		GetJiraIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryGithub {
		GetGithubIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryRedmine {
		GetRedmineIssue(w, r, currentProject)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// CreateIssue create issue
func CreateIssue(w http.ResponseWriter, r *http.Request) {
	currentProject, err := GetCurrentProject(w, r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if currentProject.RepoCategory == RepoCategoryGitlab {
		CreateGitlabIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryJira {
		CreateJiraIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryGithub {
		CreateGithubIssue(w, r, currentProject)
		return
	}

	if currentProject.RepoCategory == RepoCategoryRedmine {
		CreateRedmineIssue(w, r, currentProject)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// AddRepository add a repository to a team
func AddRepository(w http.ResponseWriter, r *http.Request) {
	data := &Repo{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	teamID, err := uuid.Parse(chi.URLParam(r, "teamID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	if data.ID == (uuid.UUID{}) {
		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		data.ID = id
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = InsertRepository(r.Context(), userID, teamID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, data)
}

// RemoveRepository delete a repository to a team
func RemoveRepository(w http.ResponseWriter, r *http.Request) {
	repositoryID, err := uuid.Parse(chi.URLParam(r, "repoID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteRepository(r.Context(), userID, repositoryID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetRepository get a repository
func GetRepository(w http.ResponseWriter, r *http.Request) {
	repositoryID, err := uuid.Parse(chi.URLParam(r, "repoID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	repos, err := SelectRepository(r.Context(), userID, repositoryID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, repos)
}

// GetRepositories get all repositories for a team
func GetRepositories(w http.ResponseWriter, r *http.Request) {
	teamID, err := uuid.Parse(chi.URLParam(r, "teamID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	repos, err := SelectAllRepositories(r.Context(), userID, teamID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, repos)
}

// GetActivities get activites
func GetActivities(w http.ResponseWriter, r *http.Request) {

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	repos, err := SelectAllAvailableRepos(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var result Activities
	for _, repo := range repos {
		if repo.Category == RepoCategoryGitlab {
			gitlabAvtivities, _ := GetGitlabActivities(w, r, repo.ID)
			result = append(result, gitlabAvtivities...)
		}
		if repo.Category == RepoCategoryJira {
			jiraAvtivities, _ := GetJiraActivities(w, r, repo.ID)
			result = append(result, jiraAvtivities...)
		}
		if repo.Category == RepoCategoryGithub {
			githubAvtivities, _ := GetGithubActivities(w, r, repo.ID)
			result = append(result, githubAvtivities...)
		}
		if repo.Category == RepoCategoryRedmine {
			redmineAvtivities, _ := GetRedmineActivities(w, r, repo.ID)
			result = append(result, redmineAvtivities...)
		}
	}

	render.Render(w, r, result)
}

func SyncReportValidation(w http.ResponseWriter, r *http.Request) {
	data := &report.ValidationSheet{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = SaveReportValidation(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = SyncGitlabIssue(w, r, userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	err = SyncJiraIssue(w, r, userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	err = SyncGithubIssue(w, r, userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	err = SyncRedmineIssue(w, r, userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
}

// SaveReportValidation save the validation sheet
func SaveReportValidation(context context.Context, userID uuid.UUID, data *report.ValidationSheet) error {
	err := report.DeleteReportValidationTime(context,
		userID,
		data.Period.ReportID, data.Period.StartDate, data.Period.EndDate,
	)
	if err != nil {
		return err
	}

	err = report.InsertReportValidationTime(context,
		userID,
		data.Period.ReportID, data.Period.StartDate, data.Period.EndDate,
		data.Tasks,
	)

	return err
}
