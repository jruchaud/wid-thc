package issue

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/tag"
	"wid/pkg/user"

	"github.com/go-chi/render"
	"github.com/google/uuid"
	"github.com/xanzy/go-gitlab"
)

// ConvertGitlabIssue convert gitlab issues
func ConvertGitlabIssue(issue gitlab.Issue) *Issue {
	id := strconv.Itoa(issue.IID)

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))
	for _, label := range issue.Labels {
		tags.Add(tag.Tag(label))
	}

	return &Issue{
		ID:    id,
		Title: issue.Title,
		URL:   issue.WebURL,
		Tags:  tags,
	}
}

// SaveGitlabAuth if token is correct
func SaveGitlabAuth(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	host, err := SelectHost(r.Context(), id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	token := r.URL.Query().Get("token")

	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(host+"/api/v4"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	_, response, err := client.Version.GetVersion()
	if response.StatusCode == http.StatusUnauthorized {
		// FIXME We should return unauthorized here but wid-cbd use this value in a generic manner to disconnect the user from the ui
		// This needs to be refined at wid-cbd level. Meanwhile, let's use forbidden status to notify the client that the auth failed
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	SaveAuth(w, r)
}

// GetClientGitlab get gitlab connection
func GetClientGitlab(w http.ResponseWriter, r *http.Request, id uuid.UUID) (*gitlab.Client, error) {
	var client *gitlab.Client

	_, token, host, err := GetToken(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return client, err
	}

	client, err = gitlab.NewClient(token, gitlab.WithBaseURL(host))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return client, err
	}

	return client, nil
}

// SearchGitlabProjects search the projects
func SearchGitlabProjects(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.URL.Query().Get("id"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	q := r.URL.Query().Get("q")

	client, err := GetClientGitlab(w, r, id)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var options gitlab.ListProjectsOptions
	options.Search = &q

	projects, _, err := client.Projects.ListProjects(&options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var repoProjects RepoProjects
	for _, project := range projects {
		repoProjects = append(repoProjects, RepoProject{
			ID:   strconv.Itoa(project.ID),
			Name: project.NameWithNamespace,
		})
	}

	render.Render(w, r, repoProjects)
}

// SearchGitlabIssues returns the issues
func SearchGitlabIssues(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetClientGitlab(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	options := &gitlab.SearchOptions{}
	values, _, err := client.Search.IssuesByProject(currentProject.RepoProjectID, q, options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var all Issues
	for _, value := range values {
		issue := ConvertGitlabIssue(*value)
		all = append(all, *issue)
	}

	render.Render(w, r, all)
}

// GetGitlabIssue get an issue
func GetGitlabIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	q := r.URL.Query().Get("q")

	client, err := GetClientGitlab(w, r, currentProject.RepoID)
	if err != nil {
		return
	}

	options := &gitlab.SearchOptions{}
	values, _, err := client.Search.IssuesByProject(currentProject.RepoProjectID, q, options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	var all Issues
	for _, value := range values {
		issue := ConvertGitlabIssue(*value)
		all = append(all, *issue)
	}

	if len(all) != 0 {
		render.Render(w, r, all[0])
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetGitlabProject returns the project
func GetGitlabProject(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	client, err := GetClientGitlab(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	options := &gitlab.GetProjectOptions{}
	projectsService, _, err := client.Projects.GetProject(currentProject.RepoProjectID, options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	repoProject := RepoProject{
		ID:   strconv.Itoa(projectsService.ID),
		Name: projectsService.NameWithNamespace,
		URL:  projectsService.WebURL,
	}
	render.Render(w, r, repoProject)
}

// CreateGitlabIssue create an issue
func CreateGitlabIssue(w http.ResponseWriter, r *http.Request, currentProject *project.Project) {
	title := r.URL.Query().Get("title")

	client, err := GetClientGitlab(w, r, currentProject.RepoID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	options := &gitlab.CreateIssueOptions{
		Title: &title,
	}
	issueService, _, err := client.Issues.CreateIssue(currentProject.RepoProjectID, options)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	issue := *ConvertGitlabIssue(*issueService)
	render.Render(w, r, issue)
}

// GetGitlabActivities returns the activities
func GetGitlabActivities(w http.ResponseWriter, r *http.Request, repoID uuid.UUID) (Activities, error) {
	var activities Activities

	userID, err := user.GetUserID(r)
	if err != nil {
		return activities, err
	}

	startDate, err := strconv.ParseInt(r.URL.Query().Get("startDate"), 10, 64)
	endDate, err := strconv.ParseInt(r.URL.Query().Get("endDate"), 10, 64)

	client, err := GetClientGitlab(w, r, repoID)
	if err != nil {
		return activities, err
	}

	startDateValue := gitlab.ISOTime(time.Unix(startDate, 0).UTC())
	endDateValue := gitlab.ISOTime(time.Unix(endDate, 0).Add(24 * time.Hour).UTC())

	// Pushed events
	PUSHED := gitlab.PushedEventType
	options := &gitlab.ListContributionEventsOptions{
		Action: &PUSHED,
		Before: &endDateValue,
		After:  &startDateValue,
	}
	pushedEvents, _, err := client.Events.ListCurrentUserContributionEvents(options)
	if err != nil {
		return activities, err
	}

	// Issue events
	ISSUE := gitlab.IssueEventTargetType
	options = &gitlab.ListContributionEventsOptions{
		TargetType: &ISSUE,
		Before:     &endDateValue,
		After:      &startDateValue,
	}
	issueEvents, _, err := client.Events.ListCurrentUserContributionEvents(options)
	if err != nil {
		return activities, err
	}

	// Get the current gitlab projects
	projects := make(map[string]*project.Project)
	for _, event := range pushedEvents {
		key := strconv.Itoa(event.ProjectID)
		projects[key] = nil
	}
	for _, event := range issueEvents {
		key := strconv.Itoa(event.ProjectID)
		projects[key] = nil
	}

	// Search the gitlab projects into wid projects
	for key := range projects {
		p, err := project.SelectFromRepo(r.Context(), userID, repoID, key)

		if err != nil {
			gitlabProject, _, err := client.Projects.GetProject(key, &gitlab.GetProjectOptions{})
			if err == nil {
				p = project.Project{Name: gitlabProject.Name}
			}
		}

		if p.Name == "" {
			p = project.Project{Name: "<error>"}
		}

		projects[key] = &p
	}

	// Convert
	for _, event := range pushedEvents {
		activity := ConvertGitlabCommitActivity(projects, event)
		activities = append(activities, *activity)
	}
	for _, event := range issueEvents {
		activity := ConvertGitlabIssueActivity(projects, event)
		activities = append(activities, *activity)
	}

	return activities, nil
}

// ConvertGitlabCommitActivity convert gitlab event
func ConvertGitlabCommitActivity(projects map[string]*project.Project, event *gitlab.ContributionEvent) *Activity {

	projectKey := strconv.Itoa(event.ProjectID)
	p := projects[projectKey]

	return &Activity{
		Category:     RepoCategoryGitlab,
		Type:         "COMMIT",
		Title:        event.PushData.CommitTitle,
		Date:         *event.CreatedAt,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// ConvertGitlabIssueActivity convert gitlab event
func ConvertGitlabIssueActivity(projects map[string]*project.Project, event *gitlab.ContributionEvent) *Activity {

	projectKey := strconv.Itoa(event.ProjectID)
	p := projects[projectKey]

	id := strconv.Itoa(event.TargetIID)

	tags := tag.Tags{}
	tags.Add(tag.Tag("#" + id))

	return &Activity{
		Category:     RepoCategoryGitlab,
		Type:         "ISSUE",
		Title:        event.TargetTitle,
		Date:         *event.CreatedAt,
		Tags:         tags,
		ProjectID:    p.ID,
		ProjectName:  p.Name,
		ProjectColor: p.Color,
	}
}

// SyncGitlabIssue synchronise the issues
func SyncGitlabIssue(
	w http.ResponseWriter, r *http.Request,
	userID uuid.UUID, data *report.ValidationSheet,
) error {

	// Delete old worklogs
	oldSync, err := report.SelectAllReportSynchronization(
		r.Context(), userID, data.Period.ReportID,
		data.Period.StartDate, data.Period.EndDate,
		RepoCategoryGitlab,
	)
	if err != nil {
		return err
	}

	for _, sync := range oldSync {
		client, err := GetClientGitlab(w, r, sync.RepoID)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogs []string
		err = json.Unmarshal([]byte(sync.Worklogs), &worklogs)
		if err != nil {
			log.Println(err)
			continue
		}

		var worklogsFailed []string
		for _, worklog := range worklogs {

			data := make(map[string]string)
			err = json.Unmarshal([]byte(worklog), &data)
			if err != nil {
				log.Println(err)
				continue
			}

			pid := data["pid"]
			issue, _ := strconv.Atoi(data["issue"])
			duration := "-" + data["duration"]

			_, _, err = client.Issues.AddSpentTime(pid, issue, &gitlab.AddSpentTimeOptions{
				Duration: &duration,
			})

			if err != nil {
				log.Println(err)
				worklogsFailed = append(worklogsFailed, worklog)
			}
		}

		// Keeps only the worklogs in error
		buf, err := json.Marshal(worklogsFailed)
		if err != nil {
			log.Println(err)
			continue
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:          sync.ID,
			Worklogs:    string(buf),
			UpdatedDate: sync.UpdatedDate,
		})
		if err != nil {
			log.Println(err)
		}
	}

	// Get all data to synchronise
	tasks, err := report.SelectAllValidatedTasksToSync(
		r.Context(),
		userID,
		data.Period.ReportID,
		data.Period.StartDate,
		data.Period.EndDate,
		RepoCategoryGitlab,
	)
	if err != nil {
		return err
	}

	// Create worklogs
	now := time.Now()
	worklogs := make(map[uuid.UUID][]string)

	for _, validation := range tasks {
		for _, tag := range validation.TaskTags.ToArray() {
			if tag[0] == '#' {

				client, err := GetClientGitlab(w, r, validation.RepoID)
				if err != nil {
					log.Println(err)
					continue
				}

				issueID, _ := strconv.Atoi(tag[1:])
				duration := strconv.FormatFloat(validation.Duration/60, 'f', 2, 64) + "m"

				_, _, err = client.Issues.AddSpentTime(validation.RepoProjectID, issueID, &gitlab.AddSpentTimeOptions{
					Duration: &duration,
				})
				if err != nil {
					log.Println(err)
					continue
				}

				worklog := `{"pid": "` + validation.RepoProjectID + `", "issue": "` + tag[1:] + `", "duration": "` + duration + `"}`

				if worklogs[validation.RepoID] == nil {
					worklogs[validation.RepoID] = []string{worklog}
				} else {
					worklogs[validation.RepoID] = append(worklogs[validation.RepoID], worklog)
				}
			}
		}
	}

	// Keep the worklogs created
	for repoID, values := range worklogs {
		syncID, _ := uuid.NewUUID()

		buf, err := json.Marshal(values)
		if err != nil {
			return err
		}

		err = report.UpInsertReportSynchronization(r.Context(), userID, report.ReportSync{
			ID:           syncID,
			RepoID:       repoID,
			RepoCategory: RepoCategoryGitlab,
			UpdatedDate:  now,
			StartDate:    data.Period.StartDate,
			EndDate:      data.Period.EndDate,
			ReportID:     data.Period.ReportID,
			Worklogs:     string(buf),
		})
		if err != nil {
			return err
		}
	}

	return err
}
