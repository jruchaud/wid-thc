package report

import (
	"context"
	goSql "database/sql"
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"
	"wid/pkg/selector"
	"wid/pkg/tag"
	"wid/pkg/user"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

// UpInsertReport insert a new Report in the database
func UpInsertReport(context context.Context,
	userID uuid.UUID,
	report *Report) error {
	db := wcontext.GetDatabase(context)

	if report.StartDate.IsZero() || report.EndDate.IsZero() {
		sql := (`
		INSERT INTO report AS r (
			id, name, description, frequency, 
			durationDisplayModeExactDuration, durationDisplayModeHoursPerDay, durationDisplayModePortionOfTotal, 
			tags, ownerId, dataVisualisations, 
			pinning, synchronizable
		) 
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
		ON CONFLICT (id) DO UPDATE SET 
			name=excluded.name, description=excluded.description, frequency=excluded.frequency, 
			durationDisplayModeExactDuration = excluded.durationDisplayModeExactDuration, 
			durationDisplayModeHoursPerDay = excluded.durationDisplayModeHoursPerDay, 
			durationDisplayModePortionOfTotal = excluded.durationDisplayModePortionOfTotal, 
			tags=excluded.tags, dataVisualisations = excluded.dataVisualisations, 
			pinning = excluded.pinning, synchronizable = excluded.synchronizable
		WHERE 
			r.ownerId=$9
	`)
		_, err := db.Exec(context, sql,
			report.ID, report.Name, report.Description, report.Frequency,
			report.DurationDisplayModeExactDuration, report.DurationDisplayModeHoursPerDay, report.DurationDisplayModePortionOfTotal,
			report.Tags.ToArray(), userID, report.DataVisualisations,
			report.Pinning, report.Synchronizable)

		return err
	}

	sql := (`
		INSERT INTO report AS r (
			id, name, description, frequency, 
			durationDisplayModeExactDuration, durationDisplayModeHoursPerDay, durationDisplayModePortionOfTotal, 
			startDate, endDate, 
			tags, ownerId, dataVisualisations, 
			pinning, synchronizable
		) 
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
		ON CONFLICT (id) DO UPDATE SET 
			name=excluded.name, description=excluded.description, frequency=excluded.frequency, 
			durationDisplayModeExactDuration = excluded.durationDisplayModeExactDuration, 
			durationDisplayModeHoursPerDay = excluded.durationDisplayModeHoursPerDay, 
			durationDisplayModePortionOfTotal = excluded.durationDisplayModePortionOfTotal, 
			startDate = excluded.startDate, endDate= excluded.endDate, 
			tags=excluded.tags, dataVisualisations = excluded.dataVisualisations, 
			pinning = excluded.pinning, synchronizable = excluded.synchronizable
		WHERE 
			r.ownerId=$11
		`)
	_, err := db.Exec(context, sql,
		report.ID, report.Name, report.Description, report.Frequency,
		report.DurationDisplayModeExactDuration, report.DurationDisplayModeHoursPerDay, report.DurationDisplayModePortionOfTotal,
		report.StartDate, report.EndDate,
		report.Tags.ToArray(), userID, report.DataVisualisations,
		report.Pinning, report.Synchronizable)

	return err
}

// DeleteReport delete a report with all datas
func DeleteReport(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql := `
		DELETE FROM report_requested_user WHERE reportId=$1 
		AND EXISTS (SELECT 1 FROM report r WHERE r.id=$1 AND r.ownerId=$2)
	`
	_, err = tx.Exec(context, sql, reportID, userID)
	if err != nil {
		return err
	}

	sql = `
		DELETE FROM report_requested_team WHERE reportId=$1 
		AND EXISTS (SELECT 1 FROM report r WHERE r.id=$1 AND r.ownerId=$2)
	`
	_, err = tx.Exec(context, sql, reportID, userID)
	if err != nil {
		return err
	}

	sql = `
		DELETE FROM report_project WHERE reportId=$1 
		AND EXISTS (SELECT 1 FROM report r WHERE r.id=$1 AND r.ownerId=$2)
	`
	_, err = tx.Exec(context, sql, reportID, userID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM report_validation_time WHERE reportId=$1`
	_, err = tx.Exec(context, sql, reportID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM report_synchronization WHERE reportId=$1 AND owner=$2`
	_, err = tx.Exec(context, sql, reportID, userID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM report WHERE id=$1 AND ownerId=$2`
	_, err = tx.Exec(context, sql, reportID, userID)
	if err != nil {
		return err
	}

	err = tx.Commit(context)
	return err
}

// InsertReportRequestedUsers insert ReporRequestedUsers in the database
func InsertReportRequestedUsers(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	userIDs *IDs) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		INSERT INTO report_requested_user (reportId, userId) 
		(
			SELECT $1, unnest($2::UUID[])
			WHERE EXISTS (SELECT 1 FROM report WHERE ownerId=$3 AND id=$1)
		)
	`)
	_, err := db.Exec(context, sql, reportID, userIDs.ToArray(), userID)
	return err
}

// DeleteReportRequestedUsers deletes ReporRequestedUsers in the database
func DeleteReportRequestedUsers(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		DELETE FROM report_requested_user
		WHERE 
			reportId=$1 AND
			EXISTS (SELECT 1 FROM report WHERE ownerId=$2 AND id=$1)
	`)
	_, err := db.Exec(context, sql, reportID, userID)
	return err
}

// InsertReportRequestedTeams insert a ReporRequestedTeams in the database
func InsertReportRequestedTeams(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	teamIDs *IDs) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		INSERT INTO report_requested_team (reportId, teamId) 
		(
			SELECT $1, unnest($2::UUID[])
			WHERE EXISTS (SELECT 1 FROM report WHERE ownerId=$3 AND id=$1)
		)
	`)
	_, err := db.Exec(context, sql, reportID, teamIDs.ToArray(), userID)
	return err
}

// DeleteReportRequestedTeams deletes ReporRequestedTeams in the database
func DeleteReportRequestedTeams(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		DELETE FROM report_requested_team
		WHERE 
			reportId=$1 AND
			EXISTS (SELECT 1 FROM report WHERE ownerId=$2 AND id=$1)
	`)
	_, err := db.Exec(context, sql, reportID, userID)
	return err
}

// InsertReportProjects insert projects linked to a report in the database
func InsertReportProjects(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	projectIDs *IDs) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		INSERT INTO report_project (reportId, projectId)
		(
			SELECT $1, unnest($2::UUID[])
			WHERE EXISTS (SELECT 1 FROM report WHERE ownerId=$3 AND id=$1)
		)
	`)
	_, err := db.Exec(context, sql, reportID, projectIDs.ToArray(), userID)

	return err
}

// DeleteReportProjects deletes ReporProjects in the database
func DeleteReportProjects(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID) error {
	db := wcontext.GetDatabase(context)

	sql := (`
		DELETE FROM report_project
		WHERE 
			reportId=$1 AND
			EXISTS (SELECT 1 FROM report WHERE ownerId=$2 AND id=$1)
	`)
	_, err := db.Exec(context, sql, reportID, userID)
	return err
}

// SelectAllDetailedReports return all reports for a given user
func SelectAllDetailedReports(context context.Context,
	userID uuid.UUID) (PaginatedDetailedReports, error) {

	sql := (`SELECT 
			r.id, r.name, COALESCE(r.description, ''), 
			r.frequency, r.durationDisplayModeExactDuration, COALESCE(r.durationDisplayModeHoursPerDay, ''), r.durationDisplayModePortionOfTotal, r.startDate, r.endDate, 
			r.tags, r.dataVisualisations, r.pinning, r.synchronizable, r.sharedUsers,
			(
				SELECT 
					COALESCE(json_agg(json_build_object('id', p.id, 'name', p.name, 'color', p.color)) FILTER (WHERE p.id IS NOT NULL), '[]')
				FROM report_project rp
					LEFT JOIN project p ON rp.projectId = p.id
				WHERE r.id = rp.reportId
			 ) as projects,
			 
			 (
				SELECT 
					COALESCE(json_agg(json_build_object('id', wu.id, 'name', wu.name)) FILTER (WHERE wu.id IS NOT NULL), '[]')
				FROM report_requested_user rru
					LEFT JOIN wid_user wu ON rru.userId = wu.id
				WHERE r.id = rru.reportId	 	
			 ) as users,
			 
			 (
				SELECT 
					COALESCE(json_agg(json_build_object('id', t.id, 'name', t.name)) FILTER (WHERE t.id IS NOT NULL), '[]')
				FROM report_requested_team rrt
					LEFT JOIN team t ON rrt.teamId = t.id
				WHERE r.id = rrt.reportId
			 ) as teams,

			 (
				SELECT json_build_object('id',u.id, 'name',u.name, 'email',u.email) FROM wid_user u
				WHERE u.id = r.ownerId
			) as owner

		FROM report r 
		WHERE 
			r.ownerId=$1 
			OR $1 = ANY (r.sharedUsers) 
			OR EXISTS (SELECT 1 FROM team_member m WHERE m.userId=$1 AND m.teamId = ANY(r.sharedUsers)) 
		ORDER BY r.pinning ASC, r.name ASC`)

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "frequency", "pinning"},
		},
		sql,
		userID,
	)

	var res PaginatedDetailedReports
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var r Report
		var tags []string
		var pJSON, tJSON, uJSON, oJSON string
		var startDate goSql.NullTime
		var endDate goSql.NullTime

		err := rows.Scan(&r.ID, &r.Name, &r.Description,
			&r.Frequency, &r.DurationDisplayModeExactDuration, &r.DurationDisplayModeHoursPerDay, &r.DurationDisplayModePortionOfTotal, &startDate, &endDate,
			&tags, &r.DataVisualisations, &r.Pinning, &r.Synchronizable, &r.SharedUsers, &pJSON, &uJSON, &tJSON, &oJSON, &res.Count)
		if err != nil {
			return res, err
		}

		var details DetailedReport
		r.Tags = tag.NewStringTags(tags...)

		var u user.User
		err = json.NewDecoder(strings.NewReader(oJSON)).Decode(&u)
		if err != nil {
			return res, err
		}

		var owner user.TinyUser
		owner.ID = u.ID
		owner.Name = u.Name
		owner.Avatar = user.GetAvatar(u.Email)
		r.Owner = &owner
		r.Shared = u.ID != userID

		details.Report = &r

		err = json.NewDecoder(strings.NewReader(pJSON)).Decode(&details.Projects)
		if err != nil {
			return res, err
		}

		if startDate.Valid {
			r.StartDate = startDate.Time
		}
		if endDate.Valid {
			r.EndDate = endDate.Time
		}

		err = json.NewDecoder(strings.NewReader(uJSON)).Decode(&details.Users)
		if err != nil {
			return res, err
		}

		err = json.NewDecoder(strings.NewReader(tJSON)).Decode(&details.Teams)
		if err != nil {
			return res, err
		}

		res.DetailedReports = append(res.DetailedReports, details)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectDetailedReport return a particuliar report for a given user
func SelectDetailedReport(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID) (DetailedReport, error) {
	db := wcontext.GetDatabase(context)
	sql := (`SELECT 
				r.id, r.name, COALESCE(r.description, ''), 
				r.frequency, r.durationDisplayModeExactDuration, COALESCE(r.durationDisplayModeHoursPerDay, ''), r.durationDisplayModePortionOfTotal, r.startDate, r.endDate, 
				r.tags, r.dataVisualisations, r.pinning, r.synchronizable, r.sharedUsers,
				(
					SELECT 
						COALESCE(json_agg(json_build_object('id', p.id, 'name', p.name, 'color', p.color)) FILTER (WHERE p.id IS NOT NULL), '[]')
					FROM report_project rp
						LEFT JOIN project p ON rp.projectId = p.id
					WHERE r.id = rp.reportId
				 ) as projects,
				 
				 (
					SELECT 
						COALESCE(json_agg(json_build_object('id', wu.id, 'name', wu.name)) FILTER (WHERE wu.id IS NOT NULL), '[]')
					FROM report_requested_user rru
						LEFT JOIN wid_user wu ON rru.userId = wu.id
					WHERE r.id = rru.reportId	 	
				 ) as users,
				 
				 (
					SELECT 
						COALESCE(json_agg(json_build_object('id', t.id, 'name', t.name)) FILTER (WHERE t.id IS NOT NULL), '[]')
					FROM report_requested_team rrt
						LEFT JOIN team t ON rrt.teamId = t.id
					WHERE r.id = rrt.reportId
				 ) as teams,

				 (
					SELECT json_build_object('id',u.id, 'name',u.name, 'email',u.email) FROM wid_user u
					WHERE u.id = r.ownerId
				) as owner

			FROM report r 
			WHERE r.id=$1 AND 
			(
				r.ownerId=$2 
				OR $2 = ANY (r.sharedUsers) 
				OR EXISTS (SELECT 1 FROM team_member m WHERE m.userId=$2 AND m.teamId = ANY(r.sharedUsers))
			)`)

	row := db.QueryRow(context, sql, reportID, userID)

	var r Report
	var tags []string
	var pJSON, tJSON, uJSON, oJSON string
	var details DetailedReport
	var startDate goSql.NullTime
	var endDate goSql.NullTime

	err := row.Scan(&r.ID, &r.Name, &r.Description,
		&r.Frequency, &r.DurationDisplayModeExactDuration, &r.DurationDisplayModeHoursPerDay, &r.DurationDisplayModePortionOfTotal, &startDate, &endDate,
		&tags, &r.DataVisualisations, &r.Pinning, &r.Synchronizable, &r.SharedUsers, &pJSON, &uJSON, &tJSON, &oJSON)

	if err != nil {
		return details, err
	}

	if startDate.Valid {
		r.StartDate = startDate.Time
	}
	if endDate.Valid {
		r.EndDate = endDate.Time
	}

	r.Tags = tag.NewStringTags(tags...)

	var u user.User
	err = json.NewDecoder(strings.NewReader(oJSON)).Decode(&u)
	if err != nil {
		return details, err
	}

	var owner user.TinyUser
	owner.ID = u.ID
	owner.Name = u.Name
	owner.Avatar = user.GetAvatar(u.Email)
	r.Owner = &owner
	r.Shared = u.ID != userID

	details.Report = &r

	err = json.NewDecoder(strings.NewReader(pJSON)).Decode(&details.Projects)
	if err != nil {
		return details, err
	}

	err = json.NewDecoder(strings.NewReader(uJSON)).Decode(&details.Users)
	if err != nil {
		return details, err
	}

	err = json.NewDecoder(strings.NewReader(tJSON)).Decode(&details.Teams)

	return details, err
}

// SelectAllRequests returns all the report requests for a given user
func SelectAllRequests(context context.Context,
	userID uuid.UUID) (PaginatedRequests, error) {

	sqlLate := `
		SELECT count(c.*)
		FROM (
			SELECT DISTINCT 
				unnest(ARRAY[
					(
						SELECT (
							CASE 
								WHEN r.frequency='year' 
									THEN EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='week' 
									THEN EXTRACT(WEEK FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='day' 
									THEN EXTRACT(DOY FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='month' 
									THEN EXTRACT(MONTH FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								ELSE '1'
							END  
						)
					),
					(
						SELECT (
							CASE 
								WHEN r.frequency='year' 
									THEN EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='week' 
									THEN EXTRACT(WEEK FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='day' 
									THEN EXTRACT(DOY FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='month' 
									THEN EXTRACT(MONTH FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								ELSE '1'
							END  
						)
					)
				])
			
			FROM task t 
			JOIN project p ON t.project = p.id
			JOIN report_project rp ON rp.projectId = p.id
			
			WHERE 
			t.owner = $1
			AND rp.reportId = r.id
			AND (
				r.tags && t.tags
				OR r.tags IS NULL 
				OR array_length(r.tags, 1)=0
			) 
			AND NOT EXISTS (
				SELECT 1 
				FROM report_validation_time rt 
				WHERE 
				rt.reportId = r.id 
				AND rt.userId = t.owner
				AND rt.startDate <= t.endDate
				AND rt.endDate >= t.startDate
			)
		) AS c
	`

	sql := fmt.Sprintf(`SELECT 
			r.id, r.name, COALESCE(r.description, ''), 
			r.frequency, r.durationDisplayModeExactDuration, COALESCE(r.durationDisplayModeHoursPerDay, ''), r.durationDisplayModePortionOfTotal, r.startDate, r.endDate, 
			r.tags, r.dataVisualisations, r.synchronizable,
			(%s) as count,

			(
				SELECT 
					COALESCE(json_agg(json_build_object('id', p.id, 'name', p.name, 'color', p.color)) FILTER (WHERE p.id IS NOT NULL), '[]')
				FROM report_project rp
					LEFT JOIN project p ON rp.projectId = p.id
				WHERE r.id = rp.reportId
			 ) as projects,

			 (
				SELECT json_build_object('id',u.id, 'name',u.name, 'email',u.email) FROM wid_user u
				WHERE u.id = r.ownerId
			) as owner

			FROM report r
			WHERE EXISTS (SELECT 1 FROM report_requested_user rru WHERE rru.reportId=r.id AND rru.userId=$1)
			OR EXISTS (SELECT 1 FROM report_requested_team rrt 
								JOIN team_member tm ON 
								tm.teamId=rrt.teamId 
								WHERE rrt.reportId=r.id AND tm.userId=$1)
			OR (r.synchronizable AND r.ownerId=$1)
			ORDER BY r.name ASC
			
		`, sqlLate)

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "frequency"},
		},
		sql,
		userID,
	)

	var res PaginatedRequests
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var tags []string
		var req Request
		var r Report
		var startDate goSql.NullTime
		var endDate goSql.NullTime
		var pJSON, oJSON string

		err := rows.Scan(&r.ID, &r.Name, &r.Description,
			&r.Frequency, &r.DurationDisplayModeExactDuration, &r.DurationDisplayModeHoursPerDay, &r.DurationDisplayModePortionOfTotal, &startDate, &endDate,
			&tags, &r.DataVisualisations, &r.Synchronizable, &req.ValidationSheetsLateCount, &pJSON, &oJSON, &res.Count)

		if err != nil {
			return res, err
		}

		if startDate.Valid {
			r.StartDate = startDate.Time
		}
		if endDate.Valid {
			r.EndDate = endDate.Time
		}

		r.Tags = tag.NewStringTags(tags...)

		var u user.User
		err = json.NewDecoder(strings.NewReader(oJSON)).Decode(&u)
		if err != nil {
			return res, err
		}

		var owner user.TinyUser
		owner.ID = u.ID
		owner.Name = u.Name
		owner.Avatar = user.GetAvatar(u.Email)
		r.Owner = &owner

		req.Report = &r

		err = json.NewDecoder(strings.NewReader(pJSON)).Decode(&req.Projects)
		if err != nil {
			return res, err
		}

		res.Requests = append(res.Requests, req)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// UpdateReportPinning pin or unpin a report in database
func UpdateReportPinning(context context.Context,
	reportID uuid.UUID,
	userID uuid.UUID,
	pinning string) error {
	db := wcontext.GetDatabase(context)

	sql := "UPDATE report SET pinning=$1 WHERE id=$2 AND ownerId=$3"
	_, err := db.Exec(context, sql, pinning, reportID, userID)

	return err
}

// SelectRequestsCount select the number of late requests
func SelectRequestsCount(context context.Context,
	userID uuid.UUID) (int64, error) {

	db := wcontext.GetDatabase(context)

	sql := `
		SELECT count(c.*)
		FROM (
			SELECT DISTINCT 
				unnest(ARRAY[
					(
						SELECT (
							CASE 
								WHEN r.frequency='year' 
									THEN EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='week' 
									THEN EXTRACT(WEEK FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='day' 
									THEN EXTRACT(DOY FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								WHEN r.frequency='month' 
									THEN EXTRACT(MONTH FROM t.startDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.startDate)::TEXT
								ELSE '1'
							END  
						)
					),
					(
						SELECT (
							CASE 
								WHEN r.frequency='year' 
									THEN EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='week' 
									THEN EXTRACT(WEEK FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='day' 
									THEN EXTRACT(DOY FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								WHEN r.frequency='month' 
									THEN EXTRACT(MONTH FROM t.endDate)::TEXT || ' ' || EXTRACT(YEAR FROM t.endDate)::TEXT
								ELSE '1'
							END  
						)
					)
				])
			
			FROM task t 
			JOIN project p ON t.project = p.id
			JOIN report_project rp ON rp.projectId = p.id
			JOIN report r ON rp.reportId = r.id
			
			WHERE 
			t.owner = $1
			AND 
			(
				EXISTS (SELECT 1 FROM report_requested_user rru WHERE rru.reportId=r.id AND rru.userId=$1)
				OR EXISTS (SELECT 1 FROM report_requested_team rrt 
								JOIN team_member tm ON 
								tm.teamId=rrt.teamId 
								WHERE rrt.reportId=r.id AND tm.userId=$1)
				OR (r.synchronizable AND r.ownerId=$1)
			)
			AND (
				r.tags && t.tags
				OR r.tags IS NULL 
				OR array_length(r.tags, 1)=0
			) 
			AND NOT EXISTS (
				SELECT 1 
				FROM report_validation_time rt 
				WHERE 
				rt.reportId = r.id 
				AND rt.userId = t.owner
				AND rt.startDate <= t.endDate
				AND rt.endDate >= t.startDate
			)
		) AS c
	`

	var count int64
	rows := db.QueryRow(context, sql, userID)
	err := rows.Scan(&count)
	return count, err
}

// SelectRequest select one request according to the user and report ids
func SelectRequest(context context.Context,
	userID uuid.UUID, reportID uuid.UUID) (Request, error) {
	db := wcontext.GetDatabase(context)

	sql := (`SELECT 
			r.id, r.name, COALESCE(r.description, ''), 
			r.frequency, r.durationDisplayModeExactDuration, COALESCE(r.durationDisplayModeHoursPerDay, ''), r.durationDisplayModePortionOfTotal, r.startDate, r.endDate, 
			r.tags, r.dataVisualisations, r.synchronizable,
			(
				SELECT 
					COALESCE(json_agg(json_build_object('id', p.id, 'name', p.name, 'color', p.color)) FILTER (WHERE p.id IS NOT NULL), '[]')
				FROM report_project rp
					LEFT JOIN project p ON rp.projectId = p.id
				WHERE r.id = rp.reportId
			 ) as projects,

			 (
				SELECT json_build_object('id',u.id, 'name',u.name, 'email',u.email) FROM wid_user u
				WHERE u.id = r.ownerId
			) as owner

			FROM report r
			WHERE r.id=$2			
			AND 
			(
				EXISTS (SELECT 1 FROM report_requested_user rru WHERE rru.reportId=r.id AND rru.userId=$1)
				OR EXISTS (SELECT 1 FROM report_requested_team rrt 
								JOIN team_member tm ON 
								tm.teamId=rrt.teamId 
								WHERE rrt.reportId=r.id AND tm.userId=$1)
				OR (r.synchronizable AND r.ownerId=$1)
			)
			ORDER BY r.name ASC
		`)

	row := db.QueryRow(context, sql, userID, reportID)

	var tags []string
	var req Request
	var r Report
	var startDate goSql.NullTime
	var endDate goSql.NullTime
	var pJSON, oJSON string

	err := row.Scan(&r.ID, &r.Name, &r.Description,
		&r.Frequency, &r.DurationDisplayModeExactDuration, &r.DurationDisplayModeHoursPerDay, &r.DurationDisplayModePortionOfTotal, &startDate, &endDate,
		&tags, &r.DataVisualisations, &r.Synchronizable, &pJSON, &oJSON)
	if err != nil {
		return req, err
	}

	if startDate.Valid {
		r.StartDate = startDate.Time
	}
	if endDate.Valid {
		r.EndDate = endDate.Time
	}

	r.Tags = tag.NewStringTags(tags...)

	var u user.User
	err = json.NewDecoder(strings.NewReader(oJSON)).Decode(&u)
	if err != nil {
		return req, err
	}

	var owner user.TinyUser
	owner.ID = u.ID
	owner.Name = u.Name
	owner.Avatar = user.GetAvatar(u.Email)
	r.Owner = &owner

	req.Report = &r

	err = json.NewDecoder(strings.NewReader(pJSON)).Decode(&req.Projects)
	if err != nil {
		return req, err
	}

	return req, nil

}

// SelectLateValidationSheet returns the user have not validate
func SelectLateValidationSheet(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time) (user.TinyUsers, error) {

	db := wcontext.GetDatabase(context)

	sqlLate := `
		SELECT DISTINCT t.owner
		
		FROM task t 
		JOIN project p ON t.project = p.id
		JOIN report_project rp ON rp.projectId = p.id
		JOIN report r ON r.id = rp.reportId
		
		WHERE 
		r.id = $1
		AND r.ownerId = $2
		AND (
			r.tags && t.tags
			OR r.tags IS NULL 
			OR array_length(r.tags, 1)=0
		) 
		AND t.startDate < $4
		AND t.endDate > $3
		AND t.endDate <> t.startdate
		AND NOT EXISTS (
			SELECT 1 
			FROM report_validation_time rt 
			WHERE 
			rt.reportId = r.id 
			AND rt.userId = t.owner
			AND rt.startDate <= t.endDate
			AND rt.endDate >= t.startDate
		)
		AND (
			EXISTS (SELECT 1 FROM report_requested_user rru WHERE rru.reportId=r.id AND rru.userId=t.owner)
			OR EXISTS (SELECT 1 FROM report_requested_team rrt 
								JOIN team_member tm ON 
								tm.teamId=rrt.teamId 
								WHERE rrt.reportId=r.id AND tm.userId=t.owner)
			OR (r.synchronizable AND r.ownerId=$2)
		)
	`

	sql := fmt.Sprintf(`
		SELECT u.id, u.name, u.email
		FROM wid_user u
		WHERE u.id IN (%s)
	`, sqlLate)

	rows, err := db.Query(context, sql, reportID, userID, startDate, endDate)
	var res user.TinyUsers
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var u user.TinyUser
		var email string

		err := rows.Scan(&u.ID, &u.Name, &email)
		if err != nil {
			return res, err
		}
		u.Avatar = user.GetAvatar(email)

		res = append(res, u)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectAllReportedTasks returns all the reported tasks to display a visualisation sheet
func SelectAllReportedTasks(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time) (ReportedTasks, error) {
	db := wcontext.GetDatabase(context)

	sql := (`SELECT 
				t.title, 
				CASE
					WHEN r.tags IS NULL
						THEN t.tags
					ELSE array_intersection(t.tags, r.tags)
				END AS tags, 
				p.id, p.name, p.color,
				
				EXTRACT(EPOCH FROM
					COALESCE(
						SUM(LEAST(t.endDate, $4) - GREATEST(t.startdate, $3)
					), '0'::interval)
				) AS duration,

				(
					EXISTS (
						SELECT 1 
						FROM report_validation_time rt 
						WHERE 
						rt.reportId = r.id 
						AND rt.userId = t.owner
						AND rt.projectId = t.project
						AND rt.taskTitle = t.title
						AND (rt.taskTags && t.tags OR t.tags IS NULL OR array_length(r.tags, 1)=0)
						AND rt.startDate <= $4
						AND rt.endDate >= $3
					)
				) AS validated

			FROM 
				task t
				JOIN project p ON p.id = t.project 
				JOIN report_project rp ON rp.projectId=p.id 
				JOIN report r ON r.id=rp.reportId
			WHERE 
				t.owner = $1 
				AND rp.reportId=$2
				AND (
					r.tags && t.tags
					OR r.tags IS NULL 
					OR array_length(r.tags, 1)=0
				)
				AND t.startDate < $4
				AND t.endDate > $3
				AND t.endDate <> t.startdate
			GROUP BY 
				t.title, t.tags, r.tags, p.id, p.name, p.color, r.id, t.owner, t.project
		`)
	rows, err := db.Query(context, sql, userID, reportID, startDate, endDate)

	var res ReportedTasks
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var rep ReportedTask
		var tags []string

		err := rows.Scan(
			&rep.TaskTitle, &tags, &rep.ProjectID,
			&rep.ProjectName, &rep.ProjectColor,
			&rep.Duration, &rep.Validated,
		)
		if err != nil {
			return res, err
		}

		rep.TaskTags = tag.NewStringTags(tags...)

		res = append(res, rep)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// InsertReportValidationTime inserts all validated time from reported tasks
func InsertReportValidationTime(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time,
	reportedTasks ReportedTasks) error {
	db := wcontext.GetDatabase(context)

	rows := [][]interface{}{}

	for _, rt := range reportedTasks {
		id, err := uuid.NewUUID()
		if err != nil {
			return err
		}

		row := []interface{}{
			id,
			startDate,
			endDate,
			rt.TaskTags.ToArray(),
			rt.TaskTitle,
			rt.Duration,
			userID,
			reportID,
			rt.ProjectID}
		rows = append(rows, row)
	}

	_, err := db.CopyFrom(
		context,
		pgx.Identifier{"report_validation_time"},
		[]string{"id", "startdate", "enddate", "tasktags", "tasktitle", "duration", "userid", "reportid", "projectid"},
		pgx.CopyFromRows(rows),
	)
	return err
}

// DeleteReportValidationTime delete old validated tasks
func DeleteReportValidationTime(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time) error {
	db := wcontext.GetDatabase(context)

	sql := (`
			DELETE FROM report_validation_time
			WHERE 
				userId=$1 AND reportId=$2 AND
				startDate < $4 AND endDate > $3
		`)
	_, err := db.Exec(context, sql, userID, reportID, startDate, endDate)
	return err
}

// SelectAllValidatedTasks returns all the validated tasks to display a visualisation sheet
func SelectAllValidatedTasks(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time) (ReportedTasks, error) {
	db := wcontext.GetDatabase(context)

	sql := (`SELECT 
				rvt.taskTitle, rvt.taskTags, SUM(rvt.duration), rvt.projectId, p.name, p.color
			FROM 
				report_validation_time rvt
				JOIN project p ON p.id = rvt.projectId 
			WHERE
				rvt.userId = $1
				AND rvt.reportId = $2
				AND rvt.startDate >= $3
				AND rvt.endDate <= $4
			GROUP BY rvt.taskTitle, rvt.taskTags, rvt.projectId, p.name, p.color
		`)
	rows, err := db.Query(context, sql, userID, reportID, startDate, endDate)

	var res ReportedTasks
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var rep ReportedTask
		var tags []string

		err := rows.Scan(&rep.TaskTitle, &tags, &rep.Duration, &rep.ProjectID, &rep.ProjectName, &rep.ProjectColor)
		if err != nil {
			return res, err
		}

		rep.TaskTags = tag.NewStringTags(tags...)

		res = append(res, rep)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

var sqlTasks string = `
	SELECT 
		t.title,
		CASE
			WHEN r.tags IS NULL
				THEN t.tags
			ELSE array_intersection(t.tags, r.tags)
		END AS tags, 
		p.name AS projectName, p.color AS projectColor, p.id AS projectId,
		u.id AS userId, u.name AS userName, u.email AS userEmail,
		EXTRACT(EPOCH FROM
			COALESCE(SUM(LEAST(t.endDate, $4) - GREATEST(t.startdate, $3)), '0'::interval)
		) AS duration
	FROM 
		task t
		LEFT JOIN project p ON p.id = t.project 
		JOIN wid_user u ON u.id=t.owner, 
		report r
	WHERE 
		t.owner = $1 
		AND r.id=$2
		AND (
			r.ownerId=$1 
			OR $1 = ANY (r.sharedUsers) 
			OR EXISTS (SELECT 1 FROM team_member m WHERE m.userId=$1 AND m.teamId = ANY(r.sharedUsers))
		)
		AND (
			r.tags && t.tags
			OR r.tags IS NULL 
			OR array_length(r.tags, 1)=0
		)
		AND t.startDate < $4 
		AND t.endDate > $3
		AND t.endDate <> t.startdate
		AND NOT EXISTS (SELECT 1 FROM report_requested_user u WHERE u.reportId = r.id)
		AND NOT EXISTS (SELECT 1 FROM report_requested_team t WHERE t.reportId = r.id)
		AND NOT r.synchronizable
		AND (
			NOT EXISTS (SELECT 1 FROM report_project rp WHERE rp.reportId = r.id)
			OR EXISTS (SELECT 1 FROM report_project rp WHERE rp.reportId = r.id AND rp.projectId = p.id)
		)
	GROUP BY
		t.title, t.tags, r.tags, p.id, p.name, p.color, u.id, u.name, u.email
`

var sqlSheets string = `
	SELECT
		v.taskTitle,
		CASE
			WHEN r.tags IS NULL
				THEN v.taskTags
			ELSE array_intersection(v.taskTags, r.tags)
		END AS tags,
		p.name AS projectName, p.color AS projectColor, p.id AS projectId,
		u.id AS userId, u.name AS userName, u.email AS userEmail,
		SUM(v.duration) AS duration
	FROM
		report_validation_time v
		JOIN report r ON r.id=v.reportId
		JOIN report_project rp ON r.id=rp.reportId
		JOIN project p ON p.id=rp.projectId 
		JOIN wid_user u ON u.id=v.userId 
	WHERE
		v.projectId = p.id
		AND (
			r.ownerId=$1 
			OR $1 = ANY (r.sharedUsers) 
			OR EXISTS (SELECT 1 FROM team_member m WHERE m.userId=$1 AND m.teamId = ANY(r.sharedUsers))
		)
		AND (
			r.tags && v.taskTags
			OR r.tags IS NULL 
			OR array_length(r.tags, 1)=0
		)
		AND v.startDate < $4 
		AND v.endDate > $3
		AND v.endDate <> v.startdate
		AND v.reportId = $2
		AND (
			EXISTS (SELECT 1 FROM report_requested_user u WHERE u.reportId = r.id)
			OR EXISTS (SELECT 1 FROM report_requested_team t WHERE t.reportId = r.id)
			OR (r.synchronizable AND v.userId = $1) 
		)
	GROUP BY
		v.taskTitle, v.taskTags, r.tags, p.id, p.name, p.color, u.id, u.name, u.email
`

// SelectProjectByTags returns the chart project by tag
func SelectProjectByTags(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (ChartData, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s UNION %s)

			SELECT 
				COALESCE(p.projectId::TEXT, ''),
				COALESCE(p.projectName, '{unknown}'),
				COALESCE(p.projectColor, ''),
				x.tag,
				COALESCE(
					(
						SELECT
							SUM(duration) AS duration
						FROM                                 
							available_tasks t,
							UNNEST(t.tags) u(tag)
						WHERE
							(t.projectId = p.projectId OR t.projectId IS NULL AND p.projectId IS NULL)
							AND u.tag = x.tag
					), 0
				) AS duration
			FROM 
				(
					SELECT p.id as projectId, p.name as projectName, p.color as projectColor
					FROM project p
					WHERE p.id IN (SELECT rp.projectId FROM report_project rp WHERE rp.reportId=$2)
					
					UNION
					
					SELECT tp.projectId, tp.projectName, tp.projectColor 
					FROM available_tasks tp
				) p,
				(
					SELECT 
						DISTINCT u.tag 
					FROM 
						available_tasks t,
						UNNEST(t.tags) u(tag)
				) AS x(tag)
			ORDER BY 
				p.projectName,
				x.tag
	`, sqlTasks, sqlSheets)

	rows, err := db.Query(context, sql, userID, query.ReportID, query.StartDate, query.EndDate)

	var res ChartData
	if err != nil {
		return res, err
	}
	defer rows.Close()

	data := make(map[string][]float64)
	tags := make(map[string]bool)
	names := make(map[string]string)
	colors := make(map[string][]string)

	for rows.Next() {
		var projectID string
		var projectName string
		var projectColor string
		var tag string
		var duration float64

		err := rows.Scan(&projectID, &projectName, &projectColor, &tag, &duration)
		if err != nil {
			return res, err
		}

		data[projectID] = append(data[projectID], duration)
		names[projectID] = projectName
		colors[projectID] = append(colors[projectID], projectColor)
		tags[tag] = true
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	for tag := range tags {
		res.Labels = append(res.Labels, tag)
	}
	sort.Strings(res.Labels)

	for projectID, values := range data {
		res.DataSets = append(res.DataSets, DataSets{
			Label:           names[projectID],
			Data:            values,
			BackgroundColor: colors[projectID],
		})
	}

	return res, nil
}

// SelectTotalForProject returns the chart project on the period
func SelectTotalForProject(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (ChartData, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s UNION %s)

			SELECT 
				COALESCE(p.projectName, '{unknown}'),
				COALESCE(p.projectColor, ''),
				COALESCE(
					(
						SELECT
							SUM(duration) AS duration
						FROM                                 
							available_tasks t
						WHERE
							t.projectId = p.projectId OR t.projectId IS NULL AND p.projectId IS NULL
					), 0
				) AS duration
			FROM 
				(
					SELECT p.id as projectId, p.name as projectName, p.color as projectColor
					FROM project p
					WHERE p.id IN (SELECT rp.projectId FROM report_project rp WHERE rp.reportId=$2)
					
					UNION
					
					SELECT tp.projectId, tp.projectName, tp.projectColor 
					FROM available_tasks tp
				) p
			GROUP BY 
				p.projectId,
				p.projectName,
				p.projectColor
			ORDER BY
				p.projectName
	`, sqlTasks, sqlSheets)

	rows, err := db.Query(context, sql, userID, query.ReportID, query.StartDate, query.EndDate)

	var res ChartData
	if err != nil {
		return res, err
	}
	defer rows.Close()

	var ds DataSets

	for rows.Next() {
		var project string
		var color string
		var duration float64

		err := rows.Scan(&project, &color, &duration)
		if err != nil {
			return res, err
		}

		res.Labels = append(res.Labels, project)
		ds.BackgroundColor = append(ds.BackgroundColor, color)
		ds.Data = append(ds.Data, duration)
	}
	res.DataSets = append(res.DataSets, ds)

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectTotalForTag returns the chart tag on the period
func SelectTotalForTag(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (ChartData, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s UNION %s)

			SELECT 
				u.tag,
				SUM(duration) AS duration
			FROM 
				available_tasks t,
				UNNEST(t.tags) u(tag)
			GROUP BY 
				u.tag
			ORDER BY
				u.tag
	`, sqlTasks, sqlSheets)

	rows, err := db.Query(context, sql, userID, query.ReportID, query.StartDate, query.EndDate)

	var res ChartData
	if err != nil {
		return res, err
	}
	defer rows.Close()

	var ds DataSets

	for rows.Next() {
		var tag string
		var duration float64

		err := rows.Scan(&tag, &duration)
		if err != nil {
			return res, err
		}

		res.Labels = append(res.Labels, tag)
		ds.Data = append(ds.Data, duration)
	}
	res.DataSets = append(res.DataSets, ds)

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectTotalForTask returns the chart tag on the period
func SelectTotalForTask(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (ChartData, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s UNION %s)

			SELECT 
				t.title,
				COALESCE(p.name, '{unknown}'),
				COALESCE(p.color, ''),
				SUM(duration) AS duration
			FROM 
				available_tasks t
				LEFT JOIN project p ON t.projectId = p.id
			GROUP BY 
				t.projectId,
				t.title,
				p.name,
				p.color
			ORDER BY
				p.name,
				t.title
	`, sqlTasks, sqlSheets)

	rows, err := db.Query(context, sql, userID, query.ReportID, query.StartDate, query.EndDate)

	var res ChartData
	if err != nil {
		return res, err
	}
	defer rows.Close()

	var ds DataSets

	for rows.Next() {
		var task string
		var project string
		var color string
		var duration float64

		err := rows.Scan(&task, &project, &color, &duration)
		if err != nil {
			return res, err
		}

		label := fmt.Sprintf("[%s] %s", project, task)
		res.Labels = append(res.Labels, label)
		ds.BackgroundColor = append(ds.BackgroundColor, color)
		ds.Data = append(ds.Data, duration)
	}
	res.DataSets = append(res.DataSets, ds)

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectTotalForUser returns the chart user on the period
func SelectTotalForUser(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (ChartData, error) {

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s UNION %s)

			SELECT 
				t.userName,
				SUM(duration) AS duration
			FROM 
				available_tasks t
			GROUP BY
				t.userId, 
				t.userName
			ORDER BY
				t.userName
	`, sqlTasks, sqlSheets)

	rows, err := db.Query(context, sql, userID, query.ReportID, query.StartDate, query.EndDate)

	var res ChartData
	if err != nil {
		return res, err
	}
	defer rows.Close()

	var ds DataSets

	for rows.Next() {
		var userName string
		var duration float64

		err := rows.Scan(&userName, &duration)
		if err != nil {
			return res, err
		}

		res.Labels = append(res.Labels, userName)
		ds.Data = append(ds.Data, duration)
	}
	res.DataSets = append(res.DataSets, ds)

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// Project by Period(n)/Tags/Collaborators
// Tag by Period(n)/Projects/Collaborators
// Collaborators by Period(n)/Projects/Tags/Collaborators
// Tasks by Period(n)/Projects/Tags/Collaborators

// SelectTotalDays returns the total worked days
func SelectTotalDays(context context.Context,
	userID uuid.UUID,
	query *QueryPeriod) (float64, error) {

	var sqlTasks string = `
		SELECT
			GREATEST(t.startdate, $3) startdate,
			LEAST(t.endDate, $4) endDate
		FROM 
			task t
			LEFT JOIN project p ON p.id = t.project 
			JOIN wid_user u ON u.id=t.owner, 
			report r
		WHERE 
			t.owner = $1 
			AND r.id=$2
			AND (
				r.tags && t.tags
				OR r.tags IS NULL 
				OR array_length(r.tags, 1)=0
			)
			AND t.startDate < $4 
			AND t.endDate > $3
			AND t.endDate <> t.startdate
			AND (
				NOT EXISTS (SELECT 1 FROM report_project rp WHERE rp.reportId = r.id)
				OR EXISTS (SELECT 1 FROM report_project rp WHERE rp.reportId = r.id AND rp.projectId = p.id)
			)
	`

	db := wcontext.GetDatabase(context)

	sql := fmt.Sprintf(`
		WITH
			available_tasks AS (%s),
			dates as (SELECT g.date FROM (SELECT (generate_series($3::DATE - interval '1' day, $4::DATE + interval '1' day, '1 day'::interval))::date) g(date))
			
			SELECT 
				(
					COUNT(*) FILTER (
						WHERE EXISTS(
							SELECT 1 
							FROM available_tasks 
							WHERE (startDate, endDate) OVERLAPS (d.date  + time '06:00:00' + $5, d.date + time '12:00:00' + $5)
						)
					)
					+
					COUNT(*) FILTER (
						WHERE EXISTS(
							SELECT 1 
							FROM available_tasks 
							WHERE (startDate, endDate) OVERLAPS (d.date  + time '14:00:00' + $5, d.date + time '20:00:00' + $5)
						)
					)		
				) / 2.0
				AS days
			FROM 
				dates d
	`, sqlTasks)

	rows := db.QueryRow(context, sql, userID, query.ReportID, query.StartDate, query.EndDate, strconv.Itoa(query.Timezone)+" minutes")
	var res float64
	err := rows.Scan(&res)

	return res, err
}

// SelectAllValidatedTasksToSync returns all the validated tasks to synchronise with the repo
func SelectAllValidatedTasksToSync(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time,
	repoCategory string) (SyncTasks, error) {
	db := wcontext.GetDatabase(context)

	sql := (`SELECT DISTINCT
				rvt.id, rvt.taskTags, rvt.duration, rvt.projectId, p.repoId, p.repoProjectId
			FROM 
				report_validation_time rvt
				JOIN project p ON p.id = rvt.projectId 
			WHERE
				rvt.userId = $1
				AND rvt.reportId = $2
				AND rvt.startDate >= $3
				AND rvt.endDate <= $4
				AND p.repoCategory = $5
		`)
	rows, err := db.Query(context, sql, userID, reportID, startDate, endDate, repoCategory)

	var res SyncTasks
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var rep SyncTask
		var tags []string

		err := rows.Scan(&rep.ID, &tags, &rep.Duration, &rep.ProjectID, &rep.RepoID, &rep.RepoProjectID)
		if err != nil {
			return res, err
		}

		rep.TaskTags = tag.NewStringTags(tags...)

		res = append(res, rep)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// UpInsertReportSynchronization update the synchronise data
func UpInsertReportSynchronization(context context.Context,
	userID uuid.UUID,
	sync ReportSync) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO report_synchronization(id, repoId, repoCategory, updatedDate, startDate, endDate, worklogs, reportId, owner) 
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
			ON CONFLICT (id) DO UPDATE SET updatedDate=excluded.updatedDate, worklogs=excluded.worklogs`

	_, err := db.Exec(
		context, sql,
		sync.ID, sync.RepoID, sync.RepoCategory, sync.UpdatedDate,
		sync.StartDate, sync.EndDate, sync.Worklogs, sync.ReportID,
		userID,
	)

	return err
}

// SelectAllReportSynchronization select current synchronization information
func SelectAllReportSynchronization(context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	startDate time.Time,
	endDate time.Time,
	repoCategory string) ([]ReportSync, error) {
	db := wcontext.GetDatabase(context)

	sql := (`
			SELECT id, repoId, repoCategory, updatedDate, startDate, endDate, worklogs, reportId 
			FROM report_synchronization
			WHERE 
				owner=$1 AND reportId=$2 AND
				startDate = $3 AND endDate = $4 AND
				repoCategory = $5
		`)

	rows, err := db.Query(context, sql, userID, reportID, startDate, endDate, repoCategory)

	var res []ReportSync
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var rep ReportSync

		err := rows.Scan(
			&rep.ID, &rep.RepoID, &rep.RepoCategory,
			&rep.UpdatedDate, &rep.StartDate, &rep.EndDate,
			&rep.Worklogs, &rep.ReportID,
		)
		if err != nil {
			return res, err
		}

		res = append(res, rep)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// UpdateSharedUsers a report
func UpdateSharedUsers(
	context context.Context,
	userID uuid.UUID,
	reportID uuid.UUID,
	userIDs *IDs) error {

	db := wcontext.GetDatabase(context)

	sql := `UPDATE report SET sharedUsers=$1::UUID[]
					WHERE id=$2 
					AND (
						ownerId=$3 
						OR $3 = ANY (sharedUsers) 
						OR EXISTS (SELECT 1 FROM team_member m WHERE m.userId=$3 AND m.teamId = ANY(sharedUsers))
					)`
	_, err := db.Exec(context, sql, userIDs.ToArray(), reportID, userID)

	return err
}
