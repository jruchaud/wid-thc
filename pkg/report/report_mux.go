package report

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"wid/pkg/httperror"
	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// SaveReport save a report
func SaveReport(w http.ResponseWriter, r *http.Request) {
	data := &Report{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	if data.ID == (uuid.UUID{}) {
		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		data.ID = id
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsertReport(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, data)
}

// RemoveReport remove a report
func RemoveReport(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = DeleteReport(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SetPinning pin or unpin a report
func SetPinning(w http.ResponseWriter, r *http.Request) {
	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	defer r.Body.Close()

	var pinning string
	err = json.NewDecoder(r.Body).Decode(&pinning)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = UpdateReportPinning(r.Context(), reportID, userID, pinning)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SetUsers add a users in a report
func SetUsers(w http.ResponseWriter, r *http.Request) {
	data := &IDs{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = DeleteReportRequestedUsers(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = InsertReportRequestedUsers(r.Context(), userID, reportID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SetTeams add a teams in a report
func SetTeams(w http.ResponseWriter, r *http.Request) {
	data := &IDs{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = DeleteReportRequestedTeams(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = InsertReportRequestedTeams(r.Context(), userID, reportID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SetProjects add a project in a report
func SetProjects(w http.ResponseWriter, r *http.Request) {
	data := &IDs{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = DeleteReportProjects(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = InsertReportProjects(r.Context(), userID, reportID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

//GetAllReports returns all reports for an user
func GetAllReports(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reports, err := SelectAllDetailedReports(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, reports)
}

//GetRequestCount return count of late requests
func GetRequestCount(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	count, err := SelectRequestsCount(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	content := strconv.FormatInt(count, 10)
	w.Write([]byte(content))
}

//GetReport returns a report for an user
func GetReport(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	report, err := SelectDetailedReport(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, report)
}

func getChart(
	w http.ResponseWriter,
	r *http.Request,
	dbCall func(context context.Context, userID uuid.UUID, query *QueryPeriod) (ChartData, error)) {

	data := &QueryPeriod{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	chart, err := dbCall(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	render.Render(w, r, chart)
}

// GetChartProjectByTags get the chart project by tags
func GetChartProjectByTags(w http.ResponseWriter, r *http.Request) {
	getChart(w, r, SelectProjectByTags)
}

// GetChartTotalForProject get the chart project
func GetChartTotalForProject(w http.ResponseWriter, r *http.Request) {
	getChart(w, r, SelectTotalForProject)
}

// GetChartTotalForTag get the chart tag
func GetChartTotalForTag(w http.ResponseWriter, r *http.Request) {
	getChart(w, r, SelectTotalForTag)
}

// GetChartTotalForTask get the chart task
func GetChartTotalForTask(w http.ResponseWriter, r *http.Request) {
	getChart(w, r, SelectTotalForTask)
}

// GetChartTotalForUser get the chart task
func GetChartTotalForUser(w http.ResponseWriter, r *http.Request) {
	getChart(w, r, SelectTotalForUser)
}

// GetChartTotalDays get the total worked days
func GetChartTotalDays(w http.ResponseWriter, r *http.Request) {
	data := &QueryPeriod{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	count, err := SelectTotalDays(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	content := strconv.FormatFloat(count, 'f', -1, 64)
	w.Write([]byte(content))
}

// GetRequests get the current validation sheets
func GetRequests(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	requests, err := SelectAllRequests(r.Context(), userID)

	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, requests)
}

//GetRequest returns a request for an user
func GetRequest(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	request, err := SelectRequest(r.Context(), userID, reportID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, request)
}

// GetReportedUsersLate get user as not yet reported
func GetReportedUsersLate(w http.ResponseWriter, r *http.Request) {
	data := &QueryPeriod{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	users, err := SelectLateValidationSheet(r.Context(), userID, data.ReportID, data.StartDate, data.EndDate)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, users)
}

// GetReportedTasks get tasks for a request
func GetReportedTasks(w http.ResponseWriter, r *http.Request) {
	data := &QueryPeriod{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportedTasks, err := SelectAllReportedTasks(r.Context(), userID, data.ReportID, data.StartDate, data.EndDate)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	displayedTasks := &DisplayedReportedTasks{
		Reported: reportedTasks,
	}

	validatedTasks, err := SelectAllValidatedTasks(r.Context(), userID, data.ReportID, data.StartDate, data.EndDate)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	displayedTasks.Validated = validatedTasks

	render.Render(w, r, displayedTasks)
}

// SaveReportValidation save the validation sheet
func SaveReportValidation(w http.ResponseWriter, r *http.Request) {
	data := &ValidationSheet{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteReportValidationTime(r.Context(),
		userID,
		data.Period.ReportID, data.Period.StartDate, data.Period.EndDate,
	)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = InsertReportValidationTime(r.Context(),
		userID,
		data.Period.ReportID, data.Period.StartDate, data.Period.EndDate,
		data.Tasks,
	)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// ShareReport shared a report with some users
func ShareReport(w http.ResponseWriter, r *http.Request) {
	data := &IDs{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	reportID, err := uuid.Parse(chi.URLParam(r, "reportID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = UpdateSharedUsers(r.Context(), userID, reportID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
