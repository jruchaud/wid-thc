package report

import (
	"net/http"
	"time"
	"wid/pkg/project"
	"wid/pkg/tag"
	"wid/pkg/team"
	"wid/pkg/user"

	"github.com/google/uuid"
)

// IDs list of uuid
type IDs []uuid.UUID

// ToArray returns the ids to string type
func (u IDs) ToArray() []string {
	var arr []string
	for _, id := range u {
		arr = append(arr, id.String())
	}
	return arr
}

// Bind the IDs
func (u *IDs) Bind(r *http.Request) error {
	return nil
}

// Report is what you what to have in your report
type Report struct {
	ID                                uuid.UUID      `json:"id"`
	Name                              string         `json:"name"`
	Description                       string         `json:"description"`
	Frequency                         string         `json:"frequency"`
	DurationDisplayModeExactDuration  bool           `json:"durationDisplayModeExactDuration"`
	DurationDisplayModeHoursPerDay    string         `json:"durationDisplayModeHoursPerDay"`
	DurationDisplayModePortionOfTotal bool           `json:"durationDisplayModePortionOfTotal"`
	StartDate                         time.Time      `json:"startDate,omitempty"`
	EndDate                           time.Time      `json:"endDate,omitempty"`
	Tags                              tag.Tags       `json:"tags,omitempty"`
	DataVisualisations                []string       `json:"dataVisualisations,omitempty"`
	Owner                             *user.TinyUser `json:"owner,omitempty"`
	Pinning                           string         `json:"pinning"`
	Synchronizable                    bool           `json:"synchronizable"`
	SharedUsers                       []string       `json:"sharedUsers"`
	Shared                            bool           `json:"shared"`
}

// Render the Report
func (u Report) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the Report
func (u *Report) Bind(r *http.Request) error {
	return nil
}

// DetailedReport is a Report with projects inside
type DetailedReport struct {
	Report   *Report          `json:"report"`
	Projects project.Projects `json:"projects"`
	Users    user.TinyUsers   `json:"users"`
	Teams    team.Teams       `json:"teams"`
}

// Render the DetailedReports
func (u DetailedReport) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// DetailedReports is an array of DetailReport
type DetailedReports []DetailedReport

// Render the DetailedReports
func (u DetailedReports) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedDetailedReports is paginated list of DetailedReports
type PaginatedDetailedReports struct {
	DetailedReports DetailedReports `json:"items"`
	Count           int             `json:"count"`
}

// Render the PaginatedDetailedReports
func (u PaginatedDetailedReports) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Request is a report request
type Request struct {
	Report                    *Report          `json:"report"`
	Projects                  project.Projects `json:"projects"`
	ValidationSheetsLateCount int64            `json:"validationSheetsLateCount"`
}

// Render the Request
func (u Request) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Requests is an array of Request
type Requests []Request

// Render the Requests
func (u Requests) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedRequests is paginated list of Requests
type PaginatedRequests struct {
	Requests Requests `json:"items"`
	Count    int      `json:"count"`
}

// Render the PaginatedRequests
func (u PaginatedRequests) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// ReportedTask is a task reported in a report
type ReportedTask struct {
	TaskTitle    string    `json:"title"`
	TaskTags     tag.Tags  `json:"tags"`
	ProjectColor string    `json:"projectColor"`
	ProjectName  string    `json:"projectName"`
	ProjectID    uuid.UUID `json:"projectId"`
	Duration     float64   `json:"duration"`
	Validated    bool      `json:"validated,omitempty"`
}

// ReportedTasks is an array of ReportedTask
type ReportedTasks []ReportedTask

// SyncTask is a task to sync
type SyncTask struct {
	ID            string    `json:"id"`
	TaskTags      tag.Tags  `json:"tags"`
	ProjectID     uuid.UUID `json:"projectId"`
	RepoID        uuid.UUID `json:"repoId"`
	RepoProjectID string    `json:"repoProjectId"`
	Duration      float64   `json:"duration"`
}

// SyncTasks is an array of SyncTask
type SyncTasks []SyncTask

// Render the ReportedTasks
func (u ReportedTasks) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the ReportedTasks
func (u *ReportedTasks) Bind(r *http.Request) error {
	return nil
}

// DisplayedReportedTasks are the reportedTasks given to front-end
type DisplayedReportedTasks struct {
	Reported  ReportedTasks `json:"reported"`
	Validated ReportedTasks `json:"validated"`
}

// Render the DisplayedReportedTasks
func (u DisplayedReportedTasks) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the DisplayedReportedTasks
func (u *DisplayedReportedTasks) Bind(r *http.Request) error {
	return nil
}

// QueryPeriod is which chart
type QueryPeriod struct {
	ReportID  uuid.UUID `json:"reportId"`
	StartDate time.Time `json:"startDate"`
	EndDate   time.Time `json:"endDate"`
	Timezone  int       `json:"timezone"`
}

// Bind the QueryPeriod
func (u *QueryPeriod) Bind(r *http.Request) error {
	return nil
}

// ValidationSheet is data for validation sheet
type ValidationSheet struct {
	Period QueryPeriod   `json:"period"`
	Tasks  ReportedTasks `json:"tasks"`
}

// Bind the ValidationSheet
func (u *ValidationSheet) Bind(r *http.Request) error {
	return nil
}

// ChartData is the chart
//
// Example:
// chartdata = {
// 	labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
// 	datasets: [
// 		{
// 			label: "Wid",
// 			data: [20, 10, 10, 15],
// 			backgroundColor: "#079992",
// 		},
// 		{
// 			label: "Pollen",
// 			data: [14, 25, 10, 5],
// 			backgroundColor: "#0a3d62",
// 		},
// 	],
// };
type ChartData struct {
	Labels   []string   `json:"labels"`
	DataSets []DataSets `json:"datasets"`
}

// Render the ChartData
func (u ChartData) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// DataSets is the data of the chart
type DataSets struct {
	Label           string    `json:"label"`
	Data            []float64 `json:"data"`
	BackgroundColor []string  `json:"backgroundColor"`
}

// ReportSync is data for the synchronization
type ReportSync struct {
	ID           uuid.UUID
	RepoID       uuid.UUID
	RepoCategory string
	UpdatedDate  time.Time
	StartDate    time.Time
	EndDate      time.Time
	ReportID     uuid.UUID
	Worklogs     string
}
