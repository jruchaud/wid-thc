package tag

import (
	"net/http"
	"wid/pkg/httperror"
	"wid/pkg/user"

	"github.com/go-chi/render"
)

// GetAllTags returns all tags used by all projects
func GetAllTags(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	allTagsUsed, err := GetAllTagsUsed(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, allTagsUsed)
}

// SearchByName returns all tags by name
func SearchByName(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	name := r.URL.Query().Get("q")
	projectIDs := r.URL.Query()["projectIds"]

	allTagsUsed, err := Search(r.Context(), name, projectIDs, userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, allTagsUsed)
}
