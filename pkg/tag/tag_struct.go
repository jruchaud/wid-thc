package tag

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"sort"
)

// see https://yourbasic.org/golang/implement-set/ for more information
type void struct{}

var member void

// Tag is a label used to time Task and Project, in order to make global reports
type Tag string

// Tags is a set of tag
type Tags map[Tag]void

// Render the Tags
func (tags Tags) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedTags is paginated list of Tags
type PaginatedTags struct {
	Tags  Tags `json:"items"`
	Count int  `json:"count"`
}

// Render the ResponseTags
func (tags PaginatedTags) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// UnmarshalJSON a tags
func (tags *Tags) UnmarshalJSON(data []byte) error {
	var ss []string
	err := json.Unmarshal(data, &ss)
	if err != nil {
		return err
	}

	*tags = make(map[Tag]void)
	tags.AddAllString(ss...)
	return nil
}

// MarshalJSON a tags as slice
func (tags Tags) MarshalJSON() ([]byte, error) {
	arr := tags.ToArray()
	return json.Marshal(arr)
}

// NewTags creates tags
func NewTags(ts ...Tag) Tags {
	var res Tags
	res = make(map[Tag]void)
	res.AddAll(ts...)
	return res
}

// NewStringTags creates tags
func NewStringTags(ts ...string) Tags {
	var res Tags
	res = make(map[Tag]void)
	res.AddAllString(ts...)
	return res
}

// Add new tag
func (tags *Tags) Add(t Tag) {
	(*tags)[t] = member
}

// Has the tag
func (tags *Tags) Has(t Tag) bool {
	_, exists := (*tags)[t]
	return exists
}

// Size of tags
func (tags *Tags) Size() int {
	size := len(*tags)
	return size
}

// Delete a tag
func (tags *Tags) Delete(t Tag) {
	delete(*tags, t)
}

// AddAll add tags
func (tags *Tags) AddAll(ts ...Tag) {
	for _, t := range ts {
		tags.Add(t)
	}
}

// AddAllString add tags from string
func (tags *Tags) AddAllString(ts ...string) {
	for _, t := range ts {
		tags.Add(Tag(t))
	}
}

// ToArray convert to an array
func (tags *Tags) ToArray() []string {
	var res []string

	for k := range *tags {
		res = append(res, fmt.Sprint(k))
	}

	sort.Sort(sort.StringSlice(res))
	return res
}

// Find a tag in a slice
func Find(tags Tags, slice []Tags) bool {
	for _, t := range slice {
		if reflect.DeepEqual(t, tags) {
			return true
		}
	}
	return false
}

// Configuration uses to add a tag
type Configuration struct {
	Tag       Tag  `json:"name"`
	IsDefault bool `json:"isDefault"`
}

// Bind the DefaultTag
func (u *Configuration) Bind(r *http.Request) error {
	return nil
}

// Configurations is array of Configuration
type Configurations []Configuration

// Render the Configurations
func (u Configurations) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedConfigurations is paginated list of Configurations
type PaginatedConfigurations struct {
	Configurations Configurations `json:"items"`
	Count          int            `json:"count"`
}

// Render the PaginatedConfigurations
func (tags PaginatedConfigurations) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
