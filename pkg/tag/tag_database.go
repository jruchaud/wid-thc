package tag

import (
	"context"
	"wid/pkg/permission"
	"wid/pkg/selector"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// GetAllTagsUsed get all tags used by all projects
func GetAllTagsUsed(
	context context.Context,
	userID uuid.UUID) (PaginatedTags, error) {

	sql := permission.SQLRequestReporter(`
		SELECT DISTINCT(unnest(p.allTagsUsed)) as tag
		FROM project p WHERE %s
	`, "$1", "p.id")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "tag",
			Fields:     []string{"tag"},
		},
		sql,
		userID,
	)

	allTagsUsed := PaginatedTags{
		Tags: Tags{},
	}
	if err != nil {
		return allTagsUsed, err
	}

	for rows.Next() {
		var tag string

		err = rows.Scan(&tag, &allTagsUsed.Count)
		if err != nil {
			return allTagsUsed, err
		}

		allTagsUsed.Tags.Add(Tag(tag))
	}

	return allTagsUsed, err
}

// Search tags by name limited by project
func Search(
	context context.Context,
	tagName string,
	projectIDs []string,
	userID uuid.UUID) (Tags, error) {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		SELECT DISTINCT(t.tags)
		FROM 
			project p,
			unnest(p.allTagsUsed) t(tags)
		WHERE 
			(p.id IN (SELECT unnest($3::UUID[]))
				OR array_length($3, 1) IS NULL)

			AND t.tags ILIKE $2 
			AND %s
		LIMIT 10
	`, "$1", "p.id")

	allTagsUsed := NewTags()

	rows, err := db.Query(context, sql, userID, "%"+tagName+"%", projectIDs)
	if err != nil {
		return allTagsUsed, err
	}

	for rows.Next() {
		var tag string

		err = rows.Scan(&tag)
		if err != nil {
			return allTagsUsed, err
		}

		allTagsUsed.Add(Tag(tag))
	}

	return allTagsUsed, err
}
