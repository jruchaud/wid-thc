package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	"wid/pkg/event"
	"wid/pkg/issue"
	"wid/pkg/permission"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/selector"
	"wid/pkg/tag"
	"wid/pkg/task"
	"wid/pkg/team"
	"wid/pkg/wcontext"

	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
)

// Run starts the server
func Run(wc wcontext.Context) error {
	mux := GetMux(wc)

	log.Printf("Server started %s", wc.Config.ServerURL)
	err := http.ListenAndServe(wc.Config.ServerURL, mux)

	return err
}

// GetMux create mux
func GetMux(wc wcontext.Context) *chi.Mux {
	m := chi.NewRouter()
	m.Use(middleware.RequestID)
	m.Use(middleware.RealIP)
	m.Use(middleware.Logger)
	m.Use(middleware.Recoverer)
	m.Use(render.SetContentType(render.ContentTypeJSON))

	m.Use(cors.Handler(cors.Options{
		AllowedOrigins:   wc.Config.ServerAllowedOrigins,
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		AllowedMethods:   []string{"GET", "POST", "DELETE", "PUT", "OPTIONS"},
		AllowCredentials: true,
	}))

	// Replace the request context by wid context
	m.Use(
		func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ctx := wc.Context(r)
				next.ServeHTTP(w, r.WithContext(ctx))
			})
		})

	// Protected routes
	m.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(wc.TokenAuth))
		r.Use(jwtauth.Authenticator)
		r.Use(PostponeJwtAuthExpiredDate)
		r.Use(AddSelector)

		r.Get("/protected", func(w http.ResponseWriter, r *http.Request) {
			_, claims, _ := jwtauth.FromContext(r.Context())
			w.Write([]byte(fmt.Sprintf("protected area. hi %v", claims["user_id"])))
		})

		r.Get("/user", user.GetUser)
		r.Post("/user", user.SaveUser)
		r.Get("/user/requests", report.GetRequestCount)
		r.Get("/user/passwd", user.ModifyPassword)
		r.Post("/user/event-api-token", user.EnableUserEventApi)
		r.Delete("/user/event-api-token", user.DisableUserEventApi)
		r.Get("/users/search", user.SearchByName)

		r.Post("/import", task.Import)

		r.Post("/project", project.SaveProject)
		r.Post("/project/{projectID}/tag", project.AddTag)
		r.Get("/project/{projectID}", project.GetProject)
		r.Delete("/project/{projectID}", project.RemoveProject)
		r.Get("/project/{projectID}/tags", project.GetTags)
		r.Get("/project/{projectID}/default-tags", project.GetDefaultTags)
		r.Get("/project/{projectID}/users", project.GetUsers)
		r.Get("/project/{projectID}/teams", project.GetTeams)
		r.Get("/projects", project.GetProjects)
		r.Get("/projects/search", project.SearchByName)
		r.Post("/projects/collaborators", project.GetCollaborators)
		r.Get("/project/{projectID}/tasks", task.GetTasks)
		r.Get("/project/{projectID}/tasks/search", task.SearchByTitle)
		r.Post("/project/{projectID}/tags", project.RenameTag)
		r.Delete("/project/{projectID}/tags", project.RemoveTag)
		r.Post("/project/{projectID}/tasks", project.RenameTasks)
		r.Put("/project/{projectID}/tasks", project.MoveTasks)

		r.Post("/project/{projectID}/repo", issue.SaveProject)
		r.Delete("/project/{projectID}/repo", issue.RemoveProject)
		r.Get("/project/{projectID}/repo", issue.GetProject)

		r.Get("/project/{projectID}/issues/search", issue.SearchIssues)
		r.Get("/project/{projectID}/issue", issue.GetIssue)
		r.Post("/project/{projectID}/issue", issue.CreateIssue)

		r.Post("/task", task.SaveTask)
		r.Get("/task/{taskID}", task.GetTask)
		r.Delete("/task/{taskID}", task.Remove)

		r.Post("/period", task.SavePeriod)
		r.Get("/period/stop", task.StopPeriod)
		r.Get("/period/running", task.GetRunningPeriod)
		r.Get("/period/{taskID}", task.GetPeriod)
		r.Delete("/period/{taskID}", task.Remove)
		r.Get("/periods/{startDate}/{endDate}", task.GetPeriods)
		r.Get("/periods/history", task.GetHistoryPeriods)

		r.Get("/tags", tag.GetAllTags)
		r.Get("/tags/search", tag.SearchByName)

		r.Get("/reports", report.GetAllReports)
		r.Post("/report", report.SaveReport)
		r.Delete("/report/{reportID}", report.RemoveReport)
		r.Post("/report/{reportID}/projects", report.SetProjects)
		r.Post("/report/{reportID}/users", report.SetUsers)
		r.Post("/report/{reportID}/teams", report.SetTeams)
		r.Post("/report/{reportID}/pinning", report.SetPinning)
		r.Get("/report/{reportID}", report.GetReport)
		r.Post("/report/{reportID}/share", report.ShareReport)
		r.Post("/report/{reportID}/collaborators", project.GetCollaborators)

		r.Get("/requests", report.GetRequests)
		r.Get("/request/{reportID}", report.GetRequest)
		r.Post("/request/late", report.GetReportedUsersLate)
		r.Post("/request/tasks", report.GetReportedTasks)
		r.Post("/request/sheet", report.SaveReportValidation)
		r.Post("/request/sync", issue.SyncReportValidation)

		r.Post("/chart/project/tags", report.GetChartProjectByTags)
		r.Post("/chart/total/project", report.GetChartTotalForProject)
		r.Post("/chart/total/tag", report.GetChartTotalForTag)
		r.Post("/chart/total/task", report.GetChartTotalForTask)
		r.Post("/chart/total/user", report.GetChartTotalForUser)
		r.Post("/chart/total/days", report.GetChartTotalDays)

		r.Post("/permission/user", permission.SaveUser)
		r.Delete("/permission/user", permission.RemoveUser)
		r.Post("/permission/team", permission.SaveTeam)
		r.Delete("/permission/team", permission.RemoveTeam)

		r.Post("/team", team.SaveTeam)
		r.Post("/team/member", team.AddMember)
		r.Delete("/team/member", team.RemoveMember)
		r.Get("/teams", team.GetTeams)
		r.Get("/teams/search", team.SearchByName)
		r.Delete("/team/{teamID}", team.RemoveTeam)
		r.Get("/team/{teamID}", team.GetTeam)
		r.Get("/team/{teamID}/members", team.GetMembers)

		r.Get("/user/repos", issue.GetRepos)
		r.Get("/repos/{teamID}", issue.GetRepositories)
		r.Post("/repo/{teamID}", issue.AddRepository)
		r.Get("/repo/{repoID}", issue.GetRepository)
		r.Delete("/repo/{repoID}", issue.RemoveRepository)
		r.Get("/repos/activities", issue.GetActivities)

		r.Get("/events", event.GetEvents)

		r.Post("/"+issue.RepoCategoryGitlab+"/auth", issue.SaveGitlabAuth)
		r.Delete("/"+issue.RepoCategoryGitlab+"/auth", issue.RemoveAuth)
		r.Get("/"+issue.RepoCategoryGitlab+"/search", issue.SearchGitlabProjects)

		r.Post("/"+issue.RepoCategoryJira+"/auth", issue.SaveJiraAuth)
		r.Delete("/"+issue.RepoCategoryJira+"/auth", issue.RemoveAuth)
		r.Get("/"+issue.RepoCategoryJira+"/search", issue.SearchJiraProjects)

		r.Post("/"+issue.RepoCategoryGithub+"/auth", issue.SaveGithubAuth)
		r.Delete("/"+issue.RepoCategoryGithub+"/auth", issue.RemoveAuth)
		r.Get("/"+issue.RepoCategoryGithub+"/search", issue.SearchGithubProjects)

		r.Post("/"+issue.RepoCategoryRedmine+"/auth", issue.SaveRedmineAuth)
		r.Delete("/"+issue.RepoCategoryRedmine+"/auth", issue.RemoveAuth)
		r.Get("/"+issue.RepoCategoryRedmine+"/search", issue.SearchRedmineProjects)

		r.Post("/notification/repo", issue.SaveNotification)
	})

	// Public routes
	m.Group(func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("welcome on wid thc server"))
		})
		r.Post("/signup", user.SignUp)
		r.Post("/signin", user.SignIn)
		r.Get("/signout", user.SignOut)
		r.Get("/resetpwd", user.ResetPwd)
		r.Get("/updatepwd", user.UpdatePwd)
		r.Get("/verify/email", user.VerifyUserEmail)
		r.Get("/verify/name", user.VerifyUserName)
		r.Get("/confirm", user.SendConfirmEmail)

		r.Post("/webhook/{token}", event.SaveEvents)
	})

	return m
}

func PostponeJwtAuthExpiredDate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		jwt, _ := r.Cookie("jwt")

		if jwt != nil {
			// Postpone the expired date
			cookie := http.Cookie{
				Name:     "jwt",
				Value:    jwt.Value,
				Expires:  time.Now().Add(wcontext.ExpiryDuration * time.Hour),
				HttpOnly: true,
				Path:     "/",
			}
			http.SetCookie(w, &cookie)
		}

		next.ServeHTTP(w, r)
	})
}

func AddSelector(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
		offset, _ := strconv.Atoi(r.URL.Query().Get("offset"))
		order := strings.Split(r.URL.Query().Get("order"), "_")
		search := r.URL.Query().Get("search")

		orderValue := ""
		orderDir := true
		if len(order) >= 1 {
			orderValue = order[0]
		}
		if len(order) >= 2 {
			orderDir = order[1] != "desc"
		}

		ctx := context.WithValue(r.Context(), selector.SelectorContext, selector.QuerySelector{
			Limit:  limit,
			Offset: offset,
			Order:  orderValue,
			Asc:    orderDir,
			Search: search,
		})

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
