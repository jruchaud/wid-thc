package selector

import (
	"context"
	"fmt"
	"strings"
	"wid/pkg/wcontext"

	"github.com/jackc/pgx/v4"
)

var SelectorContext = "selector"

type QuerySelector struct {
	Limit  int
	Offset int
	Order  string
	Asc    bool
	Search string
}

type QueryConfig struct {
	Searchable string
	Fields     []string
}

// Select wrap the current query to add specific selector
// wid=# with test as (select * from task) select t.*, count(*) over() from test t where title ilike '%t%' order by title limit 10 offset 3;
func Select(
	context context.Context,
	q *QueryConfig,
	query string,
	args ...interface{},
) (pgx.Rows, error) {

	db := wcontext.GetDatabase(context)
	selector := context.Value(SelectorContext).(QuerySelector)

	queryWithSelectors := fmt.Sprintf("WITH query AS(%s) SELECT q.*, COUNT(*) OVER() FROM query q", query)

	if selector.Search != "" && q.Searchable != "" {
		queryWithSelectors += fmt.Sprintf(" WHERE %s ILIKE $%d", q.Searchable, len(args)+1)
		args = append(args, "%"+selector.Search+"%")
	}

	if selector.Order != "" && strings.Contains(strings.Join(q.Fields, ","), selector.Order) {
		queryWithSelectors += fmt.Sprintf(" ORDER BY %s", selector.Order)

		if selector.Asc {
			queryWithSelectors += " ASC"
		} else {
			queryWithSelectors += " DESC"
		}
	}

	if selector.Limit != 0 && selector.Limit <= 100 {
		queryWithSelectors += fmt.Sprintf(" LIMIT $%d", len(args)+1)
		args = append(args, selector.Limit)
	}

	if selector.Offset != 0 {
		queryWithSelectors += fmt.Sprintf(" OFFSET $%d", len(args)+1)
		args = append(args, selector.Offset)
	}

	return db.Query(context, queryWithSelectors, args...)
}
