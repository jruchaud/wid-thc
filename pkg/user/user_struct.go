package user

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/jwtauth"
	"github.com/google/uuid"
)

// User is, for now, a creator of a Project and his timing Tasks
type User struct {
	ID             uuid.UUID `json:"id"`
	Email          string    `json:"email"`
	Password       string    `json:"password,omitempty"`
	Name           string    `json:"name"`
	Avatar         string    `json:"avatar,omitempty"`
	RecoveryToken  uuid.UUID `json:"-"`
	ExpirationTime time.Time `json:"-"`
	EventApiToken  uuid.UUID `json:"eventApiToken"`
}

// Render the user
func (u User) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the user
func (u *User) Bind(r *http.Request) error {
	return nil
}

// TinyUser is used to display some information to other user
type TinyUser struct {
	ID         uuid.UUID `json:"id"`
	Name       string    `json:"name"`
	Avatar     string    `json:"avatar,omitempty"`
	Permission string    `json:"permission,omitempty"`
	TeamAdmin  bool      `json:"admin,omitempty"`
}

// Render the TinyUser
func (t TinyUser) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// TinyUsers is array of TinyUser
type TinyUsers []TinyUser

// Render the TinyUsers
func (t TinyUsers) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// UserCredentials is used for the user authentication
type UserCredentials struct {
	Email          string    `json:"email"`
	Password       string    `json:"password"`
}

// Bind the UserCredentials
func (c *UserCredentials) Bind(r *http.Request) error {
	return nil
}

// PaginatedTinyUsers is paginated list of TinyUsers
type PaginatedTinyUsers struct {
	TinyUsers TinyUsers `json:"items"`
	Count     int       `json:"count"`
}

// Render the PaginatedTinyUsers
func (t PaginatedTinyUsers) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// GetUserID get the user id from request
func GetUserID(r *http.Request) (uuid.UUID, error) {
	_, claims, _ := jwtauth.FromContext(r.Context())
	userStringID := claims["user_id"].(string)

	userID, err := uuid.Parse(userStringID)
	if err != nil {
		return uuid.UUID{}, err
	}

	return userID, nil
}

// GetAvatar gets an avatar from email
func GetAvatar(email string) string {
	key := md5.Sum([]byte(strings.TrimSpace(strings.ToLower(email))))
	res := fmt.Sprintf("https://cdn.libravatar.org/avatar/%x?d=identicon", key)
	return res
}

// Code validation code form email
type Code string

// Render the Code
func (u Code) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type EventApiToken uuid.UUID

// Render the EventApiToken
func (t EventApiToken) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
