package user

import (
	"errors"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/wcontext"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

// SignUp the user
func SignUp(w http.ResponseWriter, r *http.Request) {
	data := &User{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	code := r.URL.Query().Get("code")
	hashCode := r.URL.Query().Get("hashcode")
	secret := wcontext.GetConfig(r.Context()).JWTSecret

	err := bcrypt.CompareHashAndPassword([]byte(hashCode), []byte(code+data.Email+secret))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(err))
		return
	}

	id, err := uuid.NewUUID()
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	data.ID = id

	password, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	data.Password = string(password)

	err = Insert(r.Context(), data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	render.Render(w, r, TinyUser{
		ID:     data.ID,
		Name:   data.Name,
		Avatar: GetAvatar(data.Email)})
}

// SignIn the user
func SignIn(w http.ResponseWriter, r *http.Request) {
	data := &UserCredentials{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	user, err := SelectByEmail(r.Context(), data.Email)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(err))
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(err))
		return
	}

	tokenAuth := wcontext.GetJWTAuth(r.Context())

	claims := jwt.MapClaims{
		"user_id": user.ID,
		"exp":     time.Now().Add(wcontext.ExpiryDuration * time.Hour).Unix(),
	}
	_, token, err := tokenAuth.Encode(claims)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	// Create jwt cookie and add it to the response
	cookie := http.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(wcontext.ExpiryDuration * time.Hour),
		HttpOnly: true,
		Path:     "/",
	}
	http.SetCookie(w, &cookie)

	user.Password = ""
	render.Render(w, r, &user)

}

// SignOut the user
func SignOut(w http.ResponseWriter, r *http.Request) {
	// Revoke jwt cookie
	cookie := http.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-wcontext.ExpiryDuration * time.Hour),
		MaxAge:   -1,
		HttpOnly: true,
		Path:     "/",
	}
	http.SetCookie(w, &cookie)
	w.WriteHeader(http.StatusNoContent)
}

// GetUser returns the connected user
func GetUser(w http.ResponseWriter, r *http.Request) {

	userID, err := GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	user, err := SelectByID(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(err))
		return
	}

	user.Avatar = GetAvatar(user.Email)

	render.Render(w, r, user)
}

// SaveUser save name and email for the user
func SaveUser(w http.ResponseWriter, r *http.Request) {
	data := &User{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpdateUser(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func EnableUserEventApi(w http.ResponseWriter, r *http.Request) {

	userID, err := GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	newUuid, err := uuid.NewUUID()
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	userEventApiToken := EventApiToken(newUuid)

	err = UpdateUserEventApiToken(r.Context(), userID, userEventApiToken)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	enableUserEventApiResult := EnableUserEventApiResult{newUuid.String()}

	render.Render(w, r, enableUserEventApiResult)

}

type EnableUserEventApiResult struct {
	NewToken string `json:"newToken"`
}

// Render the EnableUserEventApiResult
func (x EnableUserEventApiResult) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func DisableUserEventApi(w http.ResponseWriter, r *http.Request) {

	userID, err := GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = ResetUserEventApiToken(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// ModifyPassword generates token and send a email to the user to modify her password
func ModifyPassword(w http.ResponseWriter, r *http.Request) {
	userID, err := GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	origin := r.URL.Query().Get("origin")
	recoveryToken, err := uuid.NewUUID()
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	recoveryTokenExpiration := wcontext.GetConfig(r.Context()).RecoveryTokenExpiration

	email, err := UpdatePasswordToken(r.Context(), userID, recoveryToken, time.Now().Add(time.Minute*recoveryTokenExpiration))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	data := map[string]string{
		"Host":  origin,
		"Token": recoveryToken.String(),
	}
	subject, body, err := wcontext.GetTemplate(r, UpdatePasswordTemplate, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = wcontext.SendEmail(wcontext.GetConfig(r.Context()), email, subject, body)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// ResetPwd generates token and send a email to the user to reset her password
func ResetPwd(w http.ResponseWriter, r *http.Request) {
	queryEmail := r.URL.Query().Get("email")
	origin := r.URL.Query().Get("origin")
	recoveryToken, err := uuid.NewUUID()
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	recoveryTokenExpiration := wcontext.GetConfig(r.Context()).RecoveryTokenExpiration

	err = UpdatePwdRecoveryInfos(r.Context(), queryEmail, recoveryToken, time.Now().Add(time.Minute*recoveryTokenExpiration))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	data := map[string]string{
		"Host":  origin,
		"Token": recoveryToken.String(),
	}
	subject, body, err := wcontext.GetTemplate(r, ResetPasswordTemplate, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = wcontext.SendEmail(wcontext.GetConfig(r.Context()), queryEmail, subject, body)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// UpdatePwd update user password, if the token is valid
func UpdatePwd(w http.ResponseWriter, r *http.Request) {
	recoveryToken, err := uuid.Parse(r.URL.Query().Get("recoveryToken"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	pwd := r.URL.Query().Get("pwd")
	password, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	result, err := UpdateUserPwd(r.Context(), recoveryToken, string(password))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}
	if !result {
		render.Render(w, r, httperror.ErrResponseBadRequest(errors.New("Invalid Token")))
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// SearchByName search user by name
func SearchByName(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("q")

	users, err := Search(r.Context(), name)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, users)
}

// VerifyUserEmail verify the email
func VerifyUserEmail(w http.ResponseWriter, r *http.Request) {
	queryEmail := r.URL.Query().Get("email")

	exists, err := VerifyEmail(r.Context(), queryEmail)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	if exists {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(errors.New("Already exists")))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// VerifyUserName verify the username
func VerifyUserName(w http.ResponseWriter, r *http.Request) {
	queryUsername := r.URL.Query().Get("name")

	exists, err := VerifyName(r.Context(), queryUsername)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}
	if exists {
		render.Render(w, r, httperror.ErrResponseNotAcceptable(errors.New("Already exists")))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SendConfirmEmail send an email with the code
func SendConfirmEmail(w http.ResponseWriter, r *http.Request) {
	queryEmail := r.URL.Query().Get("email")

	rand.Seed(time.Now().UnixNano())
	code := strconv.Itoa(rand.Intn(999999-100000) + 100000)

	data := map[string]string{
		"Code": code,
	}
	subject, body, err := wcontext.GetTemplate(r, ComfirmEmailTemplate, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	err = wcontext.SendEmail(wcontext.GetConfig(r.Context()), queryEmail, subject, body)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	secret := wcontext.GetConfig(r.Context()).JWTSecret
	hashCode, err := bcrypt.GenerateFromPassword([]byte(code+queryEmail+secret), bcrypt.DefaultCost)
	render.Render(w, r, Code(hashCode))
}
