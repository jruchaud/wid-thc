package user

import (
	"context"
	"time"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// VerifyEmail verify if an user alredy exists
func VerifyEmail(
	context context.Context,
	email string) (bool, error) {

	db := wcontext.GetDatabase(context)

	row := db.QueryRow(context, "SELECT EXISTS (SELECT 1 FROM wid_user WHERE email = $1)", email)
	var value bool
	err := row.Scan(&value)
	return value, err
}

// VerifyName verify if an user alredy exists
func VerifyName(
	context context.Context,
	name string) (bool, error) {

	db := wcontext.GetDatabase(context)

	row := db.QueryRow(context, "SELECT EXISTS (SELECT 1 FROM wid_user WHERE name = $1)", name)
	var value bool
	err := row.Scan(&value)
	return value, err
}

// SelectByEmail selects the user by email
func SelectByEmail(
	context context.Context,
	email string) (User, error) {

	var user User
	db := wcontext.GetDatabase(context)

	row := db.QueryRow(context, "SELECT id, email, password, name FROM wid_user WHERE email = $1", email)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.Name)
	return user, err
}

// GetUserIdByToken get the owner of a token for event API
func GetUserIdByToken(
	context context.Context,
	eventApiToken EventApiToken) (uuid.UUID, error) {

	db := wcontext.GetDatabase(context)

	var userID uuid.UUID
	row := db.QueryRow(context, "SELECT id FROM wid_user WHERE eventApiToken = $1", eventApiToken)
	err := row.Scan(&userID)
	return userID, err
}

// SelectByID selects the user by ID
func SelectByID(
	context context.Context,
	userID uuid.UUID) (User, error) {

	var user User
	db := wcontext.GetDatabase(context)

	row := db.QueryRow(context, "SELECT id, email, name, eventApiToken FROM wid_user WHERE id = $1", userID)
	err := row.Scan(&user.ID, &user.Email, &user.Name, &user.EventApiToken)
	return user, err
}

// Insert the user
func Insert(
	context context.Context,
	user *User) error {

	db := wcontext.GetDatabase(context)

	_, err := db.Exec(
		context,
		"INSERT INTO wid_user(id, email, password, name) VALUES ($1, $2, $3, $4)",
		user.ID,
		user.Email,
		user.Password,
		user.Name,
	)
	return err
}

// UpdatePasswordToken updates the user password recovery informations from userID
func UpdatePasswordToken(context context.Context, userID uuid.UUID, recoveryToken uuid.UUID, expirationTime time.Time) (string, error) {
	db := wcontext.GetDatabase(context)
	row := db.QueryRow(
		context,
		"UPDATE wid_user SET recoveryToken = $1, expirationTime = $2 WHERE id = $3 RETURNING email",
		recoveryToken,
		expirationTime,
		userID,
	)

	var email string
	err := row.Scan(&email)
	if err != nil {
		return "", err
	}

	return email, err
}

// UpdatePwdRecoveryInfos updates the user password recovery informations from email
func UpdatePwdRecoveryInfos(context context.Context, mail string, recoveryToken uuid.UUID, expirationTime time.Time) error {
	db := wcontext.GetDatabase(context)
	_, err := db.Exec(
		context,
		"UPDATE wid_user SET recoveryToken = $1, expirationTime = $2 WHERE email = $3",
		recoveryToken,
		expirationTime,
		mail,
	)
	return err
}

// UpdateUserPwd updates the user password with a token
func UpdateUserPwd(context context.Context, recoveryToken uuid.UUID, pwd string) (bool, error) {
	db := wcontext.GetDatabase(context)
	r, err := db.Exec(
		context,
		"UPDATE wid_user SET password = $1 WHERE recoveryToken = $2 AND expirationTime > NOW()",
		pwd,
		recoveryToken,
	)
	return r.RowsAffected() != 0, err
}

// UpdateUser updates the user
func UpdateUser(context context.Context, userID uuid.UUID, user *User) error {
	db := wcontext.GetDatabase(context)
	_, err := db.Exec(
		context,
		"UPDATE wid_user SET name = $1, email = $2 WHERE id = $3",
		user.Name,
		user.Email,
		userID,
	)
	return err
}

// UpdateUser updates the user event API token
func UpdateUserEventApiToken(context context.Context, userID uuid.UUID, userApiToken EventApiToken) error {
	db := wcontext.GetDatabase(context)
	_, err := db.Exec(
		context,
		"UPDATE wid_user SET eventApiToken = $1 WHERE id = $2",
		userApiToken,
		userID,
	)
	return err
}

// ResetUserEventApiToken remove the event API token for given user
func ResetUserEventApiToken(context context.Context, userID uuid.UUID) error {
	db := wcontext.GetDatabase(context)
	_, err := db.Exec(
		context,
		"UPDATE wid_user SET eventApiToken = NULL WHERE id = $1",
		userID,
	)
	return err
}

// Search a user by name
func Search(
	context context.Context,
	name string) (TinyUsers, error) {

	db := wcontext.GetDatabase(context)

	rows, err := db.Query(context, `
			SELECT id, name, email
			FROM wid_user 
			WHERE name ilike $1
			ORDER BY name 
			LIMIT 10
		`, "%"+name+"%")

	var res TinyUsers
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var user TinyUser
		var email string

		err := rows.Scan(&user.ID, &user.Name, &email)
		if err != nil {
			return res, err
		}
		user.Avatar = GetAvatar(email)

		res = append(res, user)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}
