package user

import (
	"wid/pkg/wcontext"
)

// ResetPasswordTemplate template when the user reset the password
var ResetPasswordTemplate = wcontext.EmailTemplate{
	EN: &wcontext.I18nTemplate{
		Subject: "Reset password",
		Body: `Dear user, 

Please click on the link to update your password and follow the instructions: 

{{.Host}}/updatepwd?recoveryToken={{.Token}}

Regards
Wid team`,
	},
	FR: &wcontext.I18nTemplate{
		Subject: "Réinitialisation du mot de passe",
		Body: `Cher utilisateur, 

Cliquez sur le lien ci-dessous pour réinitialiser le mot de passe et suivez les instructions :

{{.Host}}/updatepwd?recoveryToken={{.Token}}

Cordialement
L'équipe Wid`,
	}}

// UpdatePasswordTemplate template when the user update the password
var UpdatePasswordTemplate = wcontext.EmailTemplate{
	EN: &wcontext.I18nTemplate{
		Subject: "Update password",
		Body: `Dear user, 

Please click on the link to reset your password and follow the instructions: 

{{.Host}}/updatepwd?recoveryToken={{.Token}}

Regards
Wid team`,
	},
	FR: &wcontext.I18nTemplate{
		Subject: "Modification du mot de passe",
		Body: `Cher utilisateur, 

Cliquez sur le lien ci-dessous pour modifier le mot de passe et suivez les instructions :

{{.Host}}/updatepwd?recoveryToken={{.Token}}

Cordialement
L'équipe Wid`,
	}}

// ComfirmEmailTemplate template when the user valid him email
var ComfirmEmailTemplate = wcontext.EmailTemplate{
	EN: &wcontext.I18nTemplate{
		Subject: "Confirm email",
		Body: `Dear user, 

Please enter the follow code to confirm your registering on Wid: 

{{.Code}}

Regards
Wid team`,
	},
	FR: &wcontext.I18nTemplate{
		Subject: "Confirmation de l'email",
		Body: `Cher utilisateur, 

Veuillez entrer le code suivant pour confirmer votre email pour valider votre enregistrement sur Wid :

{{.Code}}

Cordialement
L'équipe Wid`,
	}}
