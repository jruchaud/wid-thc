package permission

import (
	"net/http"
	"wid/pkg/httperror"
	"wid/pkg/user"

	"github.com/go-chi/render"
)

// SaveUser an user permission
func SaveUser(w http.ResponseWriter, r *http.Request) {
	data := &UserPermission{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	if userID == data.UserID {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = UpInsertUser(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RemoveUser an user permission
func RemoveUser(w http.ResponseWriter, r *http.Request) {
	data := &UserPermission{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	if userID == data.UserID {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = DeleteUser(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SaveTeam an team permission
func SaveTeam(w http.ResponseWriter, r *http.Request) {
	data := &TeamPermission{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsertTeam(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// RemoveTeam an user permission
func RemoveTeam(w http.ResponseWriter, r *http.Request) {
	data := &TeamPermission{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = DeleteTeam(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
