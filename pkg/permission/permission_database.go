package permission

import (
	"context"
	"fmt"
	"log"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// UpInsertMaintainer save the first permission on an user and a project
func UpInsertMaintainer(
	context context.Context,
	userID uuid.UUID,
	ProjectID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO user_permission(projectId, userId, permission) 
			VALUES ($1, $2, $3)`

	_, err := db.Exec(context, sql, ProjectID, userID, PermissionMaintainer)

	return err
}

// UpInsertUser save a permission on an user
func UpInsertUser(
	context context.Context,
	userID uuid.UUID,
	permission *UserPermission) error {

	db := wcontext.GetDatabase(context)

	sql := SQLRequestMaintainer(`
			INSERT INTO user_permission(projectId, userId, permission) 
			VALUES ($1, $2, $3)
			ON CONFLICT (projectId, userId) DO UPDATE SET permission=excluded.permission
			WHERE %s
		`, "$4", "$1")

	_, err := db.Exec(context, sql,
		permission.ProjectID, permission.UserID, permission.Permission, userID)

	return err
}

// DeleteUser delete a permission on an user
func DeleteUser(
	context context.Context,
	userID uuid.UUID,
	permission *UserPermission) error {

	db := wcontext.GetDatabase(context)

	sql := SQLRequestMaintainer(`
			DELETE FROM user_permission
			WHERE projectId = $1
			AND userId = $2 
			AND %s
		`, "$3", "$1")

	_, err := db.Exec(context, sql,
		permission.ProjectID, permission.UserID, userID)

	return err
}

// UpInsertTeam save a permission on an team
func UpInsertTeam(
	context context.Context,
	userID uuid.UUID,
	permission *TeamPermission) error {

	db := wcontext.GetDatabase(context)

	sql := SQLRequestMaintainer(`
			INSERT INTO team_permission(projectId, teamId, permission) 
			VALUES ($1, $2, $3)
			ON CONFLICT (projectId, teamId) DO UPDATE SET permission=excluded.permission
			WHERE %s
		`, "$4", "$1")

	_, err := db.Exec(context, sql,
		permission.ProjectID, permission.TeamID, permission.Permission, userID)

	return err
}

// DeleteTeam delete a permission on an team
func DeleteTeam(
	context context.Context,
	userID uuid.UUID,
	permission *TeamPermission) error {

	db := wcontext.GetDatabase(context)

	sql := SQLRequestMaintainer(`
			DELETE FROM team_permission
			WHERE projectId = $1
			AND teamId = $2 
			AND %s
		`, "$3", "$1")

	_, err := db.Exec(context, sql,
		permission.ProjectID, permission.TeamID, userID)

	return err
}

// SQLRequestMaintainer creates a sql request with maintainer permission verification
func SQLRequestMaintainer(sql string, user string, project string) string {
	query := fmt.Sprintf(sql, VerifyPermission(user, project, PermissionMaintainer))
	return query
}

// SQLRequestReporter creates a sql request with reporter permission verification
func SQLRequestReporter(sql string, user string, project string) string {
	query := fmt.Sprintf(sql, VerifyPermission(user, project, PermissionReporter))
	return query
}

// SQLRequestSupervisor creates a sql request with supervisor permission verification
func SQLRequestSupervisor(sql string, user string, project string) string {
	query := fmt.Sprintf(sql, VerifyPermission(user, project, PermissionSupervisor))
	return query
}

// SQLRequestWithPermission creates a sql request with permission verification
func SQLRequestWithPermission(sql string, user string, project string, permission string) string {
	query := fmt.Sprintf(sql, VerifyPermission(user, project, permission))
	log.Print(query)
	return query
}

// VerifyPermission is an helper to complete the sql request
func VerifyPermission(user string, project string, permission string) string {

	var permissionTest string
	if permission == PermissionReporter {
		permissionTest = fmt.Sprintf("__p.permission = '%s' OR __p.permission = '%s' OR __p.permission = '%s'", PermissionReporter, PermissionSupervisor, PermissionMaintainer)
	} else if permission == PermissionSupervisor {
		permissionTest = fmt.Sprintf("__p.permission = '%s' OR __p.permission = '%s'", PermissionSupervisor, PermissionMaintainer)
	} else if permission == PermissionMaintainer {
		permissionTest = fmt.Sprintf("__p.permission = '%s'", PermissionMaintainer)
	}

	res := fmt.Sprintf(`
		(
			EXISTS (
				SELECT 1 
				FROM user_permission __p
				WHERE __p.projectId = %s AND __p.userId = %s AND (%s)
			)
			OR
			EXISTS (
				SELECT 1 
				FROM team_permission __p, team_member __m
				WHERE __p.teamId = __m.teamId AND
				__p.projectId = %s AND __m.userId = %s AND (%s)
			)
		)
	`, project, user, permissionTest, project, user, permissionTest)

	return res
}
