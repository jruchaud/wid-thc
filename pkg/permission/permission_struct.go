package permission

import (
	"net/http"

	"github.com/google/uuid"
)

// PermissionMaintainer reprents the permission maintainer
var PermissionMaintainer = "maintainer"

// PermissionReporter reprents the permission reporter
var PermissionReporter = "reporter"

// PermissionSupervisor reprents the permission supervisor
var PermissionSupervisor = "supervisor"

// UserPermission is a permission for an user
type UserPermission struct {
	UserID     uuid.UUID `json:"userId"`
	ProjectID  uuid.UUID `json:"projectId"`
	Permission string    `json:"permission"`
}

// Bind the UserPermission
func (u *UserPermission) Bind(r *http.Request) error {
	return nil
}

// TeamPermission is a permission for an team
type TeamPermission struct {
	TeamID     uuid.UUID `json:"TeamId"`
	ProjectID  uuid.UUID `json:"projectId"`
	Permission string    `json:"permission"`
}

// Bind the TeamPermission
func (u *TeamPermission) Bind(r *http.Request) error {
	return nil
}
