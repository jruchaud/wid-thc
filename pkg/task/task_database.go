package task

import (
	"context"
	"fmt"
	"time"
	"wid/pkg/permission"
	"wid/pkg/selector"
	tag "wid/pkg/tag"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// UpInsertTask a task
func UpInsertTask(
	context context.Context,
	userID uuid.UUID,
	task *Task) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		INSERT INTO task(id, title, tags, owner, project)
		(SELECT $1 as id, $2 as title, $3 as tags, $4 as owner, $5 as project
			FROM project p WHERE p.id = $5 AND %s)
	`, "$4", "$5")

	sql += permission.SQLRequestMaintainer(`
		ON CONFLICT (id) DO UPDATE SET title=excluded.title, tags=excluded.tags
		WHERE %s
	`, "$4", "$5")

	_, err := db.Exec(context, sql,
		task.ID, task.Title, task.Tags.ToArray(), userID, task.ProjectID)

	return err
}

// UpInsertPeriod a period
func UpInsertPeriod(
	context context.Context,
	userID uuid.UUID,
	period *Period) error {

	db := wcontext.GetDatabase(context)

	if period.Task.ProjectID == (uuid.UUID{}) {
		_, err := db.Exec(context, `
				INSERT INTO task(id, title, tags, startDate, endDate, running, owner)
				VALUES ($1, $2, $3, $4, $5, $6, $7)
				ON CONFLICT (id) DO UPDATE SET title=excluded.title, tags=excluded.tags,
				startDate=excluded.startDate, endDate=excluded.endDate, running=excluded.running,
				project=NULL
				WHERE excluded.owner = $7
			`,
			period.Task.ID,
			period.Task.Title,
			period.Task.Tags.ToArray(),
			period.StartDate,
			period.EndDate,
			period.Running,
			userID)
		return err
	}

	sql := permission.SQLRequestReporter(`
			INSERT INTO task(id, title, tags, startDate, endDate, running, owner, project)
			(
				SELECT $1, $2, $3, $4, $5, $6, $7, $8 
				WHERE %s
			)
			ON CONFLICT (id) DO UPDATE SET title=excluded.title, tags=excluded.tags,
			startDate=excluded.startDate, endDate=excluded.endDate, running=excluded.running,
			project=excluded.project
			WHERE excluded.owner = $7
		`, "$7", "$8")

	_, err := db.Exec(context, sql,
		period.Task.ID,
		period.Task.Title,
		period.Task.Tags.ToArray(),
		period.StartDate,
		period.EndDate,
		period.Running,
		userID,
		period.Task.ProjectID)
	return err
}

// UpdatePeriodRunning stops the current period
func UpdatePeriodRunning(
	context context.Context,
	userID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	_, err := db.Exec(context, `
			UPDATE task t
			SET running = false
			WHERE t.owner = $1
		`,
		userID)

	return err
}

// Delete a task for a project
func Delete(
	context context.Context,
	userID uuid.UUID,
	taskID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		DELETE FROM task t
		WHERE id = $1 AND (%s OR owner = $2)
	`, "$2", "t.project")

	_, err := db.Exec(context, sql, taskID, userID)

	return err
}

// SelectTask get a task by id
func SelectTask(context context.Context,
	userID uuid.UUID,
	taskID uuid.UUID) (Task, error) {

	var task Task
	var tags []string

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestMaintainer(`
		SELECT id, title, tags, project 
		FROM task t
		WHERE id = $2 AND %s
	`, "$1", "t.project")

	row := db.QueryRow(context, sql, userID, taskID)
	err := row.Scan(&task.ID, &task.Title, &tags, &task.ProjectID)
	if err != nil {
		return task, err
	}

	task.Tags = tag.NewStringTags(tags...)
	return task, err
}

// SelectPeriod get a period by id
func SelectPeriod(context context.Context,
	userID uuid.UUID,
	taskID uuid.UUID) (Period, error) {

	var period Period
	var tags []string

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		SELECT t.id, t.title, t.tags, t.startDate, t.endDate, t.running, 
		p.id, COALESCE(p.name, ''), COALESCE(p.color, ''), 
		p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, '')
		FROM task t 
		LEFT OUTER JOIN project p ON t.project = p.id AND %s
		WHERE t.owner = $1 AND t.id = $2
	`, "$1", "p.id")

	row := db.QueryRow(context, sql, userID, taskID)
	err := row.Scan(
		&period.Task.ID,
		&period.Task.Title,
		&tags,
		&period.StartDate,
		&period.EndDate,
		&period.Running,
		&period.Task.ProjectID,
		&period.ProjectName,
		&period.ProjectColor,
		&period.ProjectRepoID,
		&period.ProjectRepoCategory,
		&period.ProjectRepoProjectID,
	)
	if err != nil {
		return period, err
	}

	period.Task.Tags = tag.NewStringTags(tags...)
	return period, err
}

// SelectRunningPeriod get the current running period
func SelectRunningPeriod(context context.Context,
	userID uuid.UUID) (Period, error) {

	var period Period
	var tags []string

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		SELECT t.id, t.title, t.tags, t.startDate, t.endDate, t.running, 
		p.id, COALESCE(p.name, ''), COALESCE(p.color, ''), 
		p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, '')
		FROM task t 
		LEFT OUTER JOIN project p ON t.project = p.id AND %s
		WHERE t.owner = $1 AND t.running = true
	`, "$1", "p.id")

	row := db.QueryRow(context, sql, userID)
	err := row.Scan(
		&period.Task.ID,
		&period.Task.Title,
		&tags,
		&period.StartDate,
		&period.EndDate,
		&period.Running,
		&period.Task.ProjectID,
		&period.ProjectName,
		&period.ProjectColor,
		&period.ProjectRepoID,
		&period.ProjectRepoCategory,
		&period.ProjectRepoProjectID,
	)
	if err != nil {
		return period, err
	}

	period.Task.Tags = tag.NewStringTags(tags...)
	return period, err
}

// GetAll tasks for a project
func GetAll(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID) (PaginatedTitles, error) {

	sql := permission.SQLRequestReporter(`
		SELECT t.title, t.tags, t.endDate IS NULL as isDefault, (array_agg(id))[1], count(id) as count, MAX(COALESCE(t.startDate, now())) as date
		FROM task t
		WHERE t.project = $1 
		AND %s
		GROUP BY t.title, t.tags, isDefault
		ORDER BY t.title, isDefault DESC, t.tags
	`, "$2", "$1")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "title",
			Fields:     []string{"title", "tags", "isDefault", "count", "date"},
		},
		sql,
		projectID, userID,
	)

	var res PaginatedTitles
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var title Title
		var tags []string

		err := rows.Scan(&title.Title, &tags, &title.IsDefault, &title.FirstId, &title.Count, &title.LastDate, &res.Count)
		title.Tags = tag.NewStringTags(tags...)

		if err != nil {
			return res, err
		}

		res.Titles = append(res.Titles, title)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// GetHistory get the last tasks
func GetHistory(
	context context.Context,
	userID uuid.UUID) (PaginatedHistoryTasks, error) {

	sql := permission.SQLRequestReporter(`
		SELECT t.title, t.tags, 
		p.id, COALESCE(p.name, '') as projectName, COALESCE(p.color, ''),
		MAX(t.startDate) startDate
		FROM task t 
		LEFT OUTER JOIN project p ON t.project = p.id AND %s
		WHERE t.owner = $1 
		AND (t.title != '' OR COALESCE(p.name, '') != '')
		AND t.startDate IS NOT NULL
		
		GROUP BY t.title, t.tags, p.id, p.name, p.color
		ORDER BY startDate DESC
	`, "$1", "p.id")

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "title",
			Fields:     []string{"title", "projectName", "startDate"},
		},
		sql,
		userID,
	)

	var res PaginatedHistoryTasks
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var task HistoryTask
		var tags []string

		err := rows.Scan(
			&task.Title,
			&tags,
			&task.ProjectID,
			&task.ProjectName,
			&task.ProjectColor,
			&task.StartDate,
			&res.Count,
		)
		task.Tags = tag.NewStringTags(tags...)

		if err != nil {
			return res, err
		}

		res.HistoryTasks = append(res.HistoryTasks, task)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// GetAllByDate tasks between dates (startDate is included and endDate is excluded)
func GetAllByDate(
	context context.Context,
	userID uuid.UUID,
	startDate time.Time,
	endDate time.Time) (Periods, error) {

	db := wcontext.GetDatabase(context)

	sql := permission.SQLRequestReporter(`
		SELECT t.id, t.title, t.tags, t.startDate, t.endDate, t.running, 
		p.id, COALESCE(p.name, ''), COALESCE(p.color, ''),
		p.repoId, COALESCE(p.repoCategory::TEXT, ''), COALESCE(p.repoProjectId, '')
		FROM task t 
		LEFT OUTER JOIN project p ON t.project = p.id AND %s
		WHERE t.owner = $1 AND t.startDate <= $3 AND t.endDate >= $2
	`, "$1", "p.id")

	rows, err := db.Query(context, sql, userID, startDate, endDate)

	var res Periods
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var period Period
		var tags []string

		err := rows.Scan(
			&period.Task.ID,
			&period.Task.Title,
			&tags,
			&period.StartDate,
			&period.EndDate,
			&period.Running,
			&period.Task.ProjectID,
			&period.ProjectName,
			&period.ProjectColor,
			&period.ProjectRepoID,
			&period.ProjectRepoCategory,
			&period.ProjectRepoProjectID,
		)
		period.Task.Tags = tag.NewStringTags(tags...)

		if err != nil {
			return res, err
		}

		res = append(res, period)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// Search a task by title
func Search(
	context context.Context,
	userID uuid.UUID,
	projectID uuid.UUID,
	title string) (Tasks, error) {

	db := wcontext.GetDatabase(context)

	sqlLastTags := permission.SQLRequestReporter(`
		SELECT t.id, t.title, t.tags 
		FROM task t
		WHERE t.project = $2
		AND %s
		AND t.title ilike $3
		AND t.endDate = (
			SELECT max(m.endDate) 
			FROM task m 
			WHERE m.title = t.title 
			AND m.project = $2
		) LIMIT 10
	`, "$1", "$2")

	sqlDefaultTags := permission.SQLRequestReporter(`
		SELECT t.id, t.title, t.tags 
		FROM task t
		WHERE t.project = $2
		AND %s 
		AND t.title ilike $3 
		AND t.endDate IS NULL 
		LIMIT 10
	`, "$1", "$2")

	sql := fmt.Sprintf(`
		WITH 
			last_tags AS (%s), 
			default_tags AS (%s)

			SELECT COALESCE(d.id, l.id) AS id,
				COALESCE(d.title, l.title) AS title,
				COALESCE(d.tags, l.tags) AS tags
			FROM default_tags d 
				FULL OUTER JOIN last_tags l ON d.title = l.title 
			ORDER BY title ASC LIMIT 10
	`, sqlLastTags, sqlDefaultTags)

	rows, err := db.Query(context, sql, userID, projectID, "%"+title+"%")

	var res Tasks
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var task Task
		var tags []string

		err := rows.Scan(&task.ID, &task.Title, &tags)
		task.Tags = tag.NewStringTags(tags...)

		if err != nil {
			return res, err
		}

		res = append(res, task)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}
