package task

import (
	"net/http"
	"time"
	tag "wid/pkg/tag"

	"github.com/google/uuid"
)

// Task is an activity a user is working on
type Task struct {
	ID        uuid.UUID `json:"id"`
	Title     string    `json:"title"`
	Tags      tag.Tags  `json:"tags"`
	ProjectID uuid.UUID `json:"projectId,omitempty"`
}

// Render the Task
func (u Task) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the Task
func (u *Task) Bind(r *http.Request) error {
	return nil
}

// Tasks is array of Task
type Tasks []Task

// Render the Tasks
func (u Tasks) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Period is an activity a user is working on
type Period struct {
	Task                 Task      `json:"task"`
	ProjectColor         string    `json:"projectColor"`
	ProjectName          string    `json:"projectName"`
	ProjectRepoID        uuid.UUID `json:"projectRepoId"`
	ProjectRepoCategory  string    `json:"projectRepoCategory"`
	ProjectRepoProjectID string    `json:"projectRepoProjectId"`
	StartDate            time.Time `json:"startDate"`
	EndDate              time.Time `json:"endDate"`
	Running              bool      `json:"running"`
}

// Render the Period
func (u Period) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Bind the Period
func (u *Period) Bind(r *http.Request) error {
	return nil
}

// Periods is array of Period
type Periods []Period

// Render the Periods
func (u Periods) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Title is used to display all task
type Title struct {
	Title     string    `json:"title"`
	Tags      tag.Tags  `json:"tags"`
	IsDefault bool      `json:"isDefault"`
	FirstId   uuid.UUID `json:"firstId"`
	Count     int       `json:"count"`
	LastDate  time.Time `json:"lastDate"`
}

// Titles is array of Title
type Titles []Title

// PaginatedTitles is paginated list of titles
type PaginatedTitles struct {
	Titles Titles `json:"items"`
	Count  int    `json:"count"`
}

// Render the PaginatedTitles
func (tags PaginatedTitles) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Render the Titles
func (u Titles) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Time represents a legacy time in sqlite3
type Time struct {
	StartDate float64  `json:"startDate"`
	EndDate   float64  `json:"endDate"`
	Tags      tag.Tags `json:"tags"`
	Title     string   `json:"title"`
}

// Times is array of Time
type Times struct {
	Times          []Time                `json:"times"`
	Excludes       []tag.Tags            `json:"excludes"`
	Projects       map[tag.Tag]uuid.UUID `json:"projects"`
	DefaultProject uuid.UUID             `json:"defaultProject"`
}

// Bind the LegacyTimes
func (u *Times) Bind(r *http.Request) error {
	return nil
}

// HistoryTask is an activity a user is working on
type HistoryTask struct {
	Title        string    `json:"title"`
	Tags         tag.Tags  `json:"tags"`
	ProjectID    uuid.UUID `json:"projectId,omitempty"`
	ProjectColor string    `json:"projectColor"`
	ProjectName  string    `json:"projectName"`
	StartDate    time.Time `json:"date"`
}

// HistoryTasks is array of HistoryTask
type HistoryTasks []HistoryTask

// Render the HistoryTasks
func (u HistoryTasks) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedHistoryTasks is paginated list of HistoryTasks
type PaginatedHistoryTasks struct {
	HistoryTasks HistoryTasks `json:"items"`
	Count        int          `json:"count"`
}

// Render the PaginatedHistoryTasks
func (u PaginatedHistoryTasks) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
