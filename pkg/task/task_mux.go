package task

import (
	"net/http"
	"strconv"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/project"
	"wid/pkg/tag"
	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

// SaveTask the task
func SaveTask(w http.ResponseWriter, r *http.Request) {
	data := &Task{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	if data.ID == (uuid.UUID{}) {
		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		data.ID = id
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpInsertTask(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if data.ProjectID != (uuid.UUID{}) {
		err = project.UpdateTags(r.Context(), userID, data.ProjectID, data.Tags)
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
	}

	res, err := SelectTask(r.Context(), userID, data.ID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, res)
}

// SavePeriod the period
func SavePeriod(w http.ResponseWriter, r *http.Request) {
	data := &Period{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	if data.Task.ID == (uuid.UUID{}) {
		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		data.Task.ID = id
	}

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	if data.Running {
		err := UpdatePeriodRunning(r.Context(), userID)
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
	}

	err = UpInsertPeriod(r.Context(), userID, data)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	if data.Task.ProjectID != (uuid.UUID{}) {
		err = project.UpdateTags(r.Context(), userID, data.Task.ProjectID, data.Task.Tags)
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
	}

	res, err := SelectPeriod(r.Context(), userID, data.Task.ID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, res)
}

// StopPeriod the period
func StopPeriod(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	err = UpdatePeriodRunning(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetTask get a task
func GetTask(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	taskID, err := uuid.Parse(chi.URLParam(r, "taskID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	res, err := SelectTask(r.Context(), userID, taskID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, res)
}

// GetPeriod get a period
func GetPeriod(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	taskID, err := uuid.Parse(chi.URLParam(r, "taskID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	res, err := SelectPeriod(r.Context(), userID, taskID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, res)
}

// GetRunningPeriod get the running period
func GetRunningPeriod(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	res, err := SelectRunningPeriod(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseNotFound(err))
		return
	}

	render.Render(w, r, res)
}

// Remove the task or period
func Remove(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	taskID, err := uuid.Parse(chi.URLParam(r, "taskID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	err = Delete(r.Context(), userID, taskID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// GetTasks all tasks for a project
func GetTasks(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	res, err := GetAll(r.Context(), userID, projectID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, res)
}

// GetHistoryPeriods get last tasks
func GetHistoryPeriods(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	res, err := GetHistory(r.Context(), userID)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, res)
}
func convertParamToDate(r *http.Request, name string) (time.Time, error) {
	var res time.Time
	param := chi.URLParam(r, name)
	parseInt, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		return res, err
	}
	res = time.Unix(parseInt, 0)
	return res, nil
}

// GetPeriods all period between dates
func GetPeriods(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	startDate, err := convertParamToDate(r, "startDate")
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	endDate, err := convertParamToDate(r, "endDate")
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	res, err := GetAllByDate(r.Context(), userID, startDate, endDate)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, res)
}

// SearchByTitle returns the all tasks begin by title
func SearchByTitle(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	projectID, err := uuid.Parse(chi.URLParam(r, "projectID"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	title := r.URL.Query().Get("q")

	tasks, err := Search(r.Context(), userID, projectID, title)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	render.Render(w, r, tasks)
}

// Import time data
func Import(w http.ResponseWriter, r *http.Request) {
	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	data := &Times{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	for _, tm := range data.Times {

		if tag.Find(tm.Tags, data.Excludes) {
			continue
		}

		id, err := uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}

		period := Period{
			Task: Task{
				ID:    id,
				Title: tm.Title,
				Tags:  tm.Tags,
			},
			EndDate:   time.Unix(int64(tm.EndDate/1000), 0).UTC(),
			StartDate: time.Unix(int64(tm.StartDate/1000), 0).UTC(),
		}

		if data.DefaultProject != (uuid.UUID{}) {
			period.Task.ProjectID = data.DefaultProject
		}

		for _, t := range tm.Tags.ToArray() {
			projectTag := tag.Tag(t)
			projectID, exists := data.Projects[projectTag]
			if exists {
				period.Task.ProjectID = projectID
			}
		}

		err = UpInsertPeriod(r.Context(), userID, &period)
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}

		if period.Task.ProjectID != (uuid.UUID{}) {
			err = project.UpdateTags(r.Context(), userID, period.Task.ProjectID, period.Task.Tags)
			if err != nil {
				render.Render(w, r, httperror.ErrResponseInternalServerError(err))
				return
			}
		}
	}
}
