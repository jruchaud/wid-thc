package wcontext

import (
	"bytes"
	"crypto/tls"
	"errors"
	"net"
	"net/http"
	"net/smtp"
	"reflect"
	"strings"
	"text/template"
	"time"

	"github.com/google/uuid"
)

// I18nTemplate represent a template for a lang
type I18nTemplate struct {
	Subject string
	Body    string
}

// EmailTemplate a template in all lang
type EmailTemplate struct {
	EN *I18nTemplate
	FR *I18nTemplate
}

// SendEmail send an email
func SendEmail(config *Config, to string, subject string, body string) error {
	smtpHost := config.SMTPHost
	smtpPort := config.SMTPPort
	smtpUser := config.SMTPUser
	smtpPassword := config.SMTPPassword
	mailFromAddress := config.MailFromAddress
	mailDomain := config.MailDomain

	if smtpHost == "" {
		return nil
	}

	// Authentication
	var auth smtp.Auth = nil
	if smtpUser != "" && smtpPassword != "" {
		auth = smtp.PlainAuth("", smtpUser, smtpPassword, smtpHost)
	}

	// Message
	// 'Date' and 'Message-ID' headers are added to avoid being marked as SPAM
	// https://datatracker.ietf.org/doc/html/rfc5322
	// https://www.jwz.org/doc/mid.html
	from := mailFromAddress + "@" + mailDomain
	// time.RFC1123Z seems the closest to the format defined in RFC5322
	date := time.Now().Format(time.RFC1123Z)
	message := "Date: " + date + "\r\n" +
		"From: " + from + "\r\n" +
		"To: " + to + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"Message-ID: <" + uuid.New().String() + "@" + mailDomain + ">\r\n" +
		body + "\r\n"

	// Sending email
	err := customSendMail(smtpHost+":"+smtpPort, mailDomain, auth, from, []string{to}, []byte(message))
	return err
}

// GetLang returns the lang in the request by default it is EN
func GetLang(r *http.Request) string {
	lang := strings.ToUpper(r.URL.Query().Get("lang"))
	if lang == "" {
		lang = "EN"
	}

	return lang
}

// GetTemplate select the template by the lang
func GetTemplate(r *http.Request, emailTemplate EmailTemplate, data interface{}) (string, string, error) {
	lang := GetLang(r)
	values := reflect.ValueOf(emailTemplate)
	value := values.FieldByName(lang)
	i := value.Interface()
	i18nTemplate, _ := i.(*I18nTemplate)

	subjectTemplate, err := template.New("Subject").Parse(i18nTemplate.Subject)
	if err != nil {
		return "", "", err
	}

	bodyTemplate, err := template.New("Body").Parse(i18nTemplate.Body)
	if err != nil {
		return "", "", err
	}

	buf := new(bytes.Buffer)
	err = subjectTemplate.Execute(buf, data)
	if err != nil {
		return "", "", err
	}
	subject := buf.String()

	buf = new(bytes.Buffer)
	err = bodyTemplate.Execute(buf, data)
	if err != nil {
		return "", "", err
	}
	body := buf.String()

	return subject, body, nil
}

// customSendMail custom version of smtp.SendMail
// Taken from Go net/smtp package (BSD-style licensed)
func customSendMail(addr string, localName string, a smtp.Auth, from string, to []string, msg []byte) error {
	if err := validateLine(from); err != nil {
		return err
	}
	for _, recp := range to {
		if err := validateLine(recp); err != nil {
			return err
		}
	}
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	if err = c.Hello(localName); err != nil {
		return err
	}
	if a != nil {
		if ok, _ := c.Extension("STARTTLS"); ok {
			host, _, _ := net.SplitHostPort(addr)
			//config := &tls.Config{ServerName: "mail.codelutin.com"}
			config := &tls.Config{ServerName: host}
			if err = c.StartTLS(config); err != nil {
				return err
			}
		}
		if ok, _ := c.Extension("AUTH"); !ok {
			return errors.New("smtp: server doesn't support AUTH")
		}
		if err = c.Auth(a); err != nil {
			return err
		}
	}
	if err = c.Mail(from); err != nil {
		return err
	}
	for _, addr := range to {
		if err = c.Rcpt(addr); err != nil {
			return err
		}
	}
	w, err := c.Data()
	if err != nil {
		return err
	}
	_, err = w.Write(msg)
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}

// validateLine checks to see if a line has CR or LF as per RFC 5321
// Taken from Go net/smtp package (BSD-style licensed)
func validateLine(line string) error {
	if strings.ContainsAny(line, "\n\r") {
		return errors.New("smtp: A line must not contain CR or LF")
	}
	return nil
}
