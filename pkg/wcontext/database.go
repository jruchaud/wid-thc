package wcontext

import (
	"context"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/tern/migrate"
)

// InitDatabase the connection to the database
func InitDatabase(databaseURL string) (*pgxpool.Pool, error) {
	var db *pgxpool.Pool

	poolConfig, err := pgxpool.ParseConfig(databaseURL)
	if err != nil {
		return db, err
	}

	db, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		return db, err
	}

	log.Printf("Connecting database %s", databaseURL)
	return db, nil
}

// execSQLFile run a sql script into the database
func execSQLFile(db *pgx.Conn, fileName string) error {
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	sql, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	requests := strings.Split(string(sql), "\r")
	for _, request := range requests {
		_, err := db.Exec(context.Background(), request)
		if err != nil {
			return err
		}
	}

	return nil
}

// CreateDatabase init the tables into the database and migrate the dll
func CreateDatabase(databaseURL string, path string) error {
	ctx := context.Background()

	poolConfig, err := pgx.ParseConfig(databaseURL)
	if err != nil {
		return err
	}

	db, err := pgx.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		return err
	}
	defer db.Close(ctx)

	migrator, err := migrate.NewMigrator(ctx, db, "public.schema_version")
	if err != nil {
		return err
	}

	data := make(map[string]interface{})
	migrator.Data = data

	err = migrator.LoadMigrations(path + "/sql/dll/")
	if err != nil {
		return err
	}

	currentVersion, err := migrator.GetCurrentVersion(ctx)
	log.Printf("Migrate database from %d to last version", currentVersion)

	err = migrator.Migrate(ctx)

	return err
}

// DeleteDatabase remove all tables
func DeleteDatabase(databaseURL string, path string) error {
	ctx := context.Background()

	poolConfig, err := pgx.ParseConfig(databaseURL)
	if err != nil {
		return err
	}

	db, err := pgx.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		return err
	}
	defer db.Close(ctx)

	return execSQLFile(db, path+"/sql/utils/drop_tables.sql")
}
