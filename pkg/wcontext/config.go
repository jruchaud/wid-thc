package wcontext

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

// Config of the application
// echo '{"CodeLutin":{"name": "Code lutin","host": "https://gitlab.nuiton.org/"}}' | base64
type Config struct {
	ServerURL               string        `json:"server.url"`
	ServerAllowedOrigins    []string      `json:"server.allowedOrigins"`
	DatabaseURL             string        `json:"database.url"`
	JWTSecret               string        `json:"jwt.secret"`
	RecoveryTokenExpiration time.Duration `json:"recovery.token.expiration"`
	SMTPHost                string        `json:"smtp.host"`
	SMTPPort                string        `json:"smtp.port"`
	SMTPUser                string        `json:"smtp.user"`
	SMTPPassword            string        `json:"smtp.password"`
	MailFromAddress         string        `json:"mail.from.address"`
	MailDomain              string        `json:"mail.domain"`
}

// LoadConfig the config file
func LoadConfig(fileName string) (Config, error) {
	var result Config

	file, err := os.Open(fileName)
	if err != nil {
		return result, err
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(b, &result)
	if err != nil {
		return result, err
	}

	if envValue := os.Getenv("DATABASE_URL"); envValue != "" {
		result.DatabaseURL = envValue
	}
	if envValue := os.Getenv("SERVER_URL"); envValue != "" {
		result.ServerURL = envValue
	}
	if envValue := os.Getenv("SERVER_ALLOWED_ORIGINS"); envValue != "" {
		var value []string
		err = json.NewDecoder(strings.NewReader(envValue)).Decode(&value)
		if err != nil {
			log.Fatal(err)
		}
		result.ServerAllowedOrigins = value
	}
	if envValue := os.Getenv("JWT_SECRET"); envValue != "" {
		result.JWTSecret = envValue
	}
	if envValue := os.Getenv("RECOVERY_TOKEN_EXPIRATION"); envValue != "" {
		value, err := strconv.ParseInt(envValue, 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		result.RecoveryTokenExpiration = time.Duration(value)
	}
	if envValue := os.Getenv("SMTP_HOST"); envValue != "" {
		result.SMTPHost = envValue
	}
	if envValue := os.Getenv("SMTP_PORT"); envValue != "" {
		result.SMTPPort = envValue
	}
	if envValue := os.Getenv("SMTP_USER"); envValue != "" {
		result.SMTPUser = envValue
	}
	if envValue := os.Getenv("SMTP_PASSWORD"); envValue != "" {
		result.SMTPPassword = envValue
	}
	if envValue := os.Getenv("MAIL_FROM_ADDRESS"); envValue != "" {
		result.MailFromAddress = envValue
	}
	if envValue := os.Getenv("MAIL_DOMAIN"); envValue != "" {
		result.MailDomain = envValue
	}

	return result, nil
}
