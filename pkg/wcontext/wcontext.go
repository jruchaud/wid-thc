package wcontext

import (
	"context"
	goContext "context"
	"net/http"
	"time"

	"github.com/go-chi/jwtauth"
	"github.com/jackc/pgx/v4/pgxpool"
)

type contextKey string

// WidKey is the key name for the context
var WidKey contextKey = contextKey("wid")

// Context is the context of application
type Context struct {
	Config    *Config
	Database  *pgxpool.Pool
	TokenAuth *jwtauth.JWTAuth
}

// WithConfig add config
func (wc *Context) WithConfig(config *Config) {
	wc.Config = config
}

// WithDB add database
func (wc *Context) WithDB(database *pgxpool.Pool) {
	wc.Database = database
}

// WithJWTAuth add JWT auth token
func (wc *Context) WithJWTAuth(tokenAuth *jwtauth.JWTAuth) {
	wc.TokenAuth = tokenAuth
}

// Context get the current context
func (wc *Context) Context(r *http.Request) goContext.Context {
	return context.WithValue(r.Context(), WidKey, *wc)
}

// Duration in hour before jwt token expiration
var ExpiryDuration = time.Duration(7 * 24)

// GetDatabase from the context
func GetDatabase(context goContext.Context) *pgxpool.Pool {
	return context.Value(WidKey).(Context).Database
}

// GetJWTAuth from the context
func GetJWTAuth(context goContext.Context) *jwtauth.JWTAuth {
	return context.Value(WidKey).(Context).TokenAuth
}

// GetConfig from the context
func GetConfig(context goContext.Context) *Config {
	return context.Value(WidKey).(Context).Config
}
