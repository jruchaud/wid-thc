package event

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
	"wid/pkg/httperror"
	"wid/pkg/tag"
	"wid/pkg/task"
	"wid/pkg/user"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

var markEventType = "mark"

// SaveProject saves the project
func SaveEvents(w http.ResponseWriter, r *http.Request) {
	eventApiTokenUuid, err := uuid.Parse(chi.URLParam(r, "token"))
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}
	eventApiToken := user.EventApiToken(eventApiTokenUuid)
	userID, err := user.GetUserIdByToken(r.Context(), eventApiToken)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	incomingEvents := &IncomingEvents{}
	if err := render.Bind(r, incomingEvents); err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	var events Events = make(Events, 0)
	var createdTaskIDs []uuid.UUID = make([]uuid.UUID, 0)
	for _, incomingEvent := range *incomingEvents {
		event := &Event{}
		event.UserID = userID
		event.ID, err = uuid.NewUUID()
		if err != nil {
			render.Render(w, r, httperror.ErrResponseInternalServerError(err))
			return
		}
		event.StartDate = time.Unix(int64(incomingEvent.BeginDateTime/1000), 0).UTC()
		event.EndDate = time.Unix(int64(incomingEvent.EndDateTime/1000), 0).UTC()
		event.Type = incomingEvent.Type
		event.Source = incomingEvent.Source
		event.Data = incomingEvent.Data
		events = append(events, event)

		if event.Type == markEventType && !event.StartDate.Equal(event.EndDate) {
			var markEventData = event.Data
			switch markEventData["action"] {
			case "start":
				// ignore, we wait for the "stop" event to create activity
			case "stop":
				newTask := &task.Task{}
				newTaskId, err := uuid.NewUUID()
				if err != nil {
					render.Render(w, r, httperror.ErrResponseInternalServerError(err))
					return
				}
				newTask.ID = newTaskId
				if tags, ok := markEventData["tags"]; ok {
					var tagsAsEmptyInterfaceArray = tags.([]interface{})
					var tagsAsStringArray = make([]string, 0)
					for _, aTagAsInterface := range tagsAsEmptyInterfaceArray {
						tagsAsStringArray = append(tagsAsStringArray, aTagAsInterface.(string))
					}
					newTask.Tags = tag.NewStringTags(tagsAsStringArray...)
				} else {
					render.Render(w, r, httperror.ErrResponseBadRequest(fmt.Errorf("missing 'tags' for event of type %v", markEventType)))
				}
				if projectID, ok := markEventData["projectId"]; ok {
					newTask.ProjectID, err = uuid.Parse(projectID.(string))
					if err != nil {
						render.Render(w, r, httperror.ErrResponseBadRequest(fmt.Errorf("projetId %v is not valid", projectID)))
					}
				} else {
					// it's OK, projectId is not mandatory
				}
				if title, ok := markEventData["title"]; ok {
					newTask.Title = title.(string)
				} else {
					newTask.Title = strings.Join(newTask.Tags.ToArray(), " ")
				}
				newPeriod := &task.Period{
					Task:      *newTask,
					StartDate: event.StartDate,
					EndDate:   event.EndDate,
					Running:   false}
				err = task.UpInsertPeriod(r.Context(), userID, newPeriod)
				createdTaskIDs = append(createdTaskIDs, newTask.ID)
				if err != nil {
					render.Render(w, r, httperror.ErrResponseInternalServerError(err))
					return
				}
			default:
				render.Render(w, r, httperror.ErrResponseBadRequest(fmt.Errorf("unexepected action '%v' in event of type %v", markEventData["action"], markEventType)))
				return
			}
		}
	}

	err = InsertEvents(r.Context(), &events)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	eventIDs := make([]uuid.UUID, 0)
	for _, event := range events {
		eventIDs = append(eventIDs, event.ID)
	}

	render.Render(w, r, SaveEventsResult{eventIDs, createdTaskIDs})
}

// GetEvents get activites
func GetEvents(w http.ResponseWriter, r *http.Request) {

	userID, err := user.GetUserID(r)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseForbidden(err))
		return
	}

	startDate, err := convertParamToDate(r, "startDate")
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	endDate, err := convertParamToDate(r, "endDate")
	if err != nil {
		render.Render(w, r, httperror.ErrResponseBadRequest(err))
		return
	}

	events, err := GetAllByDate(r.Context(), userID, startDate, endDate)
	if err != nil {
		render.Render(w, r, httperror.ErrResponseInternalServerError(err))
		return
	}

	result := GetEventsResult{events}

	render.Render(w, r, result)
}

// TODO factoriser
func convertParamToDate(r *http.Request, name string) (time.Time, error) {
	param := r.URL.Query().Get(name)
	parseInt, err := strconv.ParseInt(param, 10, 64)
	var res time.Time
	if err != nil {
		return res, err
	}
	res = time.Unix(parseInt, 0)
	return res, nil
}

// The (partial) content of the "data" for a event of type "mark"
type MarkEventData struct {
	Tags      tag.Tags
	Action    string
	Title     string
	ProjectID uuid.UUID
}

type SaveEventsResult struct {
	EventIDs       []uuid.UUID
	CreatedTaskIDs []uuid.UUID
}

type GetEventsResult struct {
	Events Events `json:"events"`
}

func (ser SaveEventsResult) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (ger GetEventsResult) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
