package event

import (
	"net/http"
	"time"

	"github.com/google/uuid"
)

// Event is an event tuple in the database
type Event struct {
	ID        uuid.UUID              `json:"id"`
	UserID    uuid.UUID              `json:"userId"`
	StartDate time.Time              `json:"startDate"`
	EndDate   time.Time              `json:"endDate"`
	Type      string                 `json:"type"`
	Source    string                 `json:"source"`
	Data      map[string]interface{} `json:"data"`
}

// Bind the Event
func (e *Event) Bind(r *http.Request) error {
	return nil
}

// Render the project
func (e Event) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Events is list of Event
type Events []*Event

// Bind the Events
func (es *Events) Bind(r *http.Request) error {
	return nil
}

// Render the Events
func (es Events) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// IncomingEvent is an incoming event from the public token API
type IncomingEvent struct {
	BeginDateTime float64                `json:"beginDateTime"`
	EndDateTime   float64                `json:"endDateTime"`
	Type          string                 `json:"type"`
	Source        string                 `json:"source"`
	Data          map[string]interface{} `json:"data"`
}

// Bind the IncomingEvent
func (e *IncomingEvent) Bind(r *http.Request) error {
	return nil
}

// Render the project
func (e IncomingEvent) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// IncomingEvents is list of IncomingEvent
type IncomingEvents []*IncomingEvent

// Bind the IncomingEvents
func (es *IncomingEvents) Bind(r *http.Request) error {
	return nil
}

// Render the IncomingEvents
func (es IncomingEvents) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
