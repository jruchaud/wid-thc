package event

import (
	"context"
	"time"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

// InsertEvents insert an event
func InsertEvents(
	context context.Context,
	events *Events) error {

	db := wcontext.GetDatabase(context)

	batch := &pgx.Batch{}

	for _, event := range *events {
		batch.Queue(
			`
				INSERT INTO event(
					id,
					userId,
					startDate,
					endDate,
					eventType,
					source,
					eventData)
				VALUES ($1, $2, $3, $4, $5, $6, $7)
			`,
			event.ID,
			event.UserID,
			event.StartDate,
			event.EndDate,
			event.Type,
			event.Source,
			event.Data)
	}

	br := db.SendBatch(context, batch)

	for i := 0; i < len(*events); i++ {
		_, err := br.Exec()
		if err != nil {
			return err
		}
	}

	err := br.Close()
	return err
}

func GetAllByDate(
	context context.Context,
	userID uuid.UUID,
	startDate time.Time,
	endDate time.Time) (Events, error) {

	db := wcontext.GetDatabase(context)

	sql := `
		SELECT id, userId, startDate, endDate, eventType, source, eventData
		FROM event
		WHERE userId = $1
		  AND startDate <= $3
		  AND endDate >= $2
	`

	rows, err := db.Query(context, sql, userID, startDate, endDate)

	var res Events
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var event Event

		err := rows.Scan(
			&event.ID,
			&event.UserID,
			&event.StartDate,
			&event.EndDate,
			&event.Type,
			&event.Source,
			&event.Data,
		)

		if err != nil {
			return res, err
		}

		res = append(res, &event)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}
