package team

import (
	"net/http"

	"github.com/google/uuid"
)

// Team represents a group of user
type Team struct {
	ID         uuid.UUID `json:"id"`
	Name       string    `json:"name"`
	Permission string    `json:"permission,omitempty"`
	IsAdmin    bool      `json:"admin,omitempty"`
	Count      int       `json:"count,omitempty"`
}

// Bind the Team
func (u *Team) Bind(r *http.Request) error {
	return nil
}

// Render the Team
func (u Team) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Teams is array of Team
type Teams []Team

// Render the Teams
func (u Teams) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// PaginatedTeams is paginated list of TinyUsers
type PaginatedTeams struct {
	Teams Teams `json:"items"`
	Count int   `json:"count"`
}

// Render the PaginatedTeams
func (u PaginatedTeams) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Member represents a member of group
type Member struct {
	TeamID  uuid.UUID `json:"teamId"`
	UserID  uuid.UUID `json:"userId"`
	IsAdmin bool      `json:"admin"`
}

// Bind the Member
func (u *Member) Bind(r *http.Request) error {
	return nil
}
