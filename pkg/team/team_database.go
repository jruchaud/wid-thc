package team

import (
	"context"
	"errors"
	"wid/pkg/permission"
	"wid/pkg/selector"
	"wid/pkg/user"
	"wid/pkg/wcontext"

	"github.com/google/uuid"
)

// UpInsertTeam a team
func UpInsertTeam(
	context context.Context,
	userID uuid.UUID,
	team *Team) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO team(id, name) 
		VALUES ($1, $2)
		ON CONFLICT (id) DO UPDATE SET name=excluded.name`

	_, err := db.Exec(context, sql, team.ID, team.Name)

	return err
}

// InsertMember uses to insert the first member
func InsertMember(
	context context.Context,
	userID uuid.UUID,
	teamID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := `INSERT INTO team_member(teamId, userId, isAdmin) VALUES ($1, $2, true)`
	_, err := db.Exec(context, sql, teamID, userID)

	return err
}

// UpdateMember modify a member
func UpdateMember(
	context context.Context,
	userID uuid.UUID,
	member *Member) error {

	db := wcontext.GetDatabase(context)

	sql := `
		INSERT INTO team_member(teamId, userId, isAdmin)
			SELECT $1 as teamId, $2 as userId, $3 as isAdmin
			FROM team_member t
			WHERE t.teamId = $1
			AND t.userId = $4
			AND t.isAdmin = true
		ON CONFLICT (teamId, userId) DO UPDATE SET isAdmin=excluded.isAdmin
		WHERE EXISTS (
			SELECT 1 
			FROM team_member t
			WHERE t.teamId = $1
			AND t.userId = $4
			AND t.isAdmin = true 
		)
		AND EXISTS (
			SELECT 1 
			FROM team_member t
			WHERE t.teamId = $1
			AND t.userId <> $2
			AND t.isAdmin = true 
		)
	`
	_, err := db.Exec(context, sql, member.TeamID, member.UserID, member.IsAdmin, userID)

	return err
}

// DeleteMember delete a member
func DeleteMember(
	context context.Context,
	userID uuid.UUID,
	member *Member) error {

	db := wcontext.GetDatabase(context)

	sql := `DELETE FROM team_member
		WHERE teamId = $1 AND userId = $2
		AND EXISTS (
			SELECT 1 
			FROM team_member t
			WHERE t.teamId = $1
			AND t.userId = $3
			AND t.isAdmin = true 
		)
		AND EXISTS (
			SELECT 1 
			FROM team_member t
			WHERE t.teamId = $1
			AND t.userId <> $2
			AND t.isAdmin = true
		)
	`
	_, err := db.Exec(context, sql, member.TeamID, member.UserID, userID)

	return err
}

// SelectTeams return all teams for an user
func SelectTeams(
	context context.Context,
	userID uuid.UUID) (PaginatedTeams, error) {

	sql := `
		SELECT 
			t.id, t.name,
			(SELECT count(*) FROM team_member c WHERE c.teamId = t.id) as count,
			m.isAdmin
		FROM team t, team_member m
		WHERE t.id = m.teamId
		AND m.userId = $1
		ORDER BY t.name
	`
	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name"},
		},
		sql,
		userID,
	)

	var res PaginatedTeams
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var team Team

		err := rows.Scan(&team.ID, &team.Name, &team.Count, &team.IsAdmin, &res.Count)
		if err != nil {
			return res, err
		}

		res.Teams = append(res.Teams, team)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectTeam return a team
func SelectTeam(
	context context.Context,
	userID uuid.UUID,
	teamID uuid.UUID) (Team, error) {

	db := wcontext.GetDatabase(context)

	sql := `
		SELECT 
			t.id, t.name,
			(SELECT count(*) FROM team_member c WHERE c.teamId = t.id) as count,
			m.isAdmin
		FROM team t, team_member m
		WHERE t.id = m.teamId
		AND t.id = $2
		AND m.userId = $1
		ORDER BY t.name
	`
	row := db.QueryRow(context, sql, userID, teamID)

	var team Team
	err := row.Scan(&team.ID, &team.Name, &team.Count, &team.IsAdmin)
	return team, err
}

// SearchTeams search a team by name
func SearchTeams(
	context context.Context,
	userID uuid.UUID,
	name string) (Teams, error) {

	db := wcontext.GetDatabase(context)

	sql := `
		SELECT t.id, t.name 
		FROM team t, team_member m
		WHERE t.id = m.teamId
		AND m.userId = $1
		AND t.name ilike $2
		ORDER BY t.name
		LIMIT 10
	`

	rows, err := db.Query(context, sql, userID, "%"+name+"%")

	var res Teams
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var team Team

		err := rows.Scan(&team.ID, &team.Name)
		if err != nil {
			return res, err
		}

		res = append(res, team)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// SelectMembers returns all user for a team
func SelectMembers(
	context context.Context,
	userID uuid.UUID,
	teamID uuid.UUID) (user.PaginatedTinyUsers, error) {

	sql := `
		SELECT u.id, u.name, u.email, m.isAdmin
		FROM wid_user u, team_member m
		WHERE m.userId = u.id
		AND m.teamId = $1
		AND EXISTS (
			SELECT 1
			FROM team_member t
			WHERE t.teamId = $1 AND t.userId = $2
		)
		ORDER BY u.name
	`

	rows, err := selector.Select(
		context,
		&selector.QueryConfig{
			Searchable: "name",
			Fields:     []string{"name", "email", "isAdmin"},
		},
		sql,
		teamID, userID,
	)

	var res user.PaginatedTinyUsers
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var u user.TinyUser
		var email string

		err := rows.Scan(&u.ID, &u.Name, &email, &u.TeamAdmin, &res.Count)
		if err != nil {
			return res, err
		}
		u.Avatar = user.GetAvatar(email)

		res.TinyUsers = append(res.TinyUsers, u)
	}

	err = rows.Err()
	if err != nil {
		return res, err
	}

	return res, nil
}

// DeleteTeam delete the team
func DeleteTeam(
	context context.Context,
	teamID uuid.UUID,
	userID uuid.UUID) error {

	db := wcontext.GetDatabase(context)

	sql := `SELECT 1 
			FROM team_member t 
			WHERE t.teamId=$1 AND t.userId=$2 AND t.isAdmin = true`

	var checkAdmin int
	row := db.QueryRow(context, sql, teamID, userID)
	err := row.Scan(&checkAdmin)
	if err != nil {
		return err
	}
	if checkAdmin == 0 {
		return errors.New("Invalid permission")
	}

	tx, err := db.Begin(context)
	if err != nil {
		return err
	}
	defer tx.Rollback(context)

	sql = `DELETE FROM team_permission WHERE teamId=$1 `
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM report_requested_team WHERE teamID=$1 `
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	sql = permission.SQLRequestMaintainer(`
		UPDATE project p
		SET repoId=NULL, repoCategory=NULL, repoProjectId=NULL
		WHERE EXISTS (
			SELECT 1 FROM repository r
			WHERE r.teamId = $1 AND p.repoId = r.id
		) AND %s
	`, "$2", "p.id")

	_, err = tx.Exec(context, sql, teamID, userID)
	if err != nil {
		return err
	}

	sql = `
		DELETE FROM repository_token rt 
			WHERE EXISTS (
				SELECT 1 FROM repository r WHERE rt.repositoryId = r.id AND r.teamId = $1
			)
	`
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM repository WHERE teamID=$1 `
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM team_member WHERE teamID=$1`
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	sql = `DELETE FROM team WHERE id=$1`
	_, err = tx.Exec(context, sql, teamID)
	if err != nil {
		return err
	}

	err = tx.Commit(context)
	return err
}
