package redmine

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

// NewClient create new client
func NewClient(token string, host string) (*Client, error) {

	c := Client{
		Token:      token,
		Host:       host,
		HTTPClient: http.DefaultClient,
	}

	c.Account = &AccountService{client: &c}
	c.Projects = &ProjectsService{client: &c}
	c.Search = &SearchService{client: &c}
	c.Issues = &IssuesService{client: &c}
	c.TimeEntries = &TimeEntriesService{client: &c}

	return &c, nil
}

// Client represents the client
type Client struct {
	Token string
	Host  string

	HTTPClient *http.Client

	Account     *AccountService
	Projects    *ProjectsService
	Search      *SearchService
	Issues      *IssuesService
	TimeEntries *TimeEntriesService
}

// NewRequest create a new request
func (c *Client) NewRequest(method, path string, o *Option, body interface{}) (*http.Request, error) {
	u := path
	if !strings.Contains(path, "https://") && !strings.Contains(path, "http://") {
		u = fmt.Sprintf("%s/%s", c.Host, path)
	}

	rURL, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	if o != nil {
		q := rURL.Query()
		q.Add("limit", strconv.Itoa(o.Limit))
		q.Add("offset", strconv.Itoa(o.Offset))

		rURL.RawQuery = q.Encode()
	}

	reqHeaders := make(http.Header)
	reqHeaders.Set("Accept", "application/json")
	reqHeaders.Set("Content-Type", "application/json")
	reqHeaders.Set("X-Redmine-API-Key", c.Token)

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, rURL.String(), buf)
	if err != nil {
		return nil, err
	}

	// Set the request specific headers.
	for k, v := range reqHeaders {
		req.Header[k] = v
	}

	return req, err
}

// Do execute the request
func (c *Client) Do(req *http.Request, v interface{}) (*http.Response, error) {

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return resp, err
	}

	// fmt.Println(req.URL, resp.StatusCode)

	if c := resp.StatusCode; 200 > c || c > 299 {
		err := fmt.Errorf("Request failed. Please analyze the request body for more details. Status code: %d", resp.StatusCode)
		return resp, err
	}

	if v != nil {
		defer resp.Body.Close()

		var ob map[string]interface{}
		err = json.NewDecoder(resp.Body).Decode(&ob)
		if err != nil {
			return resp, err
		}

		for key, value := range ob {
			if key != "limit" && key != "offset" && key != "total_count" {
				dbByte, err := json.Marshal(value)
				if err != nil {
					return resp, err
				}

				err = json.Unmarshal(dbByte, v)
				if err != nil {
					return resp, err
				}
			}
		}
	}

	return resp, nil
}

func (c *Client) DoXml(req *http.Request, v interface{}) (*http.Response, error) {
	httpResp, err := c.Do(req, nil)
	if err != nil {
		return nil, err
	}

	if v != nil {
		// Open a NewDecoder and defer closing the reader only if there is a provided interface to decode to
		defer httpResp.Body.Close()

		err = xml.NewDecoder(httpResp.Body).Decode(v)
	}

	return httpResp, err
}

// AccountService call for user api
type AccountService struct {
	client *Client
}

// Account information for the account
type Account struct {
	ID        int    `json:"id,omitempty"`
	Login     string `json:"login,omitempty"`
	Admin     bool   `json:"admin,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Mail      string `json:"mail,omitempty"`
}

// Me get my account information
func (s *AccountService) Me() (*Account, *http.Response, error) {
	u := fmt.Sprintf("my/account.json")

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var a *Account
	resp, err := s.client.Do(req, &a)
	if err != nil {
		return nil, resp, err
	}

	return a, resp, err
}

// GetAtomKey get atom key
func (s *AccountService) GetAtomKey() (string, *http.Response, error) {
	u := fmt.Sprintf("/projects")

	req, err := s.client.NewRequest("GET", u, nil, nil)
	req.Header.Del("Accept")
	req.Header.Del("Content-Type")
	if err != nil {
		return "", nil, err
	}

	resp, err := s.client.Do(req, nil)

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", resp, err
	}

	// href="/projects.atom?key=087dc284dd51af2093ede0e30eb62ceac167a2cd"
	r := regexp.MustCompile(`href="/projects.atom\?key=([^"]+)"`)
	result := r.FindStringSubmatch(string(data))[1]

	return result, resp, err
}

// Option to handle the pagination
type Option struct {
	Limit  int
	Offset int
}

// ProjectsService call for projects api
type ProjectsService struct {
	client *Client
}

// Project information for the project
type Project struct {
	ID          int    `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Identifier  string `json:"identifier,omitempty"`
	Description string `json:"description,omitempty"`
}

// Get get all projects
func (s *ProjectsService) Get(o *Option) ([]*Project, *http.Response, error) {
	u := fmt.Sprintf("projects.json")

	req, err := s.client.NewRequest("GET", u, o, nil)
	if err != nil {
		return nil, nil, err
	}

	var p []*Project
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

// GetByID get a projects
func (s *ProjectsService) GetByID(id int) (*Project, *http.Response, error) {
	u := fmt.Sprintf("projects/%d.json", id)

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var p *Project
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

// SearchService call for search
type SearchService struct {
	client *Client
}

// SearchResult information for the result
type SearchResult struct {
	ID    int    `json:"id,omitempty"`
	Title string `json:"title,omitempty"`
	URL   string `json:"url,omitempty"`
}

// Issues search all issues
func (s *SearchService) Issues(project string, q string, o *Option) ([]*SearchResult, *http.Response, error) {
	u := fmt.Sprintf("projects/%s/search.json?q=%s&issues=1&all_words=1&titles_only=1", project, q)

	req, err := s.client.NewRequest("GET", u, o, nil)
	if err != nil {
		return nil, nil, err
	}

	var r []*SearchResult
	resp, err := s.client.Do(req, &r)
	if err != nil {
		return nil, resp, err
	}

	return r, resp, err
}

// IssuesService call for issues
type IssuesService struct {
	client *Client
}

// Issue information for the issue
type Issue struct {
	ID          int    `json:"id,omitempty"`
	ProjectID   int    `json:"project_id,omitempty"`
	Subject     string `json:"subject,omitempty"`
	Description string `json:"description,omitempty"`
}

// CreateIssue information for the issue
type CreateIssue struct {
	Issue *Issue `json:"issue"`
}

// IssueDetails information for the issue
type IssueDetails struct {
	ID      int `json:"id,omitempty"`
	Project struct {
		ID   int    `json:"id,omitempty"`
		Name string `json:"name,omitempty"`
	} `json:"project,omitempty"`
	Subject     string `json:"subject,omitempty"`
	Description string `json:"description,omitempty"`
}

// Get get an issue
func (s *IssuesService) Get(id int) (*Issue, *http.Response, error) {
	u := fmt.Sprintf("issues/%d.json", id)

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var i *Issue
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}

// FromUrl get an issue from an url
func (s *IssuesService) FromUrl(url string) (*IssueDetails, *http.Response, error) {
	u := fmt.Sprintf("%s.json", url)

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var i *IssueDetails
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}

// Create create an issue
func (s *IssuesService) Create(issue *Issue) (*Issue, *http.Response, error) {
	u := fmt.Sprintf("issues.json")

	req, err := s.client.NewRequest("POST", u, nil, CreateIssue{Issue: issue})
	if err != nil {
		return nil, nil, err
	}

	var i *Issue
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}

// AtomActivity get atom activities
func (s *IssuesService) AtomActivity(key string, from string, userID int) (*Feed, *http.Response, error) {
	// https://www.redmine.org/activity.atom?from=2021-04-18&show_issues=1&user_id=12098&with_subprojects=1

	u := fmt.Sprintf("activity.atom?show_issues=1&with_subprojects=1&from=%s&user_id=%d&key=%s", from, userID, key)
	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var f *Feed
	resp, err := s.client.DoXml(req, &f)
	if err != nil {
		return nil, resp, err
	}

	return f, resp, err
}

type Feed struct {
	Id      string  `xml:"id"`
	Title   string  `xml:"title"`
	Entries []Entry `xml:"entry"`
}

type Entry struct {
	Title   string `xml:"title"`
	Id      string `xml:"id"`
	Updated string `xml:"updated"`
}

// TimeEntriesService call for user api
type TimeEntriesService struct {
	client *Client
}

// TimeEntry information for the time
type TimeEntry struct {
	ID         int     `json:"id,omitempty"`
	IssueID    string  `json:"issue_id,omitempty"`
	SpentOn    string  `json:"spent_on,omitempty"`
	Hours      float64 `json:"hours,omitempty"`
	ActivityId string  `json:"activity_id,omitempty"`
	Comments   string  `json:"comments,omitempty"`
	UserID     string  `json:"user_id,omitempty"`
}

// CreateTimeEntry information for the time entry
type CreateTimeEntry struct {
	TimeEntry *TimeEntry `json:"time_entry"`
}

// Create an time entry
func (s *TimeEntriesService) Create(timeEntry *TimeEntry) (*TimeEntry, *http.Response, error) {
	u := fmt.Sprintf("time_entries.json")

	req, err := s.client.NewRequest("POST", u, nil, CreateTimeEntry{TimeEntry: timeEntry})
	if err != nil {
		return nil, nil, err
	}

	var i *TimeEntry
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}

// Update an time entry
func (s *TimeEntriesService) Update(id int, timeEntry *TimeEntry) (*TimeEntry, *http.Response, error) {
	u := fmt.Sprintf("time_entries/%d.json", id)

	req, err := s.client.NewRequest("PUT", u, nil, CreateTimeEntry{TimeEntry: timeEntry})
	if err != nil {
		return nil, nil, err
	}

	var i *TimeEntry
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}

// Delete an time entry
func (s *TimeEntriesService) Delete(id int) (*http.Response, error) {
	u := fmt.Sprintf("time_entries/%d.json", id)

	req, err := s.client.NewRequest("DELETE", u, nil, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)
	if err != nil {
		return resp, err
	}

	return resp, err
}

type TimeEntryActivities []TimeEntryActivity

type TimeEntryActivity struct {
	ID        int    `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	IsDefault bool   `json:"is_default,omitempty"`
	Active    bool   `json:"active,omitempty"`
}

// FromUrl get an issue from an url
func (s *TimeEntriesService) Enumerations() ([]TimeEntryActivity, *http.Response, error) {
	u := fmt.Sprintf("/enumerations/time_entry_activities.json")

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	var i []TimeEntryActivity
	resp, err := s.client.Do(req, &i)
	if err != nil {
		return nil, resp, err
	}

	return i, resp, err
}
