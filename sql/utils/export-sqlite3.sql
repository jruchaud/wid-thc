select json_group_array(t)
AS json_result FROM
(
	SELECT json_object(
		'startDate', beginDateTime,
		'endDate', endDateTime,
		'tags',  json_group_array(tag)
	) as t 
	from time
	where beginDateTime < endDateTime
	group by beginDateTime, endDateTime
)