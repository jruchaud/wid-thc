-- Gitlab #62 
-- Display duration mode for this report
-- durationDisplayModeExactDuration : exact duration (e.g. 3:33:02)
-- durationDisplayModeHoursPerDay : if > 0, ETP will be logued (e.g. 0.25 days)
-- durationDisplayModePortionOfTotal : percentage of total work (e.g. 23%)

ALTER TABLE report ADD COLUMN durationDisplayModeExactDuration BOOLEAN DEFAULT TRUE;
ALTER TABLE report ADD COLUMN durationDisplayModeHoursPerDay VARCHAR;
ALTER TABLE report ADD COLUMN durationDisplayModePortionOfTotal BOOLEAN DEFAULT FALSE;
