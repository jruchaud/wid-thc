-- add events and token support

ALTER TABLE wid_user ADD COLUMN eventApiToken UUID;

CREATE TABLE event (
    id UUID PRIMARY KEY,
    userId UUID REFERENCES wid_user(id) NOT NULL,
    startDate TIMESTAMP NOT NULL,
    endDate TIMESTAMP NOT NULL,
    eventType TEXT NOT NULL,
    source TEXT NOT NULL,
    eventData JSONB NOT NULL
);