CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS wid_user (
    id UUID PRIMARY KEY,
    email TEXT UNIQUE,
    password TEXT,
    name TEXT UNIQUE,
    recoveryToken UUID UNIQUE,
    expirationTime TIMESTAMP
);

CREATE TABLE IF NOT EXISTS project (
    id UUID PRIMARY KEY,
    name TEXT,
    color TEXT,
	allTagsUsed TEXT[],
	defaultTags TEXT[],
	repoId TEXT,
	repoCategory TEXT,
	repoProjectId TEXT
);

CREATE TABLE IF NOT EXISTS task (
    id UUID PRIMARY KEY,
    title TEXT,
    tags TEXT[],

    startDate TIMESTAMP,
    endDate TIMESTAMP,
    running boolean,

    owner UUID REFERENCES wid_user(id) NOT NULL,
    project UUID REFERENCES project(id)
);

CREATE OR REPLACE FUNCTION array_distinct(anyarray) RETURNS anyarray AS $$
  SELECT array_agg(DISTINCT x) FROM unnest($1) t(x);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION array_intersection(anyarray, anyarray) RETURNS anyarray as $$
SELECT ARRAY(
    SELECT $1[i]
    FROM generate_series( array_lower($1, 1), array_upper($1, 1) ) i
    WHERE ARRAY[$1[i]] && $2
);
$$ LANGUAGE SQL;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'permission') THEN
    -- none: no permission
    -- maintainer: modify project members permission
    -- reporter: read/write task permission
    -- supervisor: create dashboard permission
        CREATE TYPE permission AS ENUM ('none', 'maintainer', 'reporter', 'supervisor');
    END IF;
END$$;

CREATE TABLE IF NOT EXISTS team (
    id UUID PRIMARY KEY,
    name TEXT
);

CREATE TABLE IF NOT EXISTS team_member (
    teamId UUID REFERENCES team(id) NOT NULL,
    userId UUID REFERENCES wid_user(id) NOT NULL,
    isAdmin boolean,

    PRIMARY KEY(teamId, userId)
);

CREATE TABLE IF NOT EXISTS user_permission (
    projectId UUID REFERENCES project(id) NOT NULL,
    userId UUID REFERENCES wid_user(id) NOT NULL,
    permission permission,

    PRIMARY KEY(projectId, userId)
);

CREATE TABLE IF NOT EXISTS team_permission (
    projectId UUID REFERENCES project(id) NOT NULL,
    teamId UUID REFERENCES team(id) NOT NULL,
    permission permission,

    PRIMARY KEY(projectId, teamId)
);

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'frequency') THEN
        CREATE TYPE frequency AS ENUM ('day', 'week', 'month', 'year', 'custom');
    END IF;
END$$;

CREATE TABLE IF NOT EXISTS report (
    id UUID PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT, 
    frequency frequency NOT NULL, 
    startDate TIMESTAMP,
    endDate TIMESTAMP, 
    tags TEXT[], 
    ownerId UUID REFERENCES wid_user(id) NOT NULL,
    dataVisualisations TEXT[]
);

CREATE TABLE IF NOT EXISTS report_requested_user (
    userId UUID REFERENCES wid_user(id) NOT NULL, 
    reportId UUID REFERENCES report(id) NOT NULL,

    PRIMARY KEY(userId, reportId)
);

CREATE TABLE IF NOT EXISTS report_requested_team (
    teamId UUID REFERENCES team(id) NOT NULL, 
    reportId UUID REFERENCES report(id) NOT NULL,

    PRIMARY KEY(teamId, reportId)
);

CREATE TABLE IF NOT EXISTS report_project (
    projectId UUID REFERENCES project(id) NOT NULL, 
    reportId UUID REFERENCES report(id) NOT NULL,

    PRIMARY KEY(projectId, reportId)
);

CREATE TABLE IF NOT EXISTS report_validation_time (
    id UUID PRIMARY KEY,
    startDate TIMESTAMP NOT NULL,
    endDate TIMESTAMP NOT NULL, 
    taskTags TEXT[],
    taskTitle TEXT NOT NULL,
    duration FLOAT NOT NULL,

    userId UUID REFERENCES wid_user(id) NOT NULL, 
    reportId UUID REFERENCES report(id) NOT NULL,
    projectId UUID REFERENCES project(id) NOT NULL
);

CREATE INDEX IF NOT EXISTS project_name_idx ON project USING GIST (name gist_trgm_ops);

CREATE INDEX IF NOT EXISTS task_title_idx ON task USING GIST (title gist_trgm_ops);

CREATE INDEX IF NOT EXISTS user_name_idx ON wid_user USING GIST (name gist_trgm_ops);

CREATE INDEX IF NOT EXISTS team_name_idx ON team USING GIST (name gist_trgm_ops);

CREATE INDEX IF NOT EXISTS report_validation_time_idx ON report_validation_time  USING GIST (taskTitle gist_trgm_ops);

GRANT ALL ON ALL TABLES IN SCHEMA public TO sa;
