DROP TABLE IF EXISTS "repo_token" CASCADE;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'repository_category') THEN
        CREATE TYPE repository_category AS ENUM ('gitlab', 'jira');
    END IF;
END$$;

CREATE TABLE IF NOT EXISTS repository (
    id UUID PRIMARY KEY,
    name text NOT NULL,
    category repository_category NOT NULL,
    host text NOT NULL,

    teamId UUID REFERENCES team(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS repository_token (
    repositoryId UUID REFERENCES repository(id) NOT NULL,
    userId UUID REFERENCES wid_user(id) NOT NULL,
    token JSON NOT NULL,

    PRIMARY KEY(repositoryId, userId)
);

ALTER TABLE project DROP COLUMN IF EXISTS repoId;
ALTER TABLE project ADD COLUMN repoId UUID REFERENCES repository(id);

ALTER TABLE project DROP COLUMN IF EXISTS repoCategory;
ALTER TABLE project ADD COLUMN repoCategory repository_category;

GRANT ALL ON ALL TABLES IN SCHEMA public TO sa;
