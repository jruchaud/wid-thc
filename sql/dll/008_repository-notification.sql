-- Add the configuration enable notification on the timeline

ALTER TABLE repository_token ADD COLUMN notification BOOLEAN DEFAULT TRUE;
