-- make sure we never go back in time

UPDATE report SET endDate = startDate WHERE startDate IS NOT NULL AND endDate IS NOT NULL AND startDate > endDate;
UPDATE report_validation_time SET endDate = startDate WHERE startDate > endDate;
UPDATE task SET endDate = startDate WHERE startDate IS NOT NULL AND endDate IS NOT NULL AND startDate > endDate;

ALTER TABLE report ADD CONSTRAINT chronological_consistency CHECK (startDate IS NULL OR endDate IS NULL OR startDate <= endDate);
ALTER TABLE report_validation_time ADD CONSTRAINT chronological_consistency CHECK (startDate <= endDate);
ALTER TABLE task ADD CONSTRAINT chronological_consistency CHECK (startDate IS NULL OR endDate IS NULL OR startDate <= endDate);
