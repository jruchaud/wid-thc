ALTER TABLE report ADD COLUMN synchronizable BOOLEAN DEFAULT FALSE;

CREATE TABLE IF NOT EXISTS report_synchronization (
    id UUID PRIMARY KEY,
    repoId TEXT,
    repoCategory TEXT,
    updatedDate TIMESTAMP,
    startDate TIMESTAMP,
    endDate TIMESTAMP,

    worklogs JSON,

    reportId UUID REFERENCES report(id) NOT NULL,
    owner UUID REFERENCES wid_user(id) NOT NULL
);

GRANT ALL ON ALL TABLES IN SCHEMA public TO sa;
