-- RENAME

ALTER INDEX IF EXISTS project_name_idx RENAME TO project_name_gidx;

ALTER INDEX IF EXISTS task_title_idx RENAME TO task_title_gidx;

ALTER INDEX IF EXISTS user_name_idx RENAME TO user_name_gidx;

ALTER INDEX IF EXISTS team_name_idx RENAME TO team_name_gidx;

ALTER INDEX IF EXISTS report_validation_time_idx RENAME TO report_validation_time_gidx;

-- GIN INDEX

CREATE INDEX IF NOT EXISTS task_tags_gidx on task USING GIN (tags);

CREATE INDEX IF NOT EXISTS project_alltagsused_gidx on project USING GIN (alltagsused);

CREATE INDEX IF NOT EXISTS project_defaulttags_gidx on project USING GIN (defaulttags);

CREATE INDEX IF NOT EXISTS report_validation_time_tags_gidx on report_validation_time USING GIN (taskTags);

CREATE INDEX IF NOT EXISTS report_tags_gidx on report USING GIN (tags);

-- NORMAL INDEX

CREATE INDEX IF NOT EXISTS task_tags_idx on task (tags);

CREATE INDEX IF NOT EXISTS task_title_idx on task (title);

CREATE INDEX IF NOT EXISTS report_validation_time_name_idx ON report_validation_time (taskTitle);

CREATE INDEX IF NOT EXISTS report_validation_time_tags_idx on report_validation_time (taskTags);

CREATE INDEX IF NOT EXISTS report_tags_idx on report (tags);
