module wid

go 1.14

require (
	github.com/andygrunwald/go-jira v1.12.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/franela/goblin v0.0.0-20200825194134-80c0062ed6cd
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jackc/tern v1.12.1
	github.com/xanzy/go-gitlab v0.38.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200925080053-05aa5d4ee321 // indirect
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288
)
