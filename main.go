package main

import (
	"flag"
	"log"
	"wid/pkg/server"
	"wid/pkg/wcontext"

	"github.com/go-chi/jwtauth"
)

func main() {
	var wc wcontext.Context

	fileName := flag.String("config", "./config/config-dev.json", "config file for the application")
	flag.Parse()

	c, err := wcontext.LoadConfig(*fileName)
	if err != nil {
		log.Fatal(err)
	}
	wc.WithConfig(&c)

	err = wcontext.CreateDatabase(c.DatabaseURL, ".")
	if err != nil {
		log.Fatal(err)
	}

	db, err := wcontext.InitDatabase(c.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}
	wc.WithDB(db)

	tokenAuth := jwtauth.New("HS256", []byte(c.JWTSecret), nil)
	wc.WithJWTAuth(tokenAuth)

	err = server.Run(wc)
	if err != nil {
		log.Fatal(err)
	}
}
