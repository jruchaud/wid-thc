<div align="center">

# WID

![Logo Wid](https://gitlab.nuiton.org/uploads/-/system/project/avatar/257/src_img_logo.png?width=40)

What I did ?  
_The addictive time tracking software._

</div>

## Welcome in WID-thc project

WID-thc is the backend part of the WID project.  
The frontend part is hosted in [WID-cbd project](https://gitlab.nuiton.org/jruchaud/wid-cbd).

## How to develop on wid-thc locally

### Working with a local WID instance

To start WID on your local environment, you'll need PostgreSQL and an SMTP server. You can get both by running:

```bash
docker-compose up
```

Then, you can start WID. An empty database will be created on-the-fly on the first start.

```bash
go run main.go
```

You can play with the database with PGAdmin:

- Open [the local PGAdmin instance](http://localhost:8083/).
- Login with `admin@wid-timer.org` and password `test`
- Open group `Whatever` then `Local PG`
- Enter password `sa!!`

### Run tests

To run test, you need a postgresql instance running in backgroud:

```bash
docker-compose up postgresql-test
```

To run all tests:

```bash
go test -v ./...
```

To run a single test:

```bash
go test -v ./... -run TestProject
```

### Build

Docker build

```bash
docker build -f deploy/dockerfile -t wid-thc:latest --rm .
```

Docker run interactive mode

```bash
docker run -i -p 7979:3000 wid-thc:latest`
```

Docker run deamon mode

```bash
docker run -d -p 7979:3000 wid-thc:latest
```

## How to contribute to WID project

Please refer to the [dedicated wiki page](https://gitlab.nuiton.org/jruchaud/wid-cbd/-/wikis)

## TODO

- Handle idle (see https://web.dev/idle-detection/)
- Handle events
- ~~Teams~~
- ~~Import project from gitlab, github, jira, old wid, ...~~
- ~~Total on day~~
- ~~Reports~~


