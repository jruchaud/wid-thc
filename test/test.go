package test

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"strings"
	"time"
	"wid/pkg/issue"
	"wid/pkg/project"
	"wid/pkg/report"
	"wid/pkg/server"
	"wid/pkg/tag"
	"wid/pkg/task"
	"wid/pkg/team"
	"wid/pkg/user"
	"wid/pkg/wcontext"

	"github.com/go-chi/jwtauth"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

var WidTestContext wcontext.Context

func createTestServer() *httptest.Server {
	c, err := wcontext.LoadConfig("../config/config-test.json")
	if err != nil {
		log.Fatal(err)
	}
	WidTestContext.WithConfig(&c)

	err = wcontext.DeleteDatabase(c.DatabaseURL, "..")
	if err != nil {
		log.Fatal(err)
	}

	err = wcontext.CreateDatabase(c.DatabaseURL, "..")
	if err != nil {
		log.Fatal(err)
	}

	db, err := wcontext.InitDatabase(c.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}
	WidTestContext.WithDB(db)

	tokenAuth := jwtauth.New("HS256", []byte(c.JWTSecret), nil)
	WidTestContext.WithJWTAuth(tokenAuth)

	mux := server.GetMux(WidTestContext)
	testServer := httptest.NewServer(mux)

	return testServer
}

func toJSON(obj interface{}) *strings.Reader {
	json, err := json.Marshal(obj)
	if err != nil {
		log.Fatal(err)
	}

	return strings.NewReader(string(json))
}

func bodyToJSON(r *http.Response, target interface{}) {
	defer r.Body.Close()

	err := json.NewDecoder(r.Body).Decode(target)
	if err != nil {
		bodyAsString, errBodyPrint := httputil.DumpResponse(r, true)
		fmt.Printf("Error while converting as %s the following body \n :", target)
		if errBodyPrint == nil {
			fmt.Println(string(bodyAsString))
		} else {
			fmt.Println(errBodyPrint)
		}
		log.Fatal(err)
	}
}

// Basic data for tests
var testerUser *user.User = &user.User{Name: "tester", Email: "test@test.fr", Password: "test"}
var testProject = project.Project{Name: "Wid", Color: "blue"}
var testTask = task.Task{Title: "test", Tags: tag.NewStringTags("wid", "dev")}
var testPeriod = task.Period{
	Task:      task.Task{Title: "period", Tags: tag.NewStringTags("wid", "dev")},
	StartDate: time.Now(), EndDate: time.Now()}
var testPeriodRunning = task.Period{
	Task:      task.Task{Title: "period", Tags: tag.NewStringTags("wid", "dev")},
	StartDate: time.Now(), EndDate: time.Now(), Running: true}
var testReport = report.Report{Name: "TestReport", Frequency: "day", StartDate: time.Now(), Pinning: "UNPINNED"}

// Create an user
func createUser(server *httptest.Server, testerUser *user.User) user.TinyUser {
	code := "123909"
	hashCode, _ := bcrypt.GenerateFromPassword([]byte(code+testerUser.Email+WidTestContext.Config.JWTSecret), bcrypt.DefaultCost)
	url := fmt.Sprintf("%s/signup?code=%s&hashcode=%s", server.URL, code, hashCode)

	res, err := http.Post(url, "application/json", toJSON(testerUser))
	if err != nil {
		log.Fatal(err)
	}

	var u user.TinyUser
	bodyToJSON(res, &u)

	testerUser.ID = u.ID

	return u
}

func connectUser(server *httptest.Server, testerUser *user.User) string {
	// Sign in
	url := fmt.Sprintf("%s/signin", server.URL)
	res, err := http.Post(url, "application/json", toJSON(user.UserCredentials {
		Email:    testerUser.Email,
		Password: testerUser.Password}))
	if err != nil {
		log.Fatal(err)
	}

	// We're supposed to have only one cookie, being jwt cookie. Let's just take the first one in the slice
	jwtCookie := res.Cookies()[0]

	return jwtCookie.Value
}

func enableEventApi(server *httptest.Server, jwtToken string) user.EventApiToken {
	url := fmt.Sprintf("%s/user/event-api-token?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(""))
	if err != nil {
		log.Fatal(err)
	}

	var enableUserEventApiResult user.EnableUserEventApiResult
	bodyToJSON(res, &enableUserEventApiResult)
	tokenString := enableUserEventApiResult.NewToken
	uuid, err := uuid.Parse(tokenString)
	if err != nil {
		log.Fatal(err)
	}
	return user.EventApiToken(uuid)
}

// Create an connected user
func createConnectedUser(server *httptest.Server, testerUser *user.User) string {
	createUser(server, testerUser)
	return connectUser(server, testerUser)
}

// Create a project
func createProject(server *httptest.Server, jwtToken string, testProject project.Project) project.Project {
	url := fmt.Sprintf("%s/project?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(testProject))
	if err != nil {
		log.Fatal(err)
	}

	var p project.Project
	bodyToJSON(res, &p)
	return p
}

// Create a task
func createTask(server *httptest.Server, jwtToken string, testTask task.Task, projectID uuid.UUID) task.Task {
	testTask.ProjectID = projectID

	url := fmt.Sprintf("%s/task?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(testTask))
	if err != nil {
		log.Fatal(err)
	}

	var resTask task.Task
	bodyToJSON(res, &resTask)
	return resTask
}

func createPeriod(server *httptest.Server, jwtToken string, testPeriod task.Period, projectID uuid.UUID) task.Period {
	testPeriod.Task.ProjectID = projectID

	url := fmt.Sprintf("%s/period?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(testPeriod))
	if err != nil {
		log.Fatal(err)
	}

	var resPeriod task.Period
	bodyToJSON(res, &resPeriod)
	return resPeriod
}

func createTeam(server *httptest.Server, jwtToken string, testTeam team.Team, members ...uuid.UUID) team.Team {
	url := fmt.Sprintf("%s/team?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(testTeam))
	if err != nil {
		log.Fatal(err)
	}

	var resTeam team.Team
	bodyToJSON(res, &resTeam)

	for _, id := range members {
		member := team.Member{
			TeamID:  resTeam.ID,
			UserID:  id,
			IsAdmin: false,
		}

		url := fmt.Sprintf("%s/team/member?jwt=%s", server.URL, jwtToken)
		res, err = http.Post(url, "application/json", toJSON(member))
		if err != nil {
			log.Fatal(err)
		}
	}

	return resTeam
}

// Create a report
func createReport(
	server *httptest.Server, jwtToken string, testReport report.Report,
	projects []uuid.UUID, users []uuid.UUID, teams []uuid.UUID) report.Report {

	url := fmt.Sprintf("%s/report?jwt=%s", server.URL, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(testReport))
	if err != nil {
		log.Fatal(err)
	}

	var r report.Report
	bodyToJSON(res, &r)

	if len(projects) > 0 {
		url = fmt.Sprintf("%s/report/%s/projects?jwt=%s", server.URL, r.ID, jwtToken)
		res, err = http.Post(url, "application/json", toJSON(projects))
		if err != nil {
			log.Fatal(err)
		}
	}

	if len(users) > 0 {
		url = fmt.Sprintf("%s/report/%s/users?jwt=%s", server.URL, r.ID, jwtToken)
		res, err = http.Post(url, "application/json", toJSON(users))
		if err != nil {
			log.Fatal(err)
		}
	}

	if len(teams) > 0 {
		url = fmt.Sprintf("%s/report/%s/teams?jwt=%s", server.URL, r.ID, jwtToken)
		res, err = http.Post(url, "application/json", toJSON(teams))
		if err != nil {
			log.Fatal(err)
		}
	}

	return r
}

func createRepository(server *httptest.Server, jwtToken string, repo issue.Repo, teamID uuid.UUID) issue.Repo {
	url := fmt.Sprintf("%s/repo/%s?jwt=%s", server.URL, teamID, jwtToken)
	res, err := http.Post(url, "application/json", toJSON(repo))
	if err != nil {
		log.Fatal(err)
	}

	var resRepo issue.Repo
	bodyToJSON(res, &resRepo)

	return resRepo
}

func getProject(server *httptest.Server, jwtToken string, projectID uuid.UUID) (project.Project, *http.Response) {
	url := fmt.Sprintf("%s/project/%s?jwt=%s", server.URL, projectID, jwtToken)
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	var p project.Project
	bodyToJSON(res, &p)
	return p, res
}
