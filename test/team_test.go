package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"wid/pkg/team"
	"wid/pkg/user"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestTeam test the team mux
func TestTeam(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Handle team", func() {
		var server *httptest.Server
		var jwtToken string
		var otherUser user.TinyUser

		testTeam := team.Team{Name: "Test team"}

		g.Before(func() {
			server = createTestServer()

			otherUser = createUser(server, &user.User{Name: "other", Email: "other@test.fr", Password: "test"})
			jwtToken = createConnectedUser(server, testerUser)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Create team", func() {
			url := fmt.Sprintf("%s/team?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(testTeam))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &testTeam)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(testTeam.ID == uuid.UUID{}).IsFalse()

			url = fmt.Sprintf("%s/teams?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var teams team.PaginatedTeams
			bodyToJSON(res, &teams)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(teams.Count).Equal(1)
		})

		g.It("Add member", func() {
			testMember := team.Member{TeamID: testTeam.ID, UserID: otherUser.ID}

			url := fmt.Sprintf("%s/team/member?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(testMember))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/team/%s/members?jwt=%s", server.URL, testTeam.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var users user.PaginatedTinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(users.Count).Equal(2)
		})

		g.It("Remove member", func() {
			testMember := team.Member{TeamID: testTeam.ID, UserID: otherUser.ID}

			url := fmt.Sprintf("%s/team/member?jwt=%s", server.URL, jwtToken)
			req, err := http.NewRequest("DELETE", url, toJSON(testMember))
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/team/%s/members?jwt=%s", server.URL, testTeam.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var users user.PaginatedTinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(users.Count).Equal(1)
		})

		g.It("Remove team", func() {
			url := fmt.Sprintf("%s/team/%s?jwt=%s", server.URL, testTeam.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/teams?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var teams team.PaginatedTeams
			bodyToJSON(res, &teams)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(teams.Count).Equal(0)
		})
	})

	g.Describe("Search team", func() {
		var server *httptest.Server
		var jwtToken string

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)
			createTeam(server, jwtToken, team.Team{Name: "Test team"})
			createTeam(server, jwtToken, team.Team{Name: "Test wid"})
			createTeam(server, jwtToken, team.Team{Name: "Orga"})
		})

		g.After(func() {
			server.Close()
		})

		search := func(query string) (*http.Response, team.Teams) {
			url := fmt.Sprintf("%s/teams/search?q=%s&jwt=%s", server.URL, query, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ts team.Teams
			bodyToJSON(res, &ts)

			return res, ts
		}

		g.It("Found", func() {
			res, ps := search("T")

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(2)

		})

		g.It("Not found", func() {
			res, ps := search("x")

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(0)
		})

	})
}
