package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"wid/pkg/permission"
	"wid/pkg/project"
	"wid/pkg/team"
	"wid/pkg/user"

	"github.com/franela/goblin"
)

// TestPermission test the permission mux
func TestPermission(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("User permission", func() {
		var server *httptest.Server
		var jwtToken string
		var otherUser user.TinyUser
		var aProject project.Project

		g.Before(func() {
			server = createTestServer()

			otherUser = createUser(server, &user.User{Name: "other", Email: "other@test.fr", Password: "test"})
			jwtToken = createConnectedUser(server, testerUser)
			aProject = createProject(server, jwtToken, testProject)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Create permission", func() {
			permission := permission.UserPermission{
				ProjectID:  aProject.ID,
				UserID:     otherUser.ID,
				Permission: permission.PermissionReporter,
			}

			url := fmt.Sprintf("%s/permission/user?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(permission))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/users?jwt=%s", server.URL, aProject.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var users user.PaginatedTinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(users.Count).Equal(2)

			// Test the access to the project with the other user
			otherJwtToken := connectUser(server, &user.User{Email: "other@test.fr", Password: "test"})

			url = fmt.Sprintf("%s/projects?jwt=%s", server.URL, otherJwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps project.PaginatedProjects
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(1)
		})

		g.It("Remove permission", func() {
			permission := permission.UserPermission{
				ProjectID: aProject.ID,
				UserID:    otherUser.ID,
			}

			url := fmt.Sprintf("%s/permission/user?jwt=%s", server.URL, jwtToken)
			req, err := http.NewRequest("DELETE", url, toJSON(permission))
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/users?jwt=%s", server.URL, aProject.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var users user.PaginatedTinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(users.Count).Equal(1)
		})
	})

	g.Describe("Team permission", func() {
		var server *httptest.Server
		var jwtToken string
		var otherUser user.TinyUser
		var aProject project.Project
		var aTeam team.Team = team.Team{Name: "Test team"}

		g.Before(func() {
			server = createTestServer()

			otherUser = createUser(server, &user.User{Name: "other", Email: "other@test.fr", Password: "test"})
			jwtToken = createConnectedUser(server, testerUser)
			aProject = createProject(server, jwtToken, testProject)
			aTeam = createTeam(server, jwtToken, aTeam, otherUser.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Create permission", func() {
			permission := permission.TeamPermission{
				ProjectID:  aProject.ID,
				TeamID:     aTeam.ID,
				Permission: permission.PermissionReporter,
			}

			url := fmt.Sprintf("%s/permission/team?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(permission))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/teams?jwt=%s", server.URL, aProject.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var teams team.PaginatedTeams
			bodyToJSON(res, &teams)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(teams.Count).Equal(1)

			// Test the access to the project with the other user
			otherJwtToken := connectUser(server, &user.User{Email: "other@test.fr", Password: "test"})

			url = fmt.Sprintf("%s/projects?jwt=%s", server.URL, otherJwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps project.PaginatedProjects
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(1)
		})

		g.It("Remove permission", func() {
			permission := permission.TeamPermission{
				ProjectID: aProject.ID,
				TeamID:    aTeam.ID,
			}

			url := fmt.Sprintf("%s/permission/team?jwt=%s", server.URL, jwtToken)
			req, err := http.NewRequest("DELETE", url, toJSON(permission))
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/teams?jwt=%s", server.URL, aProject.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var teams team.PaginatedTeams
			bodyToJSON(res, &teams)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(teams.Count).Equal(0)
		})
	})
}
