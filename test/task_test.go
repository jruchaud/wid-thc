package test

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"wid/pkg/project"
	tag "wid/pkg/tag"
	"wid/pkg/task"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestTask test the task mux
func TestTask(t *testing.T) {
	g := goblin.Goblin(t)

	var server *httptest.Server
	var jwtToken string
	var p project.Project

	getPeriods := func(startDate time.Time, endDate time.Time) (*http.Response, task.Periods) {
		url := fmt.Sprintf("%s/periods/%d/%d?jwt=%s", server.URL, startDate.Unix(), endDate.Unix(), jwtToken)
		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}

		var periods task.Periods
		bodyToJSON(res, &periods)

		return res, periods
	}

	g.Describe("Save task", func() {
		var myTask task.Task
		var resTask task.Task
		var myPeriod task.Period
		var resPeriod task.Period

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
			p = createProject(server, jwtToken, testProject)

			myTask = task.Task{Title: "test", Tags: tag.NewStringTags("wid", "dev"), ProjectID: p.ID}
			myPeriod = task.Period{
				Task:      task.Task{Title: "period", Tags: tag.NewStringTags("wid", "dev"), ProjectID: p.ID},
				StartDate: time.Now(), EndDate: time.Now()}
		})

		g.After(func() {
			server.Close()
		})

		g.It("New task", func() {
			url := fmt.Sprintf("%s/task?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(myTask))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &resTask)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resTask.ID == uuid.UUID{}).IsFalse()
			g.Assert(resTask.Title).Equal("test")
		})

		g.It("Get task", func() {
			url := fmt.Sprintf("%s/task/%s?jwt=%s", server.URL, resTask.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &resTask)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resTask.ID == uuid.UUID{}).IsFalse()
			g.Assert(resTask.Title).Equal("test")
		})

		g.It("Delete task", func() {
			url := fmt.Sprintf("%s/task/%s?jwt=%s", server.URL, resTask.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/task/%s?jwt=%s", server.URL, resTask.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotFound)
		})

		g.It("New period", func() {
			url := fmt.Sprintf("%s/period?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(myPeriod))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &resPeriod)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resPeriod.Task.ID == uuid.UUID{}).IsFalse()
			g.Assert(resPeriod.Task.Title).Equal("period")
		})

		g.It("Get period", func() {
			url := fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, resPeriod.Task.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &resPeriod)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resPeriod.Task.ID == uuid.UUID{}).IsFalse()
			g.Assert(resPeriod.ProjectName).Equal("Wid")
			g.Assert(resPeriod.ProjectColor).Equal("blue")
		})

		g.It("Get history", func() {
			url := fmt.Sprintf("%s/periods/history?jwt=%s&limit=10", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var history task.PaginatedHistoryTasks
			bodyToJSON(res, &history)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(history.Count).Equal(1)
		})

		g.It("Delete period", func() {
			url := fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, resPeriod.Task.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, resPeriod.Task.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotFound)
		})
	})

	g.Describe("Running", func() {

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
			p = createProject(server, jwtToken, testProject)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Stop period", func() {
			firstPeriod := createPeriod(server, jwtToken, testPeriodRunning, p.ID)

			url := fmt.Sprintf("%s/period/stop?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}
			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, firstPeriod.Task.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var resPeriod task.Period
			bodyToJSON(res, &resPeriod)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resPeriod.Running).IsFalse()
		})

		g.It("Stop old period", func() {
			firstPeriod := createPeriod(server, jwtToken, testPeriodRunning, p.ID)
			createPeriod(server, jwtToken, testPeriodRunning, p.ID)

			url := fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, firstPeriod.Task.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var resPeriod task.Period
			bodyToJSON(res, &resPeriod)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(resPeriod.Running).IsFalse()
		})

	})

	g.Describe("Project", func() {

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Only task", func() {
			p = createProject(server, jwtToken, testProject)

			createTask(server, jwtToken, task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("wid")}, p.ID)

			url := fmt.Sprintf("%s/project/%s/tasks?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var titles task.PaginatedTitles
			bodyToJSON(res, &titles)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(titles.Count).Equal(1)
			g.Assert(titles.Titles[0].Count).Equal(1)
			g.Assert(titles.Titles[0].Tags.Has("wid")).IsTrue()
			g.Assert(titles.Titles[0].IsDefault).IsTrue()
		})

		g.It("Only period", func() {
			p = createProject(server, jwtToken, testProject)

			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("wid")},
				StartDate: time.Now(), EndDate: time.Now()}, p.ID)

			url := fmt.Sprintf("%s/project/%s/tasks?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var titles task.PaginatedTitles
			bodyToJSON(res, &titles)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(titles.Count).Equal(1)
			g.Assert(titles.Titles[0].Count).Equal(1)
			g.Assert(titles.Titles[0].Tags.Has("wid")).IsTrue()
			g.Assert(titles.Titles[0].IsDefault).IsFalse()
		})

		g.It("Both task and period", func() {
			p = createProject(server, jwtToken, testProject)

			createTask(server, jwtToken, task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("task")}, p.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("period")},
				StartDate: time.Now(), EndDate: time.Now()}, p.ID)

			url := fmt.Sprintf("%s/project/%s/tasks?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var titles task.PaginatedTitles
			bodyToJSON(res, &titles)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(titles.Count).Equal(2)
			g.Assert(titles.Titles[0].Tags.Has("task")).IsTrue()
			g.Assert(titles.Titles[0].IsDefault).IsTrue()
			g.Assert(titles.Titles[0].Count).Equal(1)
			g.Assert(titles.Titles[1].Tags.Has("period")).IsTrue()
			g.Assert(titles.Titles[1].IsDefault).IsFalse()
			g.Assert(titles.Titles[1].Count).Equal(1)
		})
	})

	g.Describe("Timeline", func() {

		now := time.Now()
		oneHours := now.Add(1 * time.Hour)

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)

			p = createProject(server, jwtToken, testProject)

			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("period")},
				StartDate: now, EndDate: oneHours}, p.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Not found before now", func() {
			startDate := now.Add(-2 * time.Hour)
			endDate := now.Add(-1 * time.Hour)

			res, periods := getPeriods(startDate, endDate)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(0)
		})

		g.It("Not found after now", func() {
			startDate := now.Add(2 * time.Hour)
			endDate := now.Add(3 * time.Hour)

			res, periods := getPeriods(startDate, endDate)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(0)
		})

		g.It("Found end date", func() {
			startDate := now.Add(-1 * time.Hour)
			endDate := now.Add(30 * time.Minute)

			res, periods := getPeriods(startDate, endDate)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(1)
		})

		g.It("Found start date", func() {
			startDate := now.Add(30 * time.Minute)
			endDate := now.Add(2 * time.Hour)

			res, periods := getPeriods(startDate, endDate)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(1)
		})

		g.It("Found start date and end date", func() {
			startDate := now.Add(1 * time.Hour)
			endDate := now.Add(2 * time.Hour)

			res, periods := getPeriods(startDate, endDate)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(1)
		})

	})

	g.Describe("Search", func() {
		var search func(query string) (*http.Response, task.Tasks)

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			p := createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})

			createTask(server, jwtToken, task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("ok")}, p.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix timeline", Tags: tag.NewStringTags("ko")},
				StartDate: time.Now(), EndDate: time.Now()}, p.ID)

			createTask(server, jwtToken, task.Task{Title: "Fix project", Tags: tag.NewStringTags("ok")}, p.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix user", Tags: tag.NewStringTags("ok")},
				StartDate: time.Now(), EndDate: time.Now()}, p.ID)

			search = func(query string) (*http.Response, task.Tasks) {
				url := fmt.Sprintf("%s/project/%s/tasks/search?q=%s&jwt=%s", server.URL, p.ID, query, jwtToken)
				res, err := http.Get(url)
				if err != nil {
					log.Fatal(err)
				}

				var ts task.Tasks
				bodyToJSON(res, &ts)

				return res, ts
			}
		})

		g.After(func() {
			server.Close()
		})

		g.It("Found", func() {
			res, ts := search("Fix")

			// t.Log(ts)
			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ts)).Equal(3)
			for _, ta := range ts {
				g.Assert(ta.Tags.Has("ok")).IsTrue()
			}
		})

		g.It("Not found", func() {
			res, ts := search("z")

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ts)).Equal(0)
		})
	})

	g.Describe("Import", func() {
		var w project.Project

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
			p = createProject(server, jwtToken, project.Project{Name: "Default", Color: "1"})
			w = createProject(server, jwtToken, project.Project{Name: "Wid", Color: "2"})
		})

		g.After(func() {
			server.Close()
		})

		g.It("Process", func() {
			b := []byte(`
				[
					{
						"startDate": 1461161854317,
						"endDate": 1461161857428,
						"tags": [
							"admin",
							"upgrade"
						]
					},
					{
						"startDate": 1461162072814,
						"endDate": 1461162609881,
						"tags": [
							"wid",
							"dev"
						]
					},
					{
						"startDate": 1461162609881,
						"endDate": 1461163047798,
						"tags": [
							"wid",
							"bug"
						]
					},
					{
						"startDate": 1461162609881,
						"endDate": 1461163047798,
						"tags": [
							"pause"
						]
					}
				]
			`)

			var tm []task.Time
			err := json.Unmarshal(b, &tm)
			if err != nil {
				log.Fatal(err)
			}

			projects := make(map[tag.Tag]uuid.UUID)
			projects[tag.Tag("wid")] = w.ID

			body := task.Times{
				Times: tm,
				Excludes: []tag.Tags{
					tag.NewStringTags("pause"),
				},
				DefaultProject: p.ID,
				Projects:       projects,
			}

			url := fmt.Sprintf("%s/import?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(body))
			if err != nil {
				log.Fatal(err)
			}
			g.Assert(res.StatusCode).Equal(http.StatusOK)

			res, periods := getPeriods(time.Unix(0, 0), time.Now())

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(periods)).Equal(3)
		})
	})
}
