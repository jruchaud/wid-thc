package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"wid/pkg/user"

	"github.com/franela/goblin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

// TestUser test the user mux
func TestUser(t *testing.T) {
	g := goblin.Goblin(t)
	id, err := uuid.NewUUID()
	if err != nil {
		log.Fatal(err)
		return
	}
	testerUser := &user.User{Name: "tester", Email: "test@", Password: "test", RecoveryToken: id, ExpirationTime: time.Now()}

	code := "123909"
	hashCode, _ := bcrypt.GenerateFromPassword([]byte(code+testerUser.Email+WidTestContext.Config.JWTSecret), bcrypt.DefaultCost)

	g.Describe("User signup", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()
		})

		g.After(func() {
			server.Close()
		})

		g.It("New user", func() {
			url := fmt.Sprintf("%s/signup?code=%s&hashcode=%s", server.URL, code, hashCode)
			res, err := http.Post(url, "application/json", toJSON(testerUser))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)
		})

		g.It("Already user exist", func() {
			url := fmt.Sprintf("%s/signup?code=%s&hashcode=%s", server.URL, code, hashCode)
			res, err := http.Post(url, "application/json", toJSON(testerUser))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusBadRequest)
		})
	})

	g.Describe("User signin", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()

			// Create an user
			url := fmt.Sprintf("%s/signup?code=%s&hashcode=%s", server.URL, code, hashCode)
			_, err := http.Post(url, "application/json", toJSON(testerUser))
			if err != nil {
				log.Fatal(err)
			}
		})

		g.After(func() {
			server.Close()
		})

		g.It("Not authorized", func() {
			res, err := http.Get(server.URL + "/protected")
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusUnauthorized)
		})

		g.It("Sign in", func() {
			url := fmt.Sprintf("%s/signin", server.URL)
			res, err := http.Post(url, "application/json", toJSON(user.UserCredentials {
				Email:    testerUser.Email,
				Password: testerUser.Password}))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)

			// We're supposed to have only one cookie, being the jwt cookie. Let's just take the first one in the slice and suppose it is here
			jwtToken := res.Cookies()[0].Value

			var connectedUser user.User
			bodyToJSON(res, &connectedUser)
			log.Printf("token: %s", jwtToken)

			g.Assert(connectedUser.Email).Equal(testerUser.Email)
			g.Assert(connectedUser.Name).Equal(testerUser.Name)
			g.Assert(connectedUser.Password).Equal("")

			// Access with jwt token to protected area
			url = fmt.Sprintf("%s/protected?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)

			// Get the connected user
			url = fmt.Sprintf("%s/user?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var user user.User
			bodyToJSON(res, &user)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(user.Email).Equal(testerUser.Email)
			g.Assert(user.Name).Equal(testerUser.Name)
			g.Assert(user.Password).Equal("")
			t.Log(user.Avatar)
		})

		g.It("Invalid email", func() {
			url := fmt.Sprintf("%s/signin?email=%s&password=%s", server.URL, "invalid email", testerUser.Password)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusBadRequest)
		})

		g.It("Invalid password", func() {
			url := fmt.Sprintf("%s/signin?email=%s&password=%s", server.URL, testerUser.Email, "invalid password")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusBadRequest)
		})
	})

	g.Describe("User signout", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()

			// Create an user
			_, err := http.Post(server.URL+"/signup", "application/json", toJSON(testerUser))
			if err != nil {
				log.Fatal(err)
			}
		})

		g.After(func() {
			server.Close()
		})

		g.It("Sign out", func() {
			url := fmt.Sprintf("%s/signout", server.URL)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			jwtCookie := res.Cookies()[0]
			g.Assert(jwtCookie.Value).Equal("")
			g.Assert(jwtCookie.Expires.Before(time.Now())).IsTrue()
			g.Assert(jwtCookie.MaxAge < 0).IsTrue()

			// Access to protected area should now fail
			url = fmt.Sprintf("%s/protected?jwt=%s", server.URL, jwtCookie.Value)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusUnauthorized)
		})
	})

	g.Describe("User password recovery", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()
			createConnectedUser(server, testerUser)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Request a token", func() {
			url := fmt.Sprintf("%s/resetpwd?email=%s", server.URL, testerUser.Email)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})

		g.It("Update password", func() {
			// The token is invalid
			url := fmt.Sprintf("%s/updatepwd?recoveryToken=%s&pwd=%s", server.URL, "e8221e7b-d7f5-11ea-9851-b05216247527", testerUser.Password)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusBadRequest)
		})
	})

	g.Describe("Modify user", func() {
		var server *httptest.Server
		var jwtToken string

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)

			createUser(server, &user.User{Name: "exists"})
		})

		g.After(func() {
			server.Close()
		})

		g.It("Update the username", func() {
			url := fmt.Sprintf("%s/user?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(user.User{Name: "test2", Email: "test@"}))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			// The token is invalid
			url = fmt.Sprintf("%s/user?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var u user.User
			bodyToJSON(res, &u)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(u.Name).Equal("test2")
		})

		g.It("Username already exists", func() {
			url := fmt.Sprintf("%s/user?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(user.User{Name: "exists", Email: "test@"}))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotAcceptable)
		})
	})

	g.Describe("User has event API token management", func() {
		var server *httptest.Server
		var jwtToken string

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)

			createUser(server, &user.User{Name: "exists"})
		})

		g.After(func() {
			server.Close()
		})

		g.It("Enable then disable the event API", func() {
			url := fmt.Sprintf("%s/user/event-api-token?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(""))
			if err != nil {
				log.Fatal(err)
			}

			var enableUserEventApiResult user.EnableUserEventApiResult
			g.Assert(res.StatusCode).Equal(http.StatusOK)
			bodyToJSON(res, &enableUserEventApiResult)

			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err = http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})
	})

	g.Describe("Verify user", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()
			createUser(server, &user.User{Name: "exists", Email: "test@"})
		})

		g.After(func() {
			server.Close()
		})

		g.It("Verify email exits", func() {
			url := fmt.Sprintf("%s/verify/email?email=%s", server.URL, "test@")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotAcceptable)
		})

		g.It("Verify email no exists", func() {
			url := fmt.Sprintf("%s/verify/email?email=%s", server.URL, "other@")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})

		g.It("Verify name exists", func() {
			url := fmt.Sprintf("%s/verify/name?name=%s", server.URL, "exists")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotAcceptable)
		})

		g.It("Verify name no exists", func() {
			url := fmt.Sprintf("%s/verify/name?name=%s", server.URL, "noexists")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})
	})

	g.Describe("Confim email", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()
		})

		g.After(func() {
			server.Close()
		})

		g.It("Verify email exits", func() {
			url := fmt.Sprintf("%s/confirm?email=%s", server.URL, "test@")
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var c user.Code
			bodyToJSON(res, &c)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(c).IsNotNil()
		})
	})
}
