package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"wid/pkg/project"
	"wid/pkg/report"
	tag "wid/pkg/tag"
	"wid/pkg/task"
	"wid/pkg/team"
	"wid/pkg/user"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestReport test the report mux
func TestReport(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Create report", func() {
		var server *httptest.Server
		var jwtToken string

		var r report.Report
		var projectThc project.Project
		var projectCbd project.Project
		var otherUser user.TinyUser
		var aTeam team.Team

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			// Create projects
			projectThc = createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})

			otherUser = createUser(server, &user.User{Name: "other", Email: "other@test.fr", Password: "test"})
			aTeam = createTeam(server, jwtToken, team.Team{Name: "Test team"}, otherUser.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Create report", func() {
			url := fmt.Sprintf("%s/report?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(testReport))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &r)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(r.ID != uuid.UUID{}).IsTrue()
		})

		g.It("Pinning", func() {
			url := fmt.Sprintf("%s/report/%s/pinning?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON("PINNED"))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})

		g.It("Get report", func() {
			var rt report.DetailedReport

			url := fmt.Sprintf("%s/report/%s?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &rt)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(rt.Report.ID != uuid.UUID{}).IsTrue()
			g.Assert(rt.Report.Pinning).Equal("PINNED")
		})

		g.It("Get all reports", func() {
			var all report.PaginatedDetailedReports

			url := fmt.Sprintf("%s/reports?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &all)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(all.Count).Equal(1)
		})

		g.It("Set project", func() {
			projects := []uuid.UUID{projectThc.ID, projectCbd.ID}

			url := fmt.Sprintf("%s/report/%s/projects?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(projects))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			// Verify
			var rt report.DetailedReport

			url = fmt.Sprintf("%s/report/%s?jwt=%s", server.URL, r.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &rt)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(rt.Projects)).Equal(2)
		})

		g.It("Set user", func() {
			users := []uuid.UUID{otherUser.ID}

			url := fmt.Sprintf("%s/report/%s/users?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(users))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			// Verify
			var rt report.DetailedReport

			url = fmt.Sprintf("%s/report/%s?jwt=%s", server.URL, r.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &rt)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(rt.Users)).Equal(1)
		})

		g.It("Set team", func() {
			teams := []uuid.UUID{aTeam.ID}

			url := fmt.Sprintf("%s/report/%s/teams?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(teams))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			// Verify
			var rt report.DetailedReport

			url = fmt.Sprintf("%s/report/%s?jwt=%s", server.URL, r.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &rt)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(rt.Teams)).Equal(1)
		})

		g.It("Delete report", func() {
			url := fmt.Sprintf("%s/report/%s?jwt=%s", server.URL, r.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			// Verify
			var all report.PaginatedDetailedReports

			url = fmt.Sprintf("%s/reports?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &all)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(all.Count).Equal(0)
		})
	})

	g.Describe("Validation sheets", func() {
		var server *httptest.Server
		var jwtToken string

		var r report.Report
		var projectThc project.Project
		var projectCbd project.Project

		now := time.Now()
		oneHours := now.Add(1 * time.Hour)
		twoHours := now.Add(2 * time.Hour)

		g.Before(func() {
			server = createTestServer()

			u := createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			// Create projects
			projectThc = createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			// createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})

			// Create periods
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "bug")},
				StartDate: now, EndDate: oneHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "dev")},
				StartDate: oneHours, EndDate: twoHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix style", Tags: tag.NewStringTags("wid")},
				StartDate: now, EndDate: twoHours}, projectCbd.ID)

			// Create report
			r = createReport(
				server, jwtToken,
				report.Report{Name: "TestReport", Frequency: "day", StartDate: now, Tags: tag.NewStringTags("dev"), Pinning: "UNPINNED"},
				[]uuid.UUID{projectThc.ID, projectCbd.ID},
				[]uuid.UUID{u.ID},
				[]uuid.UUID{},
			)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Get requests", func() {
			url := fmt.Sprintf("%s/requests?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var requests report.PaginatedRequests
			bodyToJSON(res, &requests)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(requests.Count).Equal(1)
		})

		g.It("Get tasks", func() {
			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/request/tasks?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var displayedTasks report.DisplayedReportedTasks
			bodyToJSON(res, &displayedTasks)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(displayedTasks.Reported)).Equal(1)
			g.Assert(len(displayedTasks.Validated)).Equal(0)
		})

		g.It("Get late users", func() {
			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/request/late?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var users user.TinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(users)).Equal(1)
		})

		g.It("Get requests count", func() {
			url := fmt.Sprintf("%s/user/requests?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var count int
			bodyToJSON(res, &count)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(count).Equal(1)
		})

		g.It("Validate", func() {
			sheet := &report.ValidationSheet{
				Period: report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours},
				Tasks: report.ReportedTasks{
					report.ReportedTask{
						ProjectID: projectThc.ID,
						TaskTitle: "Fix project",
						TaskTags:  tag.NewStringTags("dev"),
						Duration:  14221,
					},
				},
			}

			url := fmt.Sprintf("%s/request/sheet?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(sheet))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			// Verify the validation
			url = fmt.Sprintf("%s/request/late?jwt=%s", server.URL, jwtToken)
			res, err = http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var users user.TinyUsers
			bodyToJSON(res, &users)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(users)).Equal(0)
		})
	})

	g.Describe("Shared users", func() {
		var server *httptest.Server
		var jwtToken string

		var r report.Report
		var u user.TinyUser
		var projectThc project.Project
		var projectCbd project.Project

		g.Before(func() {
			server = createTestServer()

			u = createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			// Create projects
			projectThc = createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})

			// Create report
			r = createReport(
				server, jwtToken,
				report.Report{Name: "TestReport", Frequency: "day", Tags: tag.NewStringTags("dev"), Pinning: "UNPINNED"},
				[]uuid.UUID{projectThc.ID, projectCbd.ID},
				[]uuid.UUID{u.ID},
				[]uuid.UUID{},
			)
		})

		g.It("Set shared users", func() {
			sharedUsers := []uuid.UUID{u.ID}

			url := fmt.Sprintf("%s/report/%s/share?jwt=%s", server.URL, r.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(sharedUsers))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})
	})
}
