package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"wid/pkg/project"
	"wid/pkg/tag"
	"wid/pkg/task"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestProject test the project mux
func TestProject(t *testing.T) {
	g := goblin.Goblin(t)

	var jwtToken string

	g.Describe("Project management", func() {
		var server *httptest.Server
		var p project.Project

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
		})

		g.After(func() {
			server.Close()
		})

		g.It("New project", func() {
			url := fmt.Sprintf("%s/project?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(testProject))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &p)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(p.ID == uuid.UUID{}).IsFalse()
			g.Assert(p.Color).Equal("blue")
		})

		g.It("Update project", func() {
			p.Color = "red"

			url := fmt.Sprintf("%s/project?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(p))
			if err != nil {
				log.Fatal(err)
			}

			bodyToJSON(res, &p)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(p.Color).Equal("red")
		})

		g.It("Get projects", func() {
			url := fmt.Sprintf("%s/projects?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps project.PaginatedProjects
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(1)
		})

		g.It("Get project", func() {
			url := fmt.Sprintf("%s/project/%s?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var p project.Project
			bodyToJSON(res, &p)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(p.Name).Equal("Wid")
		})

		g.It("Project not found", func() {
			url := fmt.Sprintf("%s/project/%s?jwt=%s", server.URL, "b577b575-c76a-11ea-9c6f-b05216247527", jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNotFound)
		})

		g.It("Get collaborators", func() {
			url := fmt.Sprintf("%s/projects/collaborators?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(project.IDs{p.ID}))
			if err != nil {
				log.Fatal(err)
			}

			var c project.Collaborators
			bodyToJSON(res, &c)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(c.Users)).Equal(1)
			g.Assert(len(c.Teams)).Equal(0)
		})

		g.It("Get collaborators no projects", func() {
			url := fmt.Sprintf("%s/projects/collaborators?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(project.IDs{}))
			if err != nil {
				log.Fatal(err)
			}

			var c project.Collaborators
			bodyToJSON(res, &c)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(c.Users)).Equal(1)
			g.Assert(len(c.Teams)).Equal(0)
		})

		g.It("Delete project", func() {
			url := fmt.Sprintf("%s/project/%s?jwt=%s", server.URL, p.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/projects?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps project.PaginatedProjects
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(0)
		})
	})

	g.Describe("Handle tags", func() {
		var server *httptest.Server
		var p project.Project

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
			p = createProject(server, jwtToken, testProject)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Add tag", func() {
			t := tag.Configuration{Tag: "wid", IsDefault: false}

			url := fmt.Sprintf("%s/project/%s/tag?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(t))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})

		g.It("Add default tag", func() {
			t := tag.Configuration{Tag: "test", IsDefault: true}

			url := fmt.Sprintf("%s/project/%s/tag?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(t))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)
		})

		g.It("Get all tags", func() {
			url := fmt.Sprintf("%s/project/%s/tags?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.PaginatedConfigurations
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)

			if ps.Configurations[0].Tag == "wid" {
				g.Assert(ps.Configurations[0].IsDefault).IsFalse()
				g.Assert(ps.Configurations[1].IsDefault).IsTrue()
			} else {
				g.Assert(ps.Configurations[1].IsDefault).IsFalse()
				g.Assert(ps.Configurations[0].IsDefault).IsTrue()
			}
		})

		g.It("Get all default tags", func() {
			url := fmt.Sprintf("%s/project/%s/default-tags?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.Tags
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Size()).Equal(1)
		})
	})

	g.Describe("Search", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			// Create projects
			createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})
		})

		g.After(func() {
			server.Close()
		})

		search := func(query string) (*http.Response, project.Projects) {
			url := fmt.Sprintf("%s/projects/search?q=%s&jwt=%s", server.URL, query, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps project.Projects
			bodyToJSON(res, &ps)

			return res, ps
		}

		g.It("Found", func() {
			res, ps := search("w")

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(2)

		})

		g.It("Not found", func() {
			res, ps := search("x")

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(0)
		})
	})

	g.Describe("Global modification", func() {
		var server *httptest.Server
		var p project.Project
		var w project.Project

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			p = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			w = createProject(server, jwtToken, project.Project{Name: "Wid", Color: "2"})

			createTask(server, jwtToken, task.Task{Title: "Fix", Tags: tag.NewStringTags("wid", "timeline")}, p.ID)
			createTask(server, jwtToken, task.Task{Title: "Fix move", Tags: tag.NewStringTags("test")}, p.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix", Tags: tag.NewStringTags("wid", "user")},
				StartDate: time.Now(), EndDate: time.Now()}, p.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Rename tag", func() {
			url := fmt.Sprintf("%s/project/%s/tags?tag=wid&by=test&jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Post(url, "application/json", nil)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/tags?jwt=%s", server.URL, p.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.PaginatedConfigurations
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(3)
		})

		g.It("Delete tag", func() {
			url := fmt.Sprintf("%s/project/%s/tags?tag=timeline&jwt=%s", server.URL, p.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/tags?jwt=%s", server.URL, p.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.PaginatedConfigurations
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(ps.Count).Equal(2)
		})

		g.It("Rename tasks name", func() {
			url := fmt.Sprintf("%s/project/%s/tasks?title=Fix&by=test&jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(project.ProjectTaskRemame{
				Title:     "Fix",
				Tags:      []string{"test"},
				ByTitle:   "test",
				ByTags:    []string{},
				IsDefault: true,
			}))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/tasks/search?q=test&jwt=%s", server.URL, p.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ts task.Tasks
			bodyToJSON(res, &ts)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ts)).Equal(1)
		})

		g.It("Move tasks", func() {
			move := project.TaskToMove{
				Title:       "Fix move",
				Tags:        tag.NewStringTags("test"),
				ToProjectID: w.ID,
				OnlyForMe:   false,
			}

			url := fmt.Sprintf("%s/project/%s/tasks?jwt=%s", server.URL, p.ID, jwtToken)
			req, err := http.NewRequest("PUT", url, toJSON(move))
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/project/%s/tasks?jwt=%s", server.URL, w.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var titles task.PaginatedTitles
			bodyToJSON(res, &titles)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(titles.Count).Equal(1)
		})
	})
}
