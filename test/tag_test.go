package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"
	"wid/pkg/project"
	tag "wid/pkg/tag"
	"wid/pkg/task"

	"github.com/franela/goblin"
)

// TestTags test the tags struct
func TestTags(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Tags", func() {
		g.It("New tags", func() {
			ts := tag.NewTags()

			g.Assert(ts.Size()).Equal(0)
		})

		g.It("Add tags", func() {
			ts := tag.NewTags()
			ts.Add("wid")
			ts.Add("test")

			g.Assert(ts.Size()).Equal(2)
			g.Assert(ts.Has("wid")).IsTrue()
			g.Assert(ts.Has("test")).IsTrue()
		})

		g.It("Add all tags", func() {
			ts := tag.NewTags()
			ts.AddAll("wid", "wid", "wid", "test")

			g.Assert(ts.Size()).Equal(2)
			g.Assert(ts.Has("wid")).IsTrue()
			g.Assert(ts.Has("test")).IsTrue()
		})

		g.It("Delete tags", func() {
			ts := tag.NewTags("wid", "test")
			ts.Delete("wid")

			g.Assert(ts.Size()).Equal(1)
			g.Assert(ts.Has("wid")).IsFalse()
			g.Assert(ts.Has("test")).IsTrue()
		})

		g.It("To array tags", func() {
			ts := tag.NewTags("wid", "test")
			arr := ts.ToArray()

			g.Assert(len(arr)).Equal(2)
		})

		g.It("Compare", func() {
			ts := tag.NewTags("wid", "test")
			others := tag.NewTags("test", "wid")

			g.Assert(reflect.DeepEqual(ts, others)).IsTrue()
		})

		g.It("Find", func() {
			search := tag.NewTags("wid", "test")
			all := []tag.Tags{tag.NewTags("wid", "dev"), tag.NewTags("wid", "test")}

			res := tag.Find(search, all)

			g.Assert(res).IsTrue()
		})
	})

	g.Describe("Select tags", func() {
		var server *httptest.Server
		var jwtToken string
		var projectThc project.Project
		var projectCbd project.Project

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			// Create projects
			projectThc = createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})

			// Create periods
			now := time.Now()
			oneHours := now.Add(1 * time.Hour)
			twoHours := now.Add(2 * time.Hour)

			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "bug")},
				StartDate: now, EndDate: oneHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "dev")},
				StartDate: oneHours, EndDate: twoHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix style", Tags: tag.NewStringTags("wid", "style")},
				StartDate: now, EndDate: twoHours}, projectCbd.ID)

		})

		g.It("GetAllTagsUsed", func() {
			url := fmt.Sprintf("%s/tags?limit=5&offset=0&order=tag&jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.PaginatedTags
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps.Tags)).Equal(4)
		})

		g.It("Search", func() {
			url := fmt.Sprintf("%s/tags/search?jwt=%s&q=%s&projectIds=%s&projectIds=%s",
				server.URL, jwtToken,
				"e", projectCbd.ID, projectThc.ID,
			)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.Tags
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(2)
		})

		g.It("No found", func() {
			url := fmt.Sprintf("%s/tags/search?jwt=%s&q=%s&projectIds=%s&projectIds=%s",
				server.URL, jwtToken,
				"z", projectCbd.ID, projectThc.ID,
			)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var ps tag.Tags
			bodyToJSON(res, &ps)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(ps)).Equal(0)
		})
	})
}
