package test

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
	"wid/pkg/event"
	"wid/pkg/project"
	"wid/pkg/task"
	"wid/pkg/user"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestProject test the event API
func TestEvent(t *testing.T) {
	g := goblin.Goblin(t)

	var jwtToken string
	var eventApiToken user.EventApiToken
	var aProject project.Project

	g.Describe("Event API", func() {
		var server *httptest.Server

		g.Before(func() {
			server = createTestServer()
			jwtToken = createConnectedUser(server, testerUser)
			eventApiToken = enableEventApi(server, jwtToken)
			aProject = createProject(server, jwtToken, testProject)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Store event", func() {
			var testIncomingEvent = toIncomingEvent("note", `{"content": "This is a note !"}`)
			var incomingEvents event.IncomingEvents = make(event.IncomingEvents, 0)
			incomingEvents = append(incomingEvents, testIncomingEvent)
			incomingEvents = append(incomingEvents, testIncomingEvent)
			incomingEvents = append(incomingEvents, testIncomingEvent)

			url := fmt.Sprintf("%s/webhook/%s", server.URL, uuid.UUID(eventApiToken).String())
			var json = toJSON(incomingEvents)
			res, err := http.Post(url, "application/json", json)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)

			var saveEventsResult *event.SaveEventsResult
			bodyToJSON(res, &saveEventsResult)

			for _, eventID := range saveEventsResult.EventIDs {
				g.Assert(eventID == uuid.UUID{}).IsFalse()
			}
			g.Assert(len(saveEventsResult.CreatedTaskIDs)).Equal(0)

			startDate := aRandomDateTime.Add(-2 * time.Hour)
			endDate := aRandomDateTime.Add(2 * time.Hour)

			url = fmt.Sprintf("%s/events?jwt=%s&startDate=%d&endDate=%d", server.URL, jwtToken, startDate.Unix(), endDate.Unix())
			res2, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var getEventsResult event.GetEventsResult
			bodyToJSON(res2, &getEventsResult)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(getEventsResult.Events)).Equal(3)
		})

		g.It("API is retro-compatible", func() {

			url := fmt.Sprintf("%s/webhook/%s", server.URL, uuid.UUID(eventApiToken).String())
			var json = `
				[{
					"beginDateTime": 1469449507985,
					"endDateTime": 1469449507985,
					"type": "note",
					"source": "me",
					"data": {
						"content": "This is a note !"
					}
				}]
			`
			res, err := http.Post(url, "application/json", strings.NewReader(json))
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)
		})

		g.It("Create task on mark event", func() {

			var incomingEvents event.IncomingEvents = make(event.IncomingEvents, 0)
			incomingEvents = append(incomingEvents, toIncomingEvent("mark", `{"tags": ["tag1", "tag2", "tag3"], "action": "stop"}`))
			incomingEvents = append(incomingEvents, toIncomingEvent("mark", `{"tags": ["tag4", "tag5", "tag6"], "action": "stop", "title": "I want the task to have this title"}`))
			incomingEvents = append(incomingEvents, toIncomingEvent("mark", fmt.Sprintf(`{"tags": ["tag7", "tag8", "tag9"], "action": "stop", "projectId": "%v"}`, aProject.ID.String())))

			url := fmt.Sprintf("%s/webhook/%s", server.URL, uuid.UUID(eventApiToken).String())
			var json = toJSON(incomingEvents)
			res, err := http.Post(url, "application/json", json)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusOK)

			var saveEventsResult *event.SaveEventsResult
			bodyToJSON(res, &saveEventsResult)

			for _, eventID := range saveEventsResult.EventIDs {
				g.Assert(eventID == uuid.UUID{}).IsFalse()
			}

			var createdTasks []task.Period = make([]task.Period, 0)
			for _, createdTaskID := range saveEventsResult.CreatedTaskIDs {
				getTaskUrl := fmt.Sprintf("%s/period/%s?jwt=%s", server.URL, createdTaskID.String(), jwtToken)
				tasksJson, err := http.Get(getTaskUrl)
				if err != nil {
					log.Fatal(err)
				}

				var createdTask task.Period
				bodyToJSON(tasksJson, &createdTask)

				g.Assert(tasksJson.StatusCode).Equal(http.StatusOK)
				g.Assert(createdTask.Task.ID == uuid.UUID{}).IsFalse()
				createdTasks = append(createdTasks, createdTask)
			}

			g.Assert(createdTasks[0].Task.Title).Equal("tag1 tag2 tag3")
			g.Assert(createdTasks[1].Task.Title).Equal("I want the task to have this title")
			g.Assert(createdTasks[2].Task.Title).Equal("tag7 tag8 tag9")
			g.Assert(createdTasks[2].Task.ProjectID).Equal(aProject.ID)
		})
	})
}

var aRandomDateTime = time.Date(2021, time.September, 8, 15, 30, 0, 0, time.UTC)
var beginDateTime = float64(aRandomDateTime.Unix() * 1000)
var endDateTime = float64(aRandomDateTime.Add(1*time.Hour+30*time.Minute).Unix() * 1000)

func toIncomingEvent(aType string, dataAsJson string) *event.IncomingEvent {
	var data = make(map[string]interface{})
	err := json.NewDecoder(strings.NewReader(dataAsJson)).Decode(&data)
	if err != nil {
		log.Fatal(err)
	}
	return &event.IncomingEvent{BeginDateTime: beginDateTime, EndDateTime: endDateTime, Type: aType, Source: "me", Data: data}
}
