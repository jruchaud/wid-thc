package test

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"wid/pkg/issue"
	"wid/pkg/project"
	"wid/pkg/redmine"
	"wid/pkg/team"

	"github.com/franela/goblin"
)

// TestRepository test the repository
func TestRepository(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Test decode for redmine", func() {

		decode := func(value string, v interface{}) {
			var ob map[string]interface{}
			err := json.NewDecoder(strings.NewReader(value)).Decode(&ob)
			if err != nil {
				log.Fatal(err)
			}

			for _, value := range ob {
				dbByte, err := json.Marshal(value)
				if err != nil {
					log.Fatal(err)
				}

				err = json.Unmarshal(dbByte, v)
				if err != nil {
					log.Fatal(err)
				}
			}
		}

		g.It("Decode project", func() {
			var p = `{
				"project": {
					"id": 1002,
					"name": "Wid",
					"identifier": "wid"
				}
			}`

			var result redmine.Project
			decode(p, &result)

			g.Assert(result.Name).Equal("Wid")
		})
	})

	g.Describe("Handle repo", func() {
		var server *httptest.Server
		var jwtToken string
		var myTeam team.Team
		var currentRepo issue.Repo

		g.Before(func() {
			server = createTestServer()

			me := createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			myTeam = createTeam(server, jwtToken, team.Team{Name: "Team with repo"}, me.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Create repo", func() {
			var repo = issue.Repo{
				Name:     "Test",
				Category: "gitlab",
				Host:     "http://",
			}

			url := fmt.Sprintf("%s/repo/%s?jwt=%s", server.URL, myTeam.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(repo))
			if err != nil {
				log.Fatal(err)
			}
			g.Assert(res.StatusCode).Equal(http.StatusOK)

			url = fmt.Sprintf("%s/repos/%s?jwt=%s", server.URL, myTeam.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var repos issue.PaginatedRepos
			bodyToJSON(res, &repos)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(repos.Count).Equal(1)

			currentRepo = repos.Repos[0]
		})

		g.It("Get repos", func() {
			url := fmt.Sprintf("%s/user/repos?jwt=%s", server.URL, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var repos issue.PaginatedRepos
			bodyToJSON(res, &repos)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(repos.Count).Equal(1)
		})

		g.It("Delete repo", func() {
			url := fmt.Sprintf("%s/repo/%s?jwt=%s", server.URL, currentRepo.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/repos/%s?jwt=%s", server.URL, myTeam.ID, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var repos issue.PaginatedRepos
			bodyToJSON(res, &repos)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(repos.Count).Equal(0)
		})
	})

	g.Describe("Notification repo", func() {
		var server *httptest.Server
		var jwtToken string
		var myRepo issue.Repo

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			me := createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			myTeam := createTeam(server, jwtToken, team.Team{Name: "Team with repo"}, me.ID)
			myRepo = createRepository(server, jwtToken, issue.Repo{Name: "Test", Category: "jira", Host: "http://"}, myTeam.ID)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Modify notification flag", func() {
			var repo = issue.Repo{
				ID:           myRepo.ID,
				Notification: false,
			}

			url := fmt.Sprintf("%s/notification/repo?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(repo))
			if err != nil {
				log.Fatal(err)
			}
			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			url = fmt.Sprintf("%s/user/repos?jwt=%s", server.URL, jwtToken)
			res, err = http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var repos issue.PaginatedRepos
			bodyToJSON(res, &repos)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(repos.Repos[0].Notification).IsFalse()
		})

		g.It("Get repo", func() {
			url := fmt.Sprintf("%s/repo/%s?jwt=%s", server.URL, myRepo.ID, jwtToken)
			res, err := http.Get(url)
			if err != nil {
				log.Fatal(err)
			}

			var repo issue.Repo
			bodyToJSON(res, &repo)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(repo.Name).Equal("Test")
		})
	})

	g.Describe("Handle project repo", func() {
		var server *httptest.Server
		var jwtToken string
		var p project.Project
		var myRepo issue.Repo

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			me := createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			myTeam := createTeam(server, jwtToken, team.Team{Name: "Team with repo"}, me.ID)
			myRepo = createRepository(server, jwtToken, issue.Repo{Name: "Test", Category: "jira", Host: "http://"}, myTeam.ID)

			p = createProject(server, jwtToken, testProject)
		})

		g.After(func() {
			server.Close()
		})

		g.It("Save repo", func() {
			var repo = issue.RepoProject{
				ID:   "project1",
				Repo: &myRepo,
			}

			url := fmt.Sprintf("%s/project/%s/repo?jwt=%s", server.URL, p.ID, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(repo))
			if err != nil {
				log.Fatal(err)
			}
			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			currentProject, res := getProject(server, jwtToken, p.ID)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(currentProject.RepoProjectID).Equal("project1")
		})

		g.It("Delete repo", func() {
			url := fmt.Sprintf("%s/project/%s/repo?jwt=%s", server.URL, p.ID, jwtToken)
			req, err := http.NewRequest("DELETE", url, nil)
			if err != nil {
				log.Fatal(err)
			}

			res, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			g.Assert(res.StatusCode).Equal(http.StatusNoContent)

			currentProject, res := getProject(server, jwtToken, p.ID)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(currentProject.RepoProjectID).Equal("")
		})
	})

}
