package test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"wid/pkg/project"
	"wid/pkg/report"
	tag "wid/pkg/tag"
	"wid/pkg/task"

	"github.com/franela/goblin"
	"github.com/google/uuid"
)

// TestChart test the chart mux
func TestChart(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Own report", func() {
		var server *httptest.Server
		var jwtToken string
		var rt report.Report

		now := time.Date(2021, time.Month(2), 21, 8, 10, 30, 0, time.UTC)
		oneHours := now.Add(1 * time.Hour)
		twoHours := now.Add(2 * time.Hour)

		g.Before(func() {
			server = createTestServer()

			jwtToken = createConnectedUser(server, testerUser)

			// Create projects
			projectThc := createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd := createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})

			// Create periods
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "bug")},
				StartDate: now, EndDate: oneHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "dev")},
				StartDate: oneHours, EndDate: twoHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix style", Tags: tag.NewStringTags("wid")},
				StartDate: now, EndDate: twoHours}, projectCbd.ID)

			rt = createReport(server, jwtToken, testReport, []uuid.UUID{projectThc.ID, projectCbd.ID}, []uuid.UUID{}, []uuid.UUID{})
		})

		g.After(func() {
			server.Close()
		})

		g.It("Project by tag", func() {
			query := &report.QueryPeriod{ReportID: rt.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/project/tags?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(3)
			g.Assert(len(data.DataSets)).Equal(2)
		})

		g.It("Total for project", func() {
			query := &report.QueryPeriod{ReportID: rt.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/total/project?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(2)
			g.Assert(len(data.DataSets)).Equal(1)
			g.Assert(len(data.DataSets[0].Data)).Equal(2)
			g.Assert(len(data.DataSets[0].BackgroundColor)).Equal(2)
		})

		g.It("Total for tag", func() {
			query := &report.QueryPeriod{ReportID: rt.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/total/tag?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(3)
			g.Assert(len(data.DataSets)).Equal(1)
			g.Assert(len(data.DataSets[0].Data)).Equal(3)
		})

		g.It("Total for task", func() {
			query := &report.QueryPeriod{ReportID: rt.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/total/task?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(2)
			g.Assert(len(data.DataSets)).Equal(1)
			g.Assert(len(data.DataSets[0].Data)).Equal(2)
			g.Assert(len(data.DataSets[0].BackgroundColor)).Equal(2)
		})

		g.It("Total of days", func() {
			query := &report.QueryPeriod{ReportID: rt.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/total/days?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var count float64
			bodyToJSON(res, &count)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(count).Equal(0.5)
		})
	})

	g.Describe("Report with sheets", func() {
		var server *httptest.Server
		var jwtToken string

		var r report.Report
		var projectThc project.Project
		var projectCbd project.Project

		now := time.Now()
		oneHours := now.Add(1 * time.Hour)
		twoHours := now.Add(2 * time.Hour)

		g.Before(func() {
			server = createTestServer()

			u := createUser(server, testerUser)
			jwtToken = connectUser(server, testerUser)

			// Create projects
			projectThc = createProject(server, jwtToken, project.Project{Name: "Wid-thc", Color: "blue"})
			projectCbd = createProject(server, jwtToken, project.Project{Name: "Wid-cbd", Color: "blue"})
			createProject(server, jwtToken, project.Project{Name: "Test", Color: "blue"})

			// Create periods
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "bug")},
				StartDate: now, EndDate: oneHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix project", Tags: tag.NewStringTags("wid", "dev")},
				StartDate: oneHours, EndDate: twoHours}, projectThc.ID)
			createPeriod(server, jwtToken, task.Period{
				Task:      task.Task{Title: "Fix style", Tags: tag.NewStringTags("wid")},
				StartDate: now, EndDate: twoHours}, projectCbd.ID)

			// Create report
			r = createReport(
				server, jwtToken,
				report.Report{Name: "TestReport", Frequency: "day", StartDate: now, Tags: tag.NewStringTags("dev"), Pinning: "PINNED"},
				[]uuid.UUID{projectThc.ID, projectCbd.ID},
				[]uuid.UUID{u.ID},
				[]uuid.UUID{},
			)
		})

		g.After(func() {
			server.Close()
		})

		g.It("No validation", func() {
			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/project/tags?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(0)
			g.Assert(len(data.DataSets)).Equal(0)
		})

		g.It("With validation", func() {
			sheet := &report.ValidationSheet{
				Period: report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours},
				Tasks: report.ReportedTasks{report.ReportedTask{
					ProjectID: projectThc.ID,
					TaskTitle: "Fix project",
					TaskTags:  tag.NewStringTags("dev"),
					Duration:  14221,
				}},
			}

			url := fmt.Sprintf("%s/request/sheet?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(sheet))
			if err != nil {
				log.Fatal(err)
			}

			// Get the chart
			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			url = fmt.Sprintf("%s/chart/project/tags?jwt=%s", server.URL, jwtToken)
			res, err = http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(1)
			g.Assert(len(data.DataSets)).Equal(2)
		})

		g.It("Total for user", func() {
			query := &report.QueryPeriod{ReportID: r.ID, StartDate: now, EndDate: twoHours}

			url := fmt.Sprintf("%s/chart/total/user?jwt=%s", server.URL, jwtToken)
			res, err := http.Post(url, "application/json", toJSON(query))
			if err != nil {
				log.Fatal(err)
			}

			var data report.ChartData
			bodyToJSON(res, &data)

			g.Assert(res.StatusCode).Equal(http.StatusOK)
			g.Assert(len(data.Labels)).Equal(1)
			g.Assert(len(data.DataSets)).Equal(1)
		})
	})
}
